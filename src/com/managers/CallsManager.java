package com.managers;

import java.util.ArrayList;
import java.util.List;

import com.domains.Calls;
import java.sql.*;
import java.util.*;

public class CallsManager 
{
	
	
	int id=1;
		
	public List findAll()
	{
		  
		  List callList=new ArrayList();
	      try
 	      {
 		   
			   DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			   
			   String url="jdbc:mysql://localhost:3306/test";
			   
		 	   Connection con=DriverManager.getConnection(url,"root","root");
		 	   
		 	   
		 	   String query="select * from test";
		 	   
		 	   PreparedStatement pstmt=con.prepareStatement(query);
		 	   
		 	   ResultSet rst=pstmt.executeQuery();
		 	   
		 	   while(rst.next())
		 	   {
		 		   Calls calls=new Calls();
		 		   calls.setId(rst.getInt("id"));
		 		   calls.setName11(rst.getString("name"));
		 		   calls.setName12(rst.getString("lastName"));
		 		   calls.setName13(rst.getString("middleName"));
		 		   callList.add(calls);
		 		  
		 	   }
	 	   
	 	  }
	 	   catch(Exception e)
	 	   {
	 		   
	 		  e.printStackTrace();
	 		   
	 	   }
	 	   
	 	   return callList;
	}
	
	
       public void add(Calls calls)
       {
		
    	calls.setId(id);  
		//callList.add(calls);
		id++;
		//System.out.println("successfully added,the size of the list is-->"+callList.size());
		
       }
             
       public void insertDataBase(Calls calls)
       {
    	   try
    	   {
    		   		   
    	   DriverManager.registerDriver(new com.mysql.jdbc.Driver());
    	   String url="jdbc:mysql://localhost:3306/test";
    	   Connection con=DriverManager.getConnection(url,"root","root");
    	   String query="insert into test(name,lastName,middleName,id,dob,usertype)values(?,?,?,?,?,?)";
    	   PreparedStatement pstmt=con.prepareStatement(query);
    	   pstmt.setString(1,calls.getName11());
    	   
    	   pstmt.setString(2,calls.getName12());
    	   
    	   pstmt.setString(3,calls.getName13());
    	   
    	   Timestamp tst=new Timestamp(calls.getDb().getTime());
    	   
    	   pstmt.setInt(4, calls.getId());
    	   
    	   pstmt.setTimestamp(5,tst);
    	   
    	   pstmt.setString(6,calls.getUsertype());
    	   
    	   pstmt.executeUpdate();
    	      	   
    	   }
    	   catch(Exception e)
    	   {
    		   e.printStackTrace();
    	   }
       }
       
       public void updateDataBase(Calls calls)
       {
    	   try
    	   {
    		   
    	   DriverManager.registerDriver(new com.mysql.jdbc.Driver());
    	   String url="jdbc:mysql://localhost:3306/test";
    	   Connection con=DriverManager.getConnection(url,"root","root");
    	   
    	   String query="update test set name=?,lastName=?,middleName=?,dob=? where id=?";
    	   PreparedStatement pstmt=con.prepareStatement(query);
    	   pstmt.setString(1,calls.getName11());
    	   
    	   pstmt.setString(2,calls.getName12());
    	   
    	   pstmt.setString(3,calls.getName13());
    	   
    	   Timestamp tst=new Timestamp(calls.getDb().getTime());
    	   
    	   pstmt.setInt(5, calls.getId());
    	   
    	   pstmt.setTimestamp(4,tst);
    	   
    	   	   
    	   
    	   pstmt.executeUpdate();
    	   
    	   }
    	   catch(Exception e)
    	   {
    		   e.printStackTrace();
    	   }
       }


	
}
	
	
