package com.ui.composers;

import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Div;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Textbox;

import com.domains.Calls;
import com.managers.CallsManager;

public class CallsComposer extends GenericForwardComposer {
	
	Div root,content,first,second;
	Menuitem load,edit,add,save,back;
	//Textbox field1,field2,field3;
	
	Textbox name11;
	Textbox name12;
	Textbox name13;
	Listbox listbox;
	CallsManager callsManager=new CallsManager();
	String mode="";
	
	public void onCreate$root(){
		
		//buttons visibility
		load.setDisabled(false);
		edit.setDisabled(true);
		add.setDisabled(false);
		save.setDisabled(true);
		back.setDisabled(true);
		
		
		second.setVisible(false);
		first.setParent(content);
		
		
	}
	
	public void onClick$add(){
		
		//buttons visibility
		load.setDisabled(true);
		edit.setDisabled(true);
		add.setDisabled(true);
		save.setDisabled(false);
		back.setDisabled(false);
		
		
		
		first.setVisible(false);
		second.setParent(content);
		second.setVisible(true);
		
        mode="add";
}
	
	public void onClick$save(){
	
	Calls calls=new Calls();
	
	calls.setName11(name11.getValue());
	calls.setName12(name12.getValue());
	calls.setName13(name13.getValue());
	//calls.setField1(field1.getValue());
	//calls.setField2(field2.getValue());
	//calls.setField3(field3.getValue());
	
	callsManager.add(calls);
}
	
	public void onClick$load(){
		
		listbox.setModel(new ListModelList(callsManager.findAll()));
		
	}
	
	public void onClick$back(){
		
		//buttons visibility
		load.setDisabled(false);
		edit.setDisabled(true);
		add.setDisabled(false);
		save.setDisabled(true);
		back.setDisabled(true);
		
		second.setVisible(false);
		first.setVisible(true);
		first.setParent(content);
	}
	
	
	public void onSelect$listbox()  {
		
		//buttons visibility
		load.setDisabled(true);
		edit.setDisabled(false);
		add.setDisabled(true);
		save.setDisabled(true);
		back.setDisabled(true);

	}
	
	public void onClick$edit(){
		
		//buttons visibility
		load.setDisabled(true);
		edit.setDisabled(true);
		add.setDisabled(true);
		save.setDisabled(false);
		back.setDisabled(false);
		
		first.setVisible(false);
		second.setParent(content);
		second.setVisible(true); 
		
		Calls calls=(Calls)listbox.getSelectedItem().getValue();
				
		name11.setValue(calls.getName11());
		name12.setValue(calls.getName12());
		name13.setValue(calls.getName13());
		
		mode="edit";
		
	}
	
	
		
}