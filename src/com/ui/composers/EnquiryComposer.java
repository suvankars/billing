package com.ui.composers;

import org.codehaus.groovy.control.messages.Message;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zkmax.zul.Tablelayout;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.api.Listcell;

import com.domains.Calls;
import com.managers.CallsManager;
import java.io.*;
import java.sql.*;
import org.zkoss.util.media.AMedia;

public class EnquiryComposer extends GenericForwardComposer {

	Div zmenu, zgrid, zform, main;
	Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack, mPrint;
	// Textbox field1,field2,field3;
	Textbox name11;
	Textbox name12;
	Textbox name13;
	Listbox listbox;
	Label editId;
	Datebox db;
	Combobox usertype;
	Tablelayout tbl;

	Iframe report;
	CallsManager callsManager = new CallsManager();
	String mode = "";

	public void onCreate$main() {

		// buttons visibility
		mLoad.setDisabled(false);
		mEdit.setDisabled(true);
		mNew.setDisabled(false);
		mSave.setDisabled(true);
		mDelete.setDisabled(true);
		mBack.setDisabled(true);
		mPrint.setDisabled(false);
		// pdf.setVisible(false);

	}

	public void onClick$mNew() {

		// form reset
		formReset();

		// buttons visibility
		mLoad.setDisabled(false);
		mEdit.setDisabled(true);
		mNew.setDisabled(true);
		mSave.setDisabled(false);
		mDelete.setDisabled(true);
		mBack.setDisabled(false);
		// pdf.setVisible(false);
		mode = "add";

	}

	public void onClick$mSave() {

		Calls calls = new Calls();

		calls.setName11(name11.getValue());

		calls.setName12(name12.getValue());

		calls.setName13(name13.getValue());

		calls.setUsertype(usertype.getValue());

		calls.setDb(db.getValue());

		if (!(editId.getValue().equals(""))) {
			System.out.println(editId.getValue());
			calls.setId(Integer.parseInt(editId.getValue()));
			callsManager.updateDataBase(calls);
		}

		else {

			// select mode before calling manager methods
			callsManager.add(calls);
			callsManager.insertDataBase(calls);
		}
		alert("Successfully added");

		listbox.setModel(new ListModelList(callsManager.findAll()));

	}

	public void onClick$mLoad() {

		listbox.setModel(new ListModelList(callsManager.findAll()));

	}

	public void onClick$mBack() {

		// form reset
		formReset();

		// buttons visibility
		mLoad.setDisabled(false);
		mEdit.setDisabled(true);
		mNew.setDisabled(false);
		mSave.setDisabled(true);
		mDelete.setDisabled(true);
		mBack.setDisabled(true);
		// pdf.setVisible(false);
		tbl.setVisible(true);
		report.setVisible(false);
		mPrint.setDisabled(false);

	}

	public void onSelect$listbox() {

		// buttons visibility
		mLoad.setDisabled(false);
		mEdit.setDisabled(false);
		mNew.setDisabled(true);
		mSave.setDisabled(false);
		mDelete.setDisabled(false);
		mBack.setDisabled(false);
		// pdf.setVisible(false);

	}

	public void onClick$mEdit() {
		// pdf.setVisible(false);
		// buttons visibility
		mLoad.setDisabled(false);

		mEdit.setDisabled(true);

		mNew.setDisabled(true);

		mSave.setDisabled(false);
		mDelete.setDisabled(true);
		mBack.setDisabled(false);

		Calls calls = (Calls) listbox.getSelectedItem().getValue();

		name11.setValue(calls.getName11());
		name12.setValue(calls.getName12());
		name13.setValue(calls.getName13());
		editId.setValue(String.valueOf(calls.getId()));

		mode = "edit";

	}

	public void onClick$mPrint() {
		System.out.println("print calling method");
		tbl.setVisible(false);
		report.setVisible(true);
		mBack.setDisabled(false);
		mPrint.setDisabled(true);
		mLoad.setDisabled(true);
		mSave.setDisabled(true);
		mNew.setDisabled(true);
		FileInputStream fin = null;

		try {

			File file = new File("C:/reports/bill.pdf");

			fin = new FileInputStream(file);

			int bufsize = (int) file.length();

			byte[] buf = new byte[bufsize];

			fin.read(buf);

			// prepare the AMedia for iframe
			final InputStream mediais = new ByteArrayInputStream(buf);

			final AMedia amedia = new AMedia("FirstReport.pdf", "pdf",
					"application/pdf", mediais);

			// set iframe content

			report.setContent(amedia);

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (fin != null) {

				try {
					fin.close();
				} catch (Exception e) {
				}
			}
		}

	}

	public void formReset() {

		listbox.setSelectedItem(null);

	}

}