package com.ui.composers;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class LoginComposer extends GenericForwardComposer{

	Window loginwindow;
	Button login;
	Label msg;
	Textbox username, password;
	
	public void onCreate$loginwindow(){
		
		
		loginwindow.doOverlapped();
		loginwindow.setPosition("center");
		loginwindow.setSizable(true);
		
	}
	
	
	public void onClick$login(){
		
		Session session=Sessions.getCurrent();
		
		if(username.getValue().equals("admin") && password.getValue().equals("admin")) {
			session.setAttribute("loginstatus", "ok");
			Executions.sendRedirect("index.zul");
		}
		else {
			msg.setVisible(true);
		}

	}
}
