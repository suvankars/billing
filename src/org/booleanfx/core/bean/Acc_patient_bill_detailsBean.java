	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Acc_patient_bill_detailsBean {

		private int  id ;

		private int  bill_header_id ;

		private String  line_cate_name ;

		private String  line_item_name ;

		private double  line_item_qty ;

		private String  line_item_sl_num ;

		private String  line_item_model ;

		private double  line_item_price ;

		private double  line_item_discount ;

		private String  line_item_tax_name ;

		private double  line_item_tax_amt ;

		private double  line_gross_amt ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setBill_header_id( int bill_header_id){
			this.bill_header_id = bill_header_id;
		}
		public int getBill_header_id(  ){
			return  this.bill_header_id ;
		}

		public void setLine_cate_name( String line_cate_name){
			this.line_cate_name = line_cate_name;
		}
		public String getLine_cate_name(  ){
			return  this.line_cate_name ;
		}

		public void setLine_item_name( String line_item_name){
			this.line_item_name = line_item_name;
		}
		public String getLine_item_name(  ){
			return  this.line_item_name ;
		}

		public void setLine_item_qty( double line_item_qty){
			this.line_item_qty = line_item_qty;
		}
		public double getLine_item_qty(  ){
			return  this.line_item_qty ;
		}

		public void setLine_item_sl_num( String line_item_sl_num){
			this.line_item_sl_num = line_item_sl_num;
		}
		public String getLine_item_sl_num(  ){
			return  this.line_item_sl_num ;
		}

		public void setLine_item_model( String line_item_model){
			this.line_item_model = line_item_model;
		}
		public String getLine_item_model(  ){
			return  this.line_item_model ;
		}

		public void setLine_item_price( double line_item_price){
			this.line_item_price = line_item_price;
		}
		public double getLine_item_price(  ){
			return  this.line_item_price ;
		}

		public void setLine_item_discount( double line_item_discount){
			this.line_item_discount = line_item_discount;
		}
		public double getLine_item_discount(  ){
			return  this.line_item_discount ;
		}

		public void setLine_item_tax_name( String line_item_tax_name){
			this.line_item_tax_name = line_item_tax_name;
		}
		public String getLine_item_tax_name(  ){
			return  this.line_item_tax_name ;
		}

		public void setLine_item_tax_amt( double line_item_tax_amt){
			this.line_item_tax_amt = line_item_tax_amt;
		}
		public double getLine_item_tax_amt(  ){
			return  this.line_item_tax_amt ;
		}

		public void setLine_gross_amt( double line_gross_amt){
			this.line_gross_amt = line_gross_amt;
		}
		public double getLine_gross_amt(  ){
			return  this.line_gross_amt ;
		}

	}

