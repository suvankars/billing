	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Acc_patient_bill_headerBean {

		private int  id ;

		private String  bill_no ;

		private String  bill_date ;

		private String  bill_time ;

		private String  customer_type ;

		private String  patient_reg_no ;

		private double  patient_reg_charges ;

		private String  patient_name ;

		private int  patient_age ;

		private String  patient_sex ;

		private String  patient_address ;

		private String  patient_telephone ;

		private String  patient_pincode ;

		private String  referred_by ;

		private String  professional_by ;

		private double  item_discount ;

		private double  bill_total_amt ;

		private double  amt_received ;

		private double  bill_due_amt ;

		private String  bill_payment_mode ;

		private String  bank_br ;

		private String  cheque_no ;

		private String  cheque_date ;

		private String  bank_name ;

		private String  card_no ;

		private String  card_expiry_dt ;

		private String  name_of_card ;
		
		private String naration ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setBill_no( String bill_no){
			this.bill_no = bill_no;
		}
		public String getBill_no(  ){
			return  this.bill_no ;
		}

		public void setBill_date( String bill_date){
			this.bill_date = bill_date;
		}
		public String getBill_date(  ){
			return  this.bill_date ;
		}

		public void setBill_time( String bill_time){
			this.bill_time = bill_time;
		}
		public String getBill_time(  ){
			return  this.bill_time ;
		}

		public void setCustomer_type( String customer_type){
			this.customer_type = customer_type;
		}
		public String getCustomer_type(  ){
			return  this.customer_type ;
		}

		public void setPatient_reg_no( String patient_reg_no){
			this.patient_reg_no = patient_reg_no;
		}
		public String getPatient_reg_no(  ){
			return  this.patient_reg_no ;
		}

		public void setPatient_reg_charges( double patient_reg_charges){
			this.patient_reg_charges = patient_reg_charges;
		}
		public double getPatient_reg_charges(  ){
			return  this.patient_reg_charges ;
		}

		public void setPatient_name( String patient_name){
			this.patient_name = patient_name;
		}
		public String getPatient_name(  ){
			return  this.patient_name ;
		}

		public void setPatient_age( int patient_age){
			this.patient_age = patient_age;
		}
		public int getPatient_age(  ){
			return  this.patient_age ;
		}

		public void setPatient_sex( String patient_sex){
			this.patient_sex = patient_sex;
		}
		public String getPatient_sex(  ){
			return  this.patient_sex ;
		}

		public void setPatient_address( String patient_address){
			this.patient_address = patient_address;
		}
		public String getPatient_address(  ){
			return  this.patient_address ;
		}

		public void setPatient_telephone( String patient_telephone){
			this.patient_telephone = patient_telephone;
		}
		public String getPatient_telephone(  ){
			return  this.patient_telephone ;
		}

		public void setPatient_pincode( String patient_pincode){
			this.patient_pincode = patient_pincode;
		}
		public String getPatient_pincode(  ){
			return  this.patient_pincode ;
		}

		public void setReferred_by( String referred_by){
			this.referred_by = referred_by;
		}
		public String getReferred_by(  ){
			return  this.referred_by ;
		}

		public void setProfessional_by( String professional_by){
			this.professional_by = professional_by;
		}
		public String getProfessional_by(  ){
			return  this.professional_by ;
		}

		public void setItem_discount( double item_discount){
			this.item_discount = item_discount;
		}
		public double getItem_discount(  ){
			return  this.item_discount ;
		}

		public void setBill_total_amt( double bill_total_amt){
			this.bill_total_amt = bill_total_amt;
		}
		public double getBill_total_amt(  ){
			return  this.bill_total_amt ;
		}

		public void setAmt_received( double amt_received){
			this.amt_received = amt_received;
		}
		public double getAmt_received(  ){
			return  this.amt_received ;
		}

		public void setBill_due_amt( double bill_due_amt){
			this.bill_due_amt = bill_due_amt;
		}
		public double getBill_due_amt(  ){
			return  this.bill_due_amt ;
		}

		public void setBill_payment_mode( String bill_payment_mode){
			this.bill_payment_mode = bill_payment_mode;
		}
		public String getBill_payment_mode(  ){
			return  this.bill_payment_mode ;
		}

		public void setBank_br( String bank_br){
			this.bank_br = bank_br;
		}
		public String getBank_br(  ){
			return  this.bank_br ;
		}

		public void setCheque_no( String cheque_no){
			this.cheque_no = cheque_no;
		}
		public String getCheque_no(  ){
			return  this.cheque_no ;
		}

		public void setCheque_date( String cheque_date){
			this.cheque_date = cheque_date;
		}
		public String getCheque_date(  ){
			return  this.cheque_date ;
		}

		public void setBank_name( String bank_name){
			this.bank_name = bank_name;
		}
		public String getBank_name(  ){
			return  this.bank_name ;
		}

		public void setCard_no( String card_no){
			this.card_no = card_no;
		}
		public String getCard_no(  ){
			return  this.card_no ;
		}

		public void setCard_expiry_dt( String card_expiry_dt){
			this.card_expiry_dt = card_expiry_dt;
		}
		public String getCard_expiry_dt(  ){
			return  this.card_expiry_dt ;
		}

		public void setName_of_card( String name_of_card){
			this.name_of_card = name_of_card;
		}
		public String getName_of_card(  ){
			return  this.name_of_card ;
		}
		public String getNaration() {
			return naration;
		}
		public void setNaration(String naration) {
			this.naration = naration;
		}

	}

