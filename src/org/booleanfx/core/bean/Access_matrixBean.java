	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Access_matrixBean {

		private int  id ;

		private String  resourcecategory ;

		private String  resourcename ;

		private String  view ;

		private String  save ;

		private String  edit ;

		private String  delete ;

		private String  list ;

		private String  print ;

		private int  employeeid ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setResourcecategory( String resourcecategory){
			this.resourcecategory = resourcecategory;
		}
		public String getResourcecategory(  ){
			return  this.resourcecategory ;
		}

		public void setResourcename( String resourcename){
			this.resourcename = resourcename;
		}
		public String getResourcename(  ){
			return  this.resourcename ;
		}

		public void setView( String view){
			this.view = view;
		}
		public String getView(  ){
			return  this.view ;
		}

		public void setSave( String save){
			this.save = save;
		}
		public String getSave(  ){
			return  this.save ;
		}

		public void setEdit( String edit){
			this.edit = edit;
		}
		public String getEdit(  ){
			return  this.edit ;
		}

		public void setDelete( String delete){
			this.delete = delete;
		}
		public String getDelete(  ){
			return  this.delete ;
		}

		public void setList( String list){
			this.list = list;
		}
		public String getList(  ){
			return  this.list ;
		}

		public void setPrint( String print){
			this.print = print;
		}
		public String getPrint(  ){
			return  this.print ;
		}

		public void setEmployeeid( int employeeid){
			this.employeeid = employeeid;
		}
		public int getEmployeeid(  ){
			return  this.employeeid ;
		}

	}

