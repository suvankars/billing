	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Appointment_registerBean {

		private int  id ;

		private String  entry_no ;

		private Date  entry_date ;

		private Timestamp  appointment_date ;

		private String  therapist_name ;

		private String  therapist_phone ;

		private String  patient_regno ;

		private String  patient_name ;

		private int  patient_age ;

		private String  patient_sex ;

		private String  caller_phone ;

		private String  referred_by ;

		private String  employee_name ;

		private String  description ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setEntry_no( String entry_no){
			this.entry_no = entry_no;
		}
		public String getEntry_no(  ){
			return  this.entry_no ;
		}

		public void setEntry_date( Date entry_date){
			this.entry_date = entry_date;
		}
		public Date getEntry_date(  ){
			return  this.entry_date ;
		}

		public void setAppointment_date( Timestamp appointment_date){
			this.appointment_date = appointment_date;
		}
		public Timestamp getAppointment_date(  ){
			return  this.appointment_date ;
		}

		public void setTherapist_name( String therapist_name){
			this.therapist_name = therapist_name;
		}
		public String getTherapist_name(  ){
			return  this.therapist_name ;
		}

		public void setTherapist_phone( String therapist_phone){
			this.therapist_phone = therapist_phone;
		}
		public String getTherapist_phone(  ){
			return  this.therapist_phone ;
		}

		public void setPatient_regno( String patient_regno){
			this.patient_regno = patient_regno;
		}
		public String getPatient_regno(  ){
			return  this.patient_regno ;
		}

		public void setPatient_name( String patient_name){
			this.patient_name = patient_name;
		}
		public String getPatient_name(  ){
			return  this.patient_name ;
		}

		public void setPatient_age( int patient_age){
			this.patient_age = patient_age;
		}
		public int getPatient_age(  ){
			return  this.patient_age ;
		}

		public void setPatient_sex( String patient_sex){
			this.patient_sex = patient_sex;
		}
		public String getPatient_sex(  ){
			return  this.patient_sex ;
		}

		public void setCaller_phone( String caller_phone){
			this.caller_phone = caller_phone;
		}
		public String getCaller_phone(  ){
			return  this.caller_phone ;
		}

		public void setReferred_by( String referred_by){
			this.referred_by = referred_by;
		}
		public String getReferred_by(  ){
			return  this.referred_by ;
		}

		public void setEmployee_name( String employee_name){
			this.employee_name = employee_name;
		}
		public String getEmployee_name(  ){
			return  this.employee_name ;
		}

		public void setDescription( String description){
			this.description = description;
		}
		public String getDescription(  ){
			return  this.description ;
		}

	}

