	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Category_masterBean {

		private int  id ;

		private String  category_name ;

		private String  category_code ;

		private String  category_prefix ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setCategory_name( String category_name){
			this.category_name = category_name;
		}
		public String getCategory_name(  ){
			return  this.category_name ;
		}

		public void setCategory_code( String category_code){
			this.category_code = category_code;
		}
		public String getCategory_code(  ){
			return  this.category_code ;
		}

		public void setCategory_prefix( String category_prefix){
			this.category_prefix = category_prefix;
		}
		public String getCategory_prefix(  ){
			return  this.category_prefix ;
		}

	}

