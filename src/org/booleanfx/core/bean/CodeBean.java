 package org.booleanfx.core.bean;

 public class CodeBean {
	
	public String branch ; 
	 
	public String  bill_no ;
	
	public String  patient_regno ;
	
	public String  app_entryno ;
	
public String  category_code ;
	
	public String  item_code ;
	
	public String  referral_code ;

	public String  professional_code ;
	
	public String getCategory_code() {
		return category_code;
	}

	public void setCategory_code(String categoryCode) {
		category_code = categoryCode;
	}

	public String getItem_code() {
		return item_code;
	}

	public void setItem_code(String itemCode) {
		item_code = itemCode;
	}

	public String getReferral_code() {
		return referral_code;
	}

	public void setReferral_code(String referralCode) {
		referral_code = referralCode;
	}

	public String getProfessional_code() {
		return professional_code;
	}

	public void setProfessional_code(String professionalCode) {
		professional_code = professionalCode;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public String getBill_no() {
		return bill_no;
	}

	public void setBill_no(String billNo) {
		bill_no = billNo;
	}

	public String getPatient_regno() {
		return patient_regno;
	}

	public void setPatient_regno(String patientRegno) {
		patient_regno = patientRegno;
	}

	public String getApp_entryno() {
		return app_entryno;
	}

	public void setApp_entryno(String appEntryno) {
		app_entryno = appEntryno;
	}

 }
