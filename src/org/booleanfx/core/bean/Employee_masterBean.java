	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Employee_masterBean {

		private int  id ;

		private String  emp_no ;

		private String  username ;

		private String  password ;

		private String  firstname ;

		private String  usertype ;

		private String  middlename ;

		private String  lastname ;

		private String  mobile ;

		private String  status ;

		private String  panicmode ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setEmp_no( String emp_no){
			this.emp_no = emp_no;
		}
		public String getEmp_no(  ){
			return  this.emp_no ;
		}

		public void setUsername( String username){
			this.username = username;
		}
		public String getUsername(  ){
			return  this.username ;
		}

		public void setPassword( String password){
			this.password = password;
		}
		public String getPassword(  ){
			return  this.password ;
		}

		public void setFirstname( String firstname){
			this.firstname = firstname;
		}
		public String getFirstname(  ){
			return  this.firstname ;
		}

		public void setUsertype( String usertype){
			this.usertype = usertype;
		}
		public String getUsertype(  ){
			return  this.usertype ;
		}

		public void setMiddlename( String middlename){
			this.middlename = middlename;
		}
		public String getMiddlename(  ){
			return  this.middlename ;
		}

		public void setLastname( String lastname){
			this.lastname = lastname;
		}
		public String getLastname(  ){
			return  this.lastname ;
		}

		public void setMobile( String mobile){
			this.mobile = mobile;
		}
		public String getMobile(  ){
			return  this.mobile ;
		}

		public void setStatus( String status){
			this.status = status;
		}
		public String getStatus(  ){
			return  this.status ;
		}

		public void setPanicmode( String panicmode){
			this.panicmode = panicmode;
		}
		public String getPanicmode(  ){
			return  this.panicmode ;
		}

	}

