	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class EnquiryBean {

		private int  id ;

		private String  caller_name ;

		private Date  calling_date ;

		private String  caller_phone ;

		private String  call_priority ;

		private String  patient_name ;

		private int  patient_age ;

		private String  patient_sex ;

		private String  patient_phone ;

		private String  appointment_with ;

		private Date  appointment_date ;

		private Date  appointment_time ;

		private String  referred_by ;

		private String  status ;

		private String  description ;

		private String  patient_regno ;

		private String  username ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setCaller_name( String caller_name){
			this.caller_name = caller_name;
		}
		public String getCaller_name(  ){
			return  this.caller_name ;
		}

		public void setCalling_date( Date calling_date){
			this.calling_date = calling_date;
		}
		public Date getCalling_date(  ){
			return  this.calling_date ;
		}

		public void setCaller_phone( String caller_phone){
			this.caller_phone = caller_phone;
		}
		public String getCaller_phone(  ){
			return  this.caller_phone ;
		}

		public void setCall_priority( String call_priority){
			this.call_priority = call_priority;
		}
		public String getCall_priority(  ){
			return  this.call_priority ;
		}

		public void setPatient_name( String patient_name){
			this.patient_name = patient_name;
		}
		public String getPatient_name(  ){
			return  this.patient_name ;
		}

		public void setPatient_age( int patient_age){
			this.patient_age = patient_age;
		}
		public int getPatient_age(  ){
			return  this.patient_age ;
		}

		public void setPatient_sex( String patient_sex){
			this.patient_sex = patient_sex;
		}
		public String getPatient_sex(  ){
			return  this.patient_sex ;
		}

		public void setPatient_phone( String patient_phone){
			this.patient_phone = patient_phone;
		}
		public String getPatient_phone(  ){
			return  this.patient_phone ;
		}

		public void setAppointment_with( String appointment_with){
			this.appointment_with = appointment_with;
		}
		public String getAppointment_with(  ){
			return  this.appointment_with ;
		}

		public void setAppointment_date( Date appointment_date){
			this.appointment_date = appointment_date;
		}
		public Date getAppointment_date(  ){
			return  this.appointment_date ;
		}

		public void setAppointment_time( Date appointment_time){
			this.appointment_time = appointment_time;
		}
		public Date getAppointment_time(  ){
			return  this.appointment_time ;
		}

		public void setReferred_by( String referred_by){
			this.referred_by = referred_by;
		}
		public String getReferred_by(  ){
			return  this.referred_by ;
		}

		public void setStatus( String status){
			this.status = status;
		}
		public String getStatus(  ){
			return  this.status ;
		}

		public void setDescription( String description){
			this.description = description;
		}
		public String getDescription(  ){
			return  this.description ;
		}

		public void setPatient_regno( String patient_regno){
			this.patient_regno = patient_regno;
		}
		public String getPatient_regno(  ){
			return  this.patient_regno ;
		}

		public void setUsername( String username){
			this.username = username;
		}
		public String getUsername(  ){
			return  this.username ;
		}

	}

