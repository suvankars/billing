	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Expense_registerBean {

		private int  id ;

		private Date  voucher_date ;

		private String  voucher_no ;

		private String  narration ;

		private double  amount ;

		private String  approved_by ;

		private String  remarks ;

		private String  username ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setVoucher_date( Date voucher_date){
			this.voucher_date = voucher_date;
		}
		public Date getVoucher_date(  ){
			return  this.voucher_date ;
		}

		public void setVoucher_no( String voucher_no){
			this.voucher_no = voucher_no;
		}
		public String getVoucher_no(  ){
			return  this.voucher_no ;
		}

		public void setNarration( String narration){
			this.narration = narration;
		}
		public String getNarration(  ){
			return  this.narration ;
		}

		public void setAmount( double amount){
			this.amount = amount;
		}
		public double getAmount(  ){
			return  this.amount ;
		}

		public void setApproved_by( String approved_by){
			this.approved_by = approved_by;
		}
		public String getApproved_by(  ){
			return  this.approved_by ;
		}

		public void setRemarks( String remarks){
			this.remarks = remarks;
		}
		public String getRemarks(  ){
			return  this.remarks ;
		}

		public void setUsername( String username){
			this.username = username;
		}
		public String getUsername(  ){
			return  this.username ;
		}

	}

