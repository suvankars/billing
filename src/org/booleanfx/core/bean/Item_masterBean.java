	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Item_masterBean {

		private int  id ;

		private String  category_name ;

		private String  item_code_prefix ;

		private String  item_code ;

		private String  item_name ;

		private double  item_rate ;

		private String  item_tax_type ;

		private double  item_tax_percent ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setCategory_name( String category_name){
			this.category_name = category_name;
		}
		public String getCategory_name(  ){
			return  this.category_name ;
		}

		public void setItem_code_prefix( String item_code_prefix){
			this.item_code_prefix = item_code_prefix;
		}
		public String getItem_code_prefix(  ){
			return  this.item_code_prefix ;
		}

		public void setItem_code( String item_code){
			this.item_code = item_code;
		}
		public String getItem_code(  ){
			return  this.item_code ;
		}

		public void setItem_name( String item_name){
			this.item_name = item_name;
		}
		public String getItem_name(  ){
			return  this.item_name ;
		}

		public void setItem_rate( double item_rate){
			this.item_rate = item_rate;
		}
		public double getItem_rate(  ){
			return  this.item_rate ;
		}

		public void setItem_tax_type( String item_tax_type){
			this.item_tax_type = item_tax_type;
		}
		public String getItem_tax_type(  ){
			return  this.item_tax_type ;
		}

		public void setItem_tax_percent( double item_tax_percent){
			this.item_tax_percent = item_tax_percent;
		}
		public double getItem_tax_percent(  ){
			return  this.item_tax_percent ;
		}

	}

