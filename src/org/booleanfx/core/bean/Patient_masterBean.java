	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Patient_masterBean {

		private int  id ;

		private String  reg_no ;

		private String  name ;

		private String  age ;

		private String  sex ;

		private String  telephone ;

		private String  mobile ;

		private String  category ;

		private String  status ;

		private String  priority ;

		private double  reg_charges ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setReg_no( String reg_no){
			this.reg_no = reg_no;
		}
		public String getReg_no(  ){
			return  this.reg_no ;
		}

		public void setName( String name){
			this.name = name;
		}
		public String getName(  ){
			return  this.name ;
		}

		public void setAge( String age){
			this.age = age;
		}
		public String getAge(  ){
			return  this.age ;
		}

		public void setSex( String sex){
			this.sex = sex;
		}
		public String getSex(  ){
			return  this.sex ;
		}

		public void setTelephone( String telephone){
			this.telephone = telephone;
		}
		public String getTelephone(  ){
			return  this.telephone ;
		}

		public void setMobile( String mobile){
			this.mobile = mobile;
		}
		public String getMobile(  ){
			return  this.mobile ;
		}

		public void setCategory( String category){
			this.category = category;
		}
		public String getCategory(  ){
			return  this.category ;
		}

		public void setStatus( String status){
			this.status = status;
		}
		public String getStatus(  ){
			return  this.status ;
		}

		public void setPriority( String priority){
			this.priority = priority;
		}
		public String getPriority(  ){
			return  this.priority ;
		}

		public void setReg_charges( double reg_charges){
			this.reg_charges = reg_charges;
		}
		public double getReg_charges(  ){
			return  this.reg_charges ;
		}

	}

