	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Payment_registerBean {

		private int  id ;

		private String  payment_register_type ;

		private int  payment_register_name ;

		private int  payment_register_cell_no ;

		private String  payment_register_address ;

		private int  category_name_id ;

		private int  item_name_id ;

		private Date  from_date ;

		private Date  to_date ;

		private String  volume_payment_register ;

		private double  percent_payment_register ;

		private double  commission_amount ;

		private double  due_amount ;

		private double  amount_paid ;
		
		private Date  entry_date ;

		public void setId( int id){
			this.id = id;
		}
		public int getId(  ){
			return  this.id ;
		}

		public void setPayment_register_type( String payment_register_type){
			this.payment_register_type = payment_register_type;
		}
		public String getPayment_register_type(  ){
			return  this.payment_register_type ;
		}

		public void setPayment_register_name( int payment_register_name){
			this.payment_register_name = payment_register_name;
		}
		public int getPayment_register_name(  ){
			return  this.payment_register_name ;
		}

		public void setPayment_register_cell_no( int payment_register_cell_no){
			this.payment_register_cell_no = payment_register_cell_no;
		}
		public int getPayment_register_cell_no(  ){
			return  this.payment_register_cell_no ;
		}

		

		public void setCategory_name_id( int category_name_id){
			this.category_name_id = category_name_id;
		}
		public int getCategory_name_id(  ){
			return  this.category_name_id ;
		}

		public void setItem_name_id( int item_name_id){
			this.item_name_id = item_name_id;
		}
		public int getItem_name_id(  ){
			return  this.item_name_id ;
		}

		public void setFrom_date( Date from_date){
			this.from_date = from_date;
		}
		public Date getFrom_date(  ){
			return  this.from_date ;
		}

		public void setTo_date( Date to_date){
			this.to_date = to_date;
		}
		public Date getTo_date(  ){
			return  this.to_date ;
		}

		public void setVolume_payment_register( String volume_payment_register){
			this.volume_payment_register = volume_payment_register;
		}
		public String getVolume_payment_register(  ){
			return  this.volume_payment_register ;
		}

		public void setPercent_payment_register( double percent_payment_register){
			this.percent_payment_register = percent_payment_register;
		}
		public double getPercent_payment_register(  ){
			return  this.percent_payment_register ;
		}

		public void setCommission_amount( double commission_amount){
			this.commission_amount = commission_amount;
		}
		public double getCommission_amount(  ){
			return  this.commission_amount ;
		}

		public void setDue_amount( double due_amount){
			this.due_amount = due_amount;
		}
		public double getDue_amount(  ){
			return  this.due_amount ;
		}

		public void setAmount_paid( double amount_paid){
			this.amount_paid = amount_paid;
		}
		public double getAmount_paid(  ){
			return  this.amount_paid ;
		}
		public void setPayment_register_address(String payment_register_address) {
			this.payment_register_address = payment_register_address;
		}
		public String getPayment_register_address() {
			return payment_register_address;
		}
		public void setEntry_date(Date entry_date) {
			this.entry_date = entry_date;
		}
		public Date getEntry_date() {
			return entry_date;
		}

	}

