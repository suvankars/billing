	package org.booleanfx.core.bean ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	public class Professional_masterBean {

		private int  id ;

		private String  professional_code ;

		private String  professional_name ;

		private String  professional_cell1 ;

		private String  professional_cell2 ;

		private String  professional_address ;

		private String  professional_address_telephone ;

		private int  category_name_id ;

		private int  item_name_id ;

		private String  target_volume1 ;

		private String  target_volume_commission_percent1 ;

		private String  target_volume2 ;

		private String  target_volume_commission2 ;

		private String  target_volume3 ;

		private String  target_volume_commission3 ;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getProfessional_code() {
			return professional_code;
		}

		public void setProfessional_code(String professionalCode) {
			professional_code = professionalCode;
		}

		public String getProfessional_name() {
			return professional_name;
		}

		public void setProfessional_name(String professionalName) {
			professional_name = professionalName;
		}

		public String getProfessional_cell1() {
			return professional_cell1;
		}

		public void setProfessional_cell1(String professionalCell1) {
			professional_cell1 = professionalCell1;
		}

		public String getProfessional_cell2() {
			return professional_cell2;
		}

		public void setProfessional_cell2(String professionalCell2) {
			professional_cell2 = professionalCell2;
		}

		public String getProfessional_address() {
			return professional_address;
		}

		public void setProfessional_address(String professionalAddress) {
			professional_address = professionalAddress;
		}

		public String getProfessional_address_telephone() {
			return professional_address_telephone;
		}

		public void setProfessional_address_telephone(
				String professionalAddressTelephone) {
			professional_address_telephone = professionalAddressTelephone;
		}

		public int getCategory_name_id() {
			return category_name_id;
		}

		public void setCategory_name_id(int categoryNameId) {
			category_name_id = categoryNameId;
		}

		public int getItem_name_id() {
			return item_name_id;
		}

		public void setItem_name_id(int itemNameId) {
			item_name_id = itemNameId;
		}

		public String getTarget_volume1() {
			return target_volume1;
		}

		public void setTarget_volume1(String targetVolume1) {
			target_volume1 = targetVolume1;
		}

		public String getTarget_volume_commission_percent1() {
			return target_volume_commission_percent1;
		}

		public void setTarget_volume_commission_percent1(
				String targetVolumeCommissionPercent1) {
			target_volume_commission_percent1 = targetVolumeCommissionPercent1;
		}

		public String getTarget_volume2() {
			return target_volume2;
		}

		public void setTarget_volume2(String targetVolume2) {
			target_volume2 = targetVolume2;
		}

		public String getTarget_volume_commission2() {
			return target_volume_commission2;
		}

		public void setTarget_volume_commission2(String targetVolumeCommission2) {
			target_volume_commission2 = targetVolumeCommission2;
		}

		public String getTarget_volume3() {
			return target_volume3;
		}

		public void setTarget_volume3(String targetVolume3) {
			target_volume3 = targetVolume3;
		}

		public String getTarget_volume_commission3() {
			return target_volume_commission3;
		}

		public void setTarget_volume_commission3(String targetVolumeCommission3) {
			target_volume_commission3 = targetVolumeCommission3;
		}

	
	}

