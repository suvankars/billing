	package org.booleanfx.core.bean ;

	import java.sql.Date ;

import java.sql.Timestamp ;

	public class Referrer_masterBean {

		private int  id ;

		private String  referrer_code ;

		private String  referrer_name ;

		private String  referrer_mobile1 ;

		private String  referrer_address ;

		private String  referrer_telephone ;

		private int  category_id ;

		private int  item_id ;

		private String  target_volume1 ;

		private String  target_commission1 ;

		private String  referrer_mobile2 ;

		private String  target_volume2 ;

		private String  target_commission2 ;

		private String  target_volume3 ;

		private String  target_commission3 ;

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}

		public String getReferrer_code() {
			return referrer_code;
		}

		public void setReferrer_code(String referrerCode) {
			referrer_code = referrerCode;
		}

		public String getReferrer_name() {
			return referrer_name;
		}

		public void setReferrer_name(String referrerName) {
			referrer_name = referrerName;
		}

		public String getReferrer_mobile1() {
			return referrer_mobile1;
		}

		public void setReferrer_mobile1(String referrerMobile1) {
			referrer_mobile1 = referrerMobile1;
		}

		public String getReferrer_address() {
			return referrer_address;
		}

		public void setReferrer_address(String referrerAddress) {
			referrer_address = referrerAddress;
		}

		public String getReferrer_telephone() {
			return referrer_telephone;
		}

		public void setReferrer_telephone(String referrerTelephone) {
			referrer_telephone = referrerTelephone;
		}

		public int getCategory_id() {
			return category_id;
		}

		public void setCategory_id(int categoryId) {
			category_id = categoryId;
		}

		public int getItem_id() {
			return item_id;
		}

		public void setItem_id(int itemId) {
			item_id = itemId;
		}

		public String getTarget_volume1() {
			return target_volume1;
		}

		public void setTarget_volume1(String targetVolume1) {
			target_volume1 = targetVolume1;
		}

		public String getTarget_commission1() {
			return target_commission1;
		}

		public void setTarget_commission1(String targetCommission1) {
			target_commission1 = targetCommission1;
		}

		public String getReferrer_mobile2() {
			return referrer_mobile2;
		}

		public void setReferrer_mobile2(String referrerMobile2) {
			referrer_mobile2 = referrerMobile2;
		}

		public String getTarget_volume2() {
			return target_volume2;
		}

		public void setTarget_volume2(String targetVolume2) {
			target_volume2 = targetVolume2;
		}

		public String getTarget_commission2() {
			return target_commission2;
		}

		public void setTarget_commission2(String targetCommission2) {
			target_commission2 = targetCommission2;
		}

		public String getTarget_volume3() {
			return target_volume3;
		}

		public void setTarget_volume3(String targetVolume3) {
			target_volume3 = targetVolume3;
		}

		public String getTarget_commission3() {
			return target_commission3;
		}

		public void setTarget_commission3(String targetCommission3) {
			target_commission3 = targetCommission3;
		}

	
	}

