	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

	import org.booleanfx.core.dao.* ;

	public class Access_matrixComposer extends GenericForwardComposer{

			//Define Common members here atin

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack, mPrint;

			Listbox listbox;

			Label editId;

			Grid zformgrid;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			Textbox id ;

			Textbox resourcecategory ;

			Textbox resourcename ;

			Textbox view ;

			Textbox save ;

			Textbox edit ;

			Textbox delete ;

			Textbox list ;

			Textbox print ;

			Textbox employeeid ;

			Access_matrixDAO daoHandler = new Access_matrixDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				mPrint.setDisabled(true);

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				mPrint.setDisabled(true);

				mode = "add";

				selectedId = -1 ;

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Access_matrixBean webbean = new Access_matrixBean();

				try{

					webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setResourcecategory(resourcecategory.getValue());

					webbean.setResourcename(resourcename.getValue());

					webbean.setView(view.getValue());

					webbean.setSave(save.getValue());

					webbean.setEdit(edit.getValue());

					webbean.setDelete(delete.getValue());

					webbean.setList(list.getValue());

					webbean.setPrint(print.getValue());

					webbean.setEmployeeid(Integer.parseInt(employeeid.getValue()));

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

						listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					mPrint.setDisabled(true);

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

				mPrint.setDisabled(true);

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);

				mPrint.setDisabled(false);

				Access_matrixBean webbean  = (Access_matrixBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Access_matrixBean webbean  = (Access_matrixBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					id.setValue(""+webbean.getId());

					resourcecategory.setValue(webbean.getResourcecategory());

					resourcename.setValue(webbean.getResourcename());

					view.setValue(webbean.getView());

					save.setValue(webbean.getSave());

					edit.setValue(webbean.getEdit());

					delete.setValue(webbean.getDelete());

					list.setValue(webbean.getList());

					print.setValue(webbean.getPrint());

					employeeid.setValue(""+webbean.getEmployeeid());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Access_matrixBean webbean  = (Access_matrixBean)listbox.getSelectedItem().getValue();

					int result = Messagebox.show("Delete is pressed. Are you sure?", "Question", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION);

					if(result == Messagebox.OK){

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

						listbox.setModel(new ListModelList(daoHandler.findAll()));

					}

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				id.setValue("");

				resourcecategory.setValue("");

				resourcename.setValue("");

				view.setValue("");

				save.setValue("");

				edit.setValue("");

				delete.setValue("");

				list.setValue("");

				print.setValue("");

				employeeid.setValue("");

				listbox.setSelectedItem(null);

			}

	}

