	package org.booleanfx.core.composer ;



import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.GenericForwardComposer;


	

import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
	import org.zkoss.zul.Datebox;
import org.zkoss.zul.Intbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

import org.booleanfx.core.dao.* ;

	public class Appointment_registerComposer extends GenericForwardComposer{

			//Define Common members here
		Referrer_masterDAO rmd=new Referrer_masterDAO();
		
		Professional_masterDAO masterDAO = new Professional_masterDAO();
		
			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Grid zformgrid;
			
			Grid zformgrid2;
			
			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox entry_no ;

			Datebox entry_date ;

			Datebox appointment_date ;
			
			Textbox searchname;
			
			Combobox therapist_name ;
			
			Combobox type ;

			Textbox therapist_phone ;

			//Textbox patient_regno ;

			Textbox patient_name ;

			Intbox patient_age ;

			Combobox patient_sex ;

			Intbox caller_phone ;

			Combobox referred_by ;

			Textbox description ;
			
			Textbox employee_name;

			Button search;
			
			Appointment_registerDAO daoHandler = new Appointment_registerDAO();

			CodeDAO codeDAO = new CodeDAO() ;
			
			public void onCreate$main() throws Exception {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				//mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				 

				report.setVisible(false);
				
				employee_name.setReadonly(true);
				
				patient_sex.appendItem("male");
				
				patient_sex.appendItem("female");
				
				Referrer_masterDAO rmd=new Referrer_masterDAO();
				
				Professional_masterDAO masterDAO = new Professional_masterDAO();
				
				referred_by.setModel(new ListModelList(rmd.findAll()));

				therapist_name.setModel(new ListModelList(masterDAO.findAll()));
				selectedId = -1 ;
				
				
			}

			public void onClick$mNew() throws WrongValueException, Exception {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				 
				
				therapist_name.setModel(new ListModelList(masterDAO.findAll()));
				
                employee_name.setValue(((Employee_masterBean)session.getAttribute("logininfo")).getUsername());		

				mode = "add";

				selectedId = -1 ;
				
				if(UUIDGenerator.app_entryno == null){
					
					entry_no.setValue(UUIDGenerator.getInstance().getFirstTimeAppointment_entryNo());
					
				}else{
					
					entry_no.setValue(UUIDGenerator.getInstance().getNextAppointment_entryNo());
				}


				codeDAO.updateEntry_No();
				
                 entry_no.setReadonly(true);
			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {
               
                	

                
				Appointment_registerBean webbean = new Appointment_registerBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setEntry_no(entry_no.getValue());

					webbean.setEntry_date(new java.sql.Date(entry_date.getValue().getTime()));

					webbean.setAppointment_date(new java.sql.Timestamp(appointment_date.getValue().getTime()));

					webbean.setTherapist_name(therapist_name.getSelectedItem().getLabel());

					webbean.setTherapist_phone(therapist_phone.getValue());

					//webbean.setPatient_regno(patient_regno.getValue());

					webbean.setPatient_name(patient_name.getValue());

					webbean.setPatient_age(patient_age.getValue());

					webbean.setPatient_sex(patient_sex.getSelectedItem().getLabel());

					webbean.setCaller_phone(""+caller_phone.getValue());

					webbean.setReferred_by(referred_by.getSelectedItem().getLabel());

					webbean.setDescription(description.getValue());
					
					webbean.setEmployee_name(employee_name.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);
						
						Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					}else{
                        
					    if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
					    	webbean.setId(selectedId);
    
					        daoHandler.updateAllCols(webbean);
					        
					    	Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);
					        
					    }else{
					    	
					    	alert("you dont have enough privilege for this action");
					    }

					}

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					 

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

                	
                }

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

				 

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				Appointment_registerBean webbean  = (Appointment_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Appointment_registerBean webbean  = (Appointment_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					entry_no.setValue(webbean.getEntry_no());

					entry_date.setValue(webbean.getEntry_date());

					appointment_date.setValue(webbean.getAppointment_date());

					therapist_name.setValue(webbean.getTherapist_name());

					therapist_phone.setValue(webbean.getTherapist_phone());

					//patient_regno.setValue(webbean.getPatient_regno());

					patient_name.setValue(webbean.getPatient_name());

					patient_age.setValue(webbean.getPatient_age());

					patient_sex.setValue(webbean.getPatient_sex());

					caller_phone.setValue(Integer.parseInt(webbean.getCaller_phone()));

					referred_by.setValue(webbean.getReferred_by());

					description.setValue(webbean.getDescription());
					
					employee_name.setValue(webbean.getEmployee_name());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Appointment_registerBean webbean  = (Appointment_registerBean)listbox.getSelectedItem().getValue();

				
					  if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
						  selectedId = webbean.getId();

					      daoHandler.deleteByid(selectedId);

					      listbox.setModel(new ListModelList(daoHandler.findAll()));
					       
					  }else{
					    	
					    	alert("you dont have enough privilege for this action");
					   }
				

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				entry_no.setValue("");		

				therapist_name.setValue("");

				therapist_phone.setValue("");

				//patient_regno.setValue("");

				patient_name.setValue("");

				patient_age.setValue(0);

				patient_sex.setValue("");

				caller_phone.setValue(0);

				referred_by.setValue("");

				description.setValue("");
				
				employee_name.setValue("");

				listbox.setSelectedItem(null);

			}

			
		
			
			public void onClick$search() {

				try {
					if(type.getSelectedItem()== null){
						alert("Enter some Search Name Or Date");
					}else{
						
						if(type.getSelectedItem().getLabel().equals("Professional Name")){
							
							listbox.setModel(new ListModelList(daoHandler.findBytherapist_name(searchname.getValue())));
							
						}else if(type.getSelectedItem().getLabel().equals("Appointment Date(yyyy-mm-dd)")){
							
							String date_content = searchname.getValue();
							
							String format[] = date_content.split("-");
							
							if(format[0].length() != 4 ||format[1].length() != 2 || format[2].length() != 2 ){
								
								alert("Date format is Proper");
								
							}else{
							
								listbox.setModel(new ListModelList(daoHandler.findByappointment_date(date_content+"%")));
								
							}
							
						}
					}
						
					
					

				} catch (Exception e) {

					e.printStackTrace();

				}

				searchname.setValue("");
				
			}
			
	}

