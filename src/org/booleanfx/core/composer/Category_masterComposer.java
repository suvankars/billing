	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Category_masterComposer extends GenericForwardComposer{

			//Define Common members here

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			int value ;
			
			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox category_name ;

			Textbox category_code ;

			Textbox category_prefix ;

			CodeDAO codeDAO = new CodeDAO();
			Category_masterDAO daoHandler = new Category_masterDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				  

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew() throws WrongValueException, Exception{

				//formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				  

				mode = "add";

				selectedId = -1 ;
				if(UUIDGenerator.category_code == null){
					
					value = Integer.parseInt(UUIDGenerator.getInstance().getFirstTimecategory_code());
					
				}else{
					
					value = Integer.parseInt(UUIDGenerator.getInstance().getNextcategory_code());
				}


				codeDAO.updateCategorycode();
				

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Category_masterBean webbean = new Category_masterBean();

				try{

					webbean.setCategory_name(category_name.getValue());
					webbean.setCategory_code(category_prefix.getValue()+value);
					webbean.setCategory_prefix(category_prefix.getValue());

					
					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					  

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				report.setVisible(false);

				  

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);

				

				Category_masterBean webbean  = (Category_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit() throws NumberFormatException, Exception{

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Category_masterBean webbean  = (Category_masterBean)listbox.getSelectedItem().getValue();

				
				selectedId = webbean.getId();

				String code=(webbean.getCategory_code());
				String codes =code.substring(3, code.length());
				value = Integer.parseInt(codes);
				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					category_name.setValue(webbean.getCategory_name());

					category_prefix.setValue(webbean.getCategory_prefix());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Category_masterBean webbean  = (Category_masterBean)listbox.getSelectedItem().getValue();

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

					alert("Data deleted successfully");

						listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				category_name.setValue("");

				//category_code.setValue("");

				category_prefix.setValue("");

				listbox.setSelectedItem(null);

			}

	}

