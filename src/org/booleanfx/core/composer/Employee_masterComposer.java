	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Employee_masterComposer extends GenericForwardComposer{

			//Define Common members here

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Grid zformgrid;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox emp_no ;

			Textbox username ;

			Textbox password ;
			
			Combobox usertype;

			Textbox firstname ;

			Textbox middlename ;

			Textbox lastname ;

			Textbox mobile ;

			Textbox status ;

			Textbox panicmode ;

			Employee_masterDAO daoHandler = new Employee_masterDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

			

				report.setVisible(false);
				
				selectedId = -1 ;

			}

			public void onClick$mNew() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

			

				mode = "add";

				selectedId = -1 ;
				
				

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Employee_masterBean webbean = new Employee_masterBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setEmp_no(emp_no.getValue());

					webbean.setUsername(username.getValue());

					webbean.setPassword(password.getValue());
					
					webbean.setUsertype(usertype.getSelectedItem().getLabel());

					webbean.setFirstname(firstname.getValue());

					webbean.setMiddlename(middlename.getValue());

					webbean.setLastname(lastname.getValue());

					webbean.setMobile(mobile.getValue());

					webbean.setStatus(status.getValue());

					webbean.setPanicmode(panicmode.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);
						
						Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					}else{
                        
					    if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
					    	webbean.setId(selectedId);
    
					        daoHandler.updateAllCols(webbean);
					        
					    	Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);
					        
					    }else{
					    	
					    	alert("you dont have enough privilege for this action");
					    }

					}

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);


					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

			

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);

			

				Employee_masterBean webbean  = (Employee_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Employee_masterBean webbean  = (Employee_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					emp_no.setValue(webbean.getEmp_no());

					username.setValue(webbean.getUsername());

					password.setValue(webbean.getPassword());
					
					usertype.setValue(webbean.getUsertype());

					firstname.setValue(webbean.getFirstname());

					middlename.setValue(webbean.getMiddlename());

					lastname.setValue(webbean.getLastname());

					mobile.setValue(webbean.getMobile());

					status.setValue(webbean.getStatus());

					panicmode.setValue(webbean.getPanicmode());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Employee_masterBean webbean  = (Employee_masterBean)listbox.getSelectedItem().getValue();


					  if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
						  selectedId = webbean.getId();

					      daoHandler.deleteByid(selectedId);

					      listbox.setModel(new ListModelList(daoHandler.findAll()));
					       
					  }else{
					    	
					    	alert("you dont have enough privilege for this action");
					   }

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				emp_no.setValue("");

				username.setValue("");

				password.setValue("");
				
				usertype.setValue("");

				firstname.setValue("");

				middlename.setValue("");

				lastname.setValue("");

				mobile.setValue("");

				status.setValue("");

				panicmode.setValue("");

				listbox.setSelectedItem(null);

			}

	}

