	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class EnquiryComposer extends GenericForwardComposer{

			//Define Common members here

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Grid zformgrid;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox caller_name ;

			Datebox calling_date ;

			Textbox caller_phone ;

			Textbox call_priority ;

			Textbox patient_name ;

			Textbox patient_age ;

			Combobox patient_sex ;

			Textbox patient_phone ;

			Textbox appointment_with ;

			Datebox appointment_date ;

			Datebox appointment_time ;

			Combobox referred_by ;

			Textbox status ;

			Textbox description ;

			Textbox patient_regno ;

			Textbox username ;

			EnquiryDAO daoHandler = new EnquiryDAO();

			public void onCreate$main() throws Exception {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);


				report.setVisible(false);
				
                patient_sex.appendItem("male");
				
				patient_sex.appendItem("female");
				
	            Referrer_masterDAO rmd=new Referrer_masterDAO();
				
				referred_by.setModel(new ListModelList(rmd.findAll()));

				selectedId = -1 ;

			}

			public void onClick$mNew() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

			 

				mode = "add";

				selectedId = -1 ;

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				EnquiryBean webbean = new EnquiryBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setCaller_name(caller_name.getValue());

					webbean.setCalling_date(new java.sql.Date(calling_date.getValue().getTime()));

					webbean.setCaller_phone(caller_phone.getValue());

					webbean.setCall_priority(call_priority.getValue());

					webbean.setPatient_name(patient_name.getValue());

					webbean.setPatient_age(Integer.parseInt(patient_age.getValue()));

					webbean.setPatient_sex(patient_sex.getSelectedItem().getLabel());

					webbean.setPatient_phone(patient_phone.getValue());

					webbean.setAppointment_with(appointment_with.getValue());

					webbean.setAppointment_date(new java.sql.Date(appointment_date.getValue().getTime()));

					webbean.setAppointment_time(new java.sql.Date(appointment_time.getValue().getTime()));

					webbean.setReferred_by(referred_by.getSelectedItem().getLabel());

					webbean.setStatus(status.getValue());

					webbean.setDescription(description.getValue());

					webbean.setPatient_regno(patient_regno.getValue());

					webbean.setUsername(username.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);
						
						Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					}else{
                        
					    if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
					    	webbean.setId(selectedId);
    
					        daoHandler.updateAllCols(webbean);
					        
					    	Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);
					        
					    }else{
					    	
					    	alert("you dont have enough privilege for this action");
					    }

					}

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

				 

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

			 

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				EnquiryBean webbean  = (EnquiryBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				EnquiryBean webbean  = (EnquiryBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					caller_name.setValue(webbean.getCaller_name());

					calling_date.setValue(webbean.getCalling_date());

					caller_phone.setValue(webbean.getCaller_phone());

					call_priority.setValue(webbean.getCall_priority());

					patient_name.setValue(webbean.getPatient_name());

					patient_age.setValue(""+webbean.getPatient_age());

					patient_sex.setValue(webbean.getPatient_sex());

					patient_phone.setValue(webbean.getPatient_phone());

					appointment_with.setValue(webbean.getAppointment_with());

					appointment_date.setValue(webbean.getAppointment_date());

					appointment_time.setValue(webbean.getAppointment_time());

					referred_by.setValue(webbean.getReferred_by());

					status.setValue(webbean.getStatus());

					description.setValue(webbean.getDescription());

					patient_regno.setValue(webbean.getPatient_regno());

					username.setValue(webbean.getUsername());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					EnquiryBean webbean  = (EnquiryBean)listbox.getSelectedItem().getValue();


					  if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
						  selectedId = webbean.getId();

					      daoHandler.deleteByid(selectedId);

					      listbox.setModel(new ListModelList(daoHandler.findAll()));
					       
					  }else{
					    	
					    	alert("you dont have enough privilege for this action");
					   }

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				caller_name.setValue("");

				

				caller_phone.setValue("");

				call_priority.setValue("");

				patient_name.setValue("");

				patient_age.setValue("");

				patient_sex.setValue("");

				patient_phone.setValue("");

				appointment_with.setValue("");

				

				

				referred_by.setValue("");

				status.setValue("");

				description.setValue("");

				patient_regno.setValue("");

				username.setValue("");

				listbox.setSelectedItem(null);

			}

	}

