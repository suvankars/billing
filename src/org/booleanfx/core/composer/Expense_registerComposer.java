	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Expense_registerComposer extends GenericForwardComposer{

			//Define Common members here

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Grid zformgrid;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Datebox voucher_date ;

			Textbox voucher_no ;

			Textbox narration ;

			Decimalbox amount ;

			Textbox approved_by ;

			Textbox remarks ;

			Textbox username ;

			Expense_registerDAO daoHandler = new Expense_registerDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				   

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				   

				mode = "add";

				selectedId = -1 ;

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Expense_registerBean webbean = new Expense_registerBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setVoucher_date(new java.sql.Date(voucher_date.getValue().getTime()));

					webbean.setVoucher_no(voucher_no.getValue());

					webbean.setNarration(narration.getValue());

					webbean.setAmount(amount.getValue().doubleValue());

					webbean.setApproved_by(approved_by.getValue());

					webbean.setRemarks(remarks.getValue());

					webbean.setUsername(username.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);
						
						Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					}else{
                        
					    if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
					    	webbean.setId(selectedId);
    
					        daoHandler.updateAllCols(webbean);
					        
					    	Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);
					        
					    }else{
					    	
					    	alert("you dont have enough privilege for this action");
					    }

					}

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					   

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

				   

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				Expense_registerBean webbean  = (Expense_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Expense_registerBean webbean  = (Expense_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					voucher_date.setValue(webbean.getVoucher_date());

					voucher_no.setValue(webbean.getVoucher_no());

					narration.setValue(webbean.getNarration());

					amount.setValue(new BigDecimal(webbean.getAmount()));

					approved_by.setValue(webbean.getApproved_by());

					remarks.setValue(webbean.getRemarks());

					username.setValue(webbean.getUsername());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Expense_registerBean webbean  = (Expense_registerBean)listbox.getSelectedItem().getValue();

					if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
						  selectedId = webbean.getId();

					      daoHandler.deleteByid(selectedId);

					      listbox.setModel(new ListModelList(daoHandler.findAll()));
					       
					  }else{
					    	
					    	alert("you dont have enough privilege for this action");
					   }

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				

				voucher_no.setValue("");

				narration.setValue("");

				amount.setValue(new BigDecimal(0.00));

				approved_by.setValue("");

				remarks.setValue("");

				username.setValue("");

				listbox.setSelectedItem(null);

			}

	}

