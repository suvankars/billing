package org.booleanfx.core.composer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.booleanfx.core.bean.Acc_patient_bill_headerBean;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Iframe;

import com.domains.servlets.ConstantServlet;

public class IncomeExpenditureReportComposer extends GenericForwardComposer {


Button show;
	
	Datebox fromdate,todate;
	
	Iframe report;
	
	public void onClick$show(){
		
		
		  //for bill generation
		
		JasperReport jasperReport;
		
		JasperPrint jasperPrint;
		
		String contextPath = ConstantServlet.contextPath ;
		
			try {
				
				
										
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				
				Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/mammothshq","root","samsung123");
											
				Map parameters = new HashMap();
				
				parameters.put("Title", "PDF");
				
				parameters.put("todate", todate.getValue());
				
				parameters.put("fromdate",fromdate.getValue());
										  
				jasperReport = JasperCompileManager.compileReport(contextPath+"reports/incomeexpendaturereport.jrxml");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,conn);
				
				JasperExportManager.exportReportToPdfFile(jasperPrint, contextPath+"reports/IncomeExpenditure.pdf");
				
				JasperExportManager.exportReportToPdf(jasperPrint);
				
				
				
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			
			FileInputStream fin = null;

			try {
				
				/*  1. Remove the hard-coding filepath
				  2. Create a constant servlet to get the realpath
				  3. Create the PDF report from the code.*/
			
				
				File file = new File(contextPath + "reports/IncomeExpenditure.pdf");

				fin = new FileInputStream(file);

				int bufsize = (int) file.length();

				byte[] buf = new byte[bufsize];

				fin.read(buf);

				// prepare the AMedia for iframe
				final InputStream mediais = new ByteArrayInputStream(buf);

				final AMedia amedia = new AMedia("IncomeExpenditure.pdf", "pdf",
						"application/pdf", mediais);

				// set iframe content
                if(file!=null){
				report.setContent(amedia);
                }

			} catch (Exception ex) {
				throw new RuntimeException(ex);
			} finally {
				if (fin != null) {

					try {
						fin.close();
					} catch (Exception e) {
					}
				}
			}

		
	}
	




}
