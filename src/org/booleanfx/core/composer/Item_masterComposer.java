	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Item_masterComposer extends GenericForwardComposer{

			//Define Common members here
		Category_masterDAO categoryMasterDAO = new Category_masterDAO();		
		
			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;
			int value ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Combobox category_name ;
			

			Textbox item_code_prefix ;

			Textbox item_code ;

			Textbox item_name ;

			Decimalbox item_rate ;

			Textbox item_tax_type ;

			Decimalbox item_tax_percent ;

			CodeDAO codeDAO = new CodeDAO();
			Item_masterDAO daoHandler = new Item_masterDAO();

			public void onCreate$main() throws Exception {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				   

				report.setVisible(false);

				selectedId = -1 ;
				
				category_name.setModel(new ListModelList(categoryMasterDAO.findAll()));


			}

			public void onClick$mNew()throws WrongValueException, Exception {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				   

				mode = "add";

				selectedId = -1 ;

				
				if(UUIDGenerator.item_code == null){
				
				value = Integer.parseInt(UUIDGenerator.getInstance().getFirstTimeitem_code());
				
			}else{
				
				value = Integer.parseInt(UUIDGenerator.getInstance().getNextitem_code());
			}


			codeDAO.updateItemcode();
			

		}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Item_masterBean webbean = new Item_masterBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setCategory_name(category_name.getSelectedItem().getLabel());

					webbean.setItem_code_prefix(item_code_prefix.getValue());

					webbean.setItem_code(item_code_prefix.getValue() + value);

					webbean.setItem_name(item_name.getValue());

					webbean.setItem_rate(item_rate.getValue().doubleValue());
					
					webbean.setItem_tax_type(item_tax_type.getValue());
					
					if(item_tax_percent.getValue().doubleValue() == 0){
						
						webbean.setItem_tax_percent(0.0);
					}else{
						webbean.setItem_tax_percent(item_tax_percent.getValue().doubleValue());
					}

					

					

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					   

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				report.setVisible(false);

				   

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				Item_masterBean webbean  = (Item_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Item_masterBean webbean  = (Item_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				

				String code=(webbean.getItem_code());
				String codes =code.substring(3, code.length());
				value = Integer.parseInt(codes);
				mode = "edit";

				try{

				//	id.setValue(""+webbean.getId());

					category_name.setValue(webbean.getCategory_name());

					item_code_prefix.setValue(webbean.getItem_code_prefix());
					

					item_name.setValue(webbean.getItem_name());

					item_rate.setValue(new BigDecimal(webbean.getItem_rate()));
					

					item_tax_type.setValue(webbean.getItem_tax_type());

					item_tax_percent.setValue(new BigDecimal(webbean.getItem_tax_percent()));

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Item_masterBean webbean  = (Item_masterBean)listbox.getSelectedItem().getValue();

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

					alert("Data deleted successfully");

						listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				category_name.setValue("");

				item_code_prefix.setValue("");

				//item_code.setValue("");

				item_name.setValue("");

				item_rate.setValue(new BigDecimal(0.00));

				item_tax_type.setValue("");

				item_tax_percent.setValue(new BigDecimal(0.00));

				listbox.setSelectedItem(null);

			}

	}

