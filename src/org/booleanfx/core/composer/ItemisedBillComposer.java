package org.booleanfx.core.composer;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.math.BigDecimal;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.booleanfx.core.bean.Acc_patient_bill_detailsBean;
import org.booleanfx.core.bean.Acc_patient_bill_headerBean;
import org.booleanfx.core.bean.Category_masterBean;
import org.booleanfx.core.bean.Employee_masterBean;
import org.booleanfx.core.bean.Item_masterBean;
import org.booleanfx.core.dao.Acc_patient_bill_detailsDAO;
import org.booleanfx.core.dao.Acc_patient_bill_headerDAO;
import org.booleanfx.core.dao.Category_masterDAO;
import org.booleanfx.core.dao.CodeDAO;
import org.booleanfx.core.dao.Item_masterDAO;
import org.booleanfx.core.dao.Professional_masterDAO;
import org.booleanfx.core.dao.Referrer_masterDAO;
import org.booleanfx.core.utils.MoneyReceipt;
import org.booleanfx.core.utils.Superimposing;
import org.booleanfx.core.utils.UUIDGenerator;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Div;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Label;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listcell;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Menuitem;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Timebox;

import com.domains.servlets.ConstantServlet;

public class ItemisedBillComposer extends GenericForwardComposer {
	
	Label msg;
		
	Div zmenu, zgrid, zform, main,itemdetails;

	Iframe report;

	String mode = "";
	
	Grid zformgrid;

	int selectedId = -1 ;
	
	Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack, mPrint,mPrint1;
	
	Textbox bill_no ;

	Professional_masterDAO masterDAO = new Professional_masterDAO();
	
	Datebox bill_date ;

	Timebox bill_time ;
	
	Combobox customer_type ;

	Textbox patient_reg_no ;
	
	Combobox patient_reg_no_old;

	Decimalbox patient_reg_charges ;

	Textbox patient_name ;

	Intbox patient_age ;

	Combobox patient_sex ;

	Textbox patient_address ;

	Textbox patient_telephone ;
	
	Textbox naration ;

	Intbox patient_pincode ;

	Combobox referred_by ;

	Combobox professional_by ;

	Decimalbox item_discount ;

	Decimalbox bill_total_amt ;

	Decimalbox amt_received ;

	Decimalbox bill_due_amt ;

	Combobox bill_payment_mode ;
	
	Combobox type ;
	
	Textbox searchname;

	Textbox bank_br ;

	Textbox cheque_no ;

	Datebox cheque_date ;

	Textbox bank_name ;

	Textbox card_no ;

	Datebox card_expiry_dt ;

	Textbox name_of_card ;

	Textbox bill_header_id ;

	Textbox line_item_sl_num ;

	Textbox line_item_model ;

     double value =0; 
     double discount=0 ;
     
	
	Row Chequerow1,Chequerow2,Cardrow1,Cardrow2,Newrow1,Oldrow1;
    Listcell itemCategoryCell,itemNameCell,itemSLNOCell,itemModelCell,itemRateCell,itemQuantityCell;
    
    Listcell itemDiscountCell,taxPercentageCell,taxAmountCell,totalAmountCell;
	
	Listbox listbox,itemlistbox;
		
	Decimalbox itemRate,itemDiscount,taxPercentage,taxAmount,totalAmount;
	
	Intbox itemQuantity;
	
	Combobox itemName,itemCategory;
	
	Category_masterDAO cMasterDAO = new Category_masterDAO();
	
	CodeDAO codeDAO = new CodeDAO() ;
	
	Acc_patient_bill_headerDAO accPatientBillHeaderDAO=new Acc_patient_bill_headerDAO();
	
	Acc_patient_bill_detailsDAO accPatientBillDetailsDAO=new Acc_patient_bill_detailsDAO();
	
	Referrer_masterDAO referrerMasterDAO = new Referrer_masterDAO() ;
	
	
	public void onCreate$main() throws Exception {
		
		
	
		Chequerow1.setVisible(false);
		
		Chequerow2.setVisible(false);
		
		Cardrow1.setVisible(false);
		
		Cardrow2.setVisible(false);
		
		Newrow1.setVisible(true);
		
		Oldrow1.setVisible(false);
		
			mLoad.setDisabled(false);

			mEdit.setDisabled(true);

			//mNew.setDisabled(false);

			mSave.setDisabled(true);

			mDelete.setDisabled(true);

			mBack.setDisabled(true);

			mPrint.setDisabled(true);
			
			mPrint1.setDisabled(true);

			report.setVisible(false);

			selectedId = -1 ;

		
	
	}
	
	public void onClick$mLoad() {

		try {

			 listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findAll()));

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
	
	
	
	public void onSelect$listbox() throws Exception {

		mLoad.setDisabled(false);

		mEdit.setDisabled(false);

		mNew.setDisabled(true);

		mSave.setDisabled(true);

		mDelete.setDisabled(false);

		mBack.setDisabled(false);

		mPrint.setDisabled(false);
		
		Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();


		
		if(webbean.getCustomer_type().equals("Old")){
			
			patient_reg_no_old.setModel(new ListModelList(accPatientBillHeaderDAO.findAll1()));
			
		
			
			
			Newrow1.setVisible(false);
			Oldrow1.setVisible(true);
				
		}else if(webbean.getCustomer_type().equals("New")){
			Newrow1.setVisible(true);
			Oldrow1.setVisible(false);
			
		}
		
		
		if(webbean.getBill_due_amt() == 0){
			
			mPrint1.setDisabled(false);
			
		}
		
		selectedId = webbean.getId();
		
		
	
	}
	
	public void onClick$mBack() {

		reset();

		mLoad.setDisabled(false);

		mEdit.setDisabled(true);

		mNew.setDisabled(false);

		mSave.setDisabled(true);

		mDelete.setDisabled(true);

		mBack.setDisabled(true);

		zformgrid.setVisible(true);

		report.setVisible(false);

		mPrint.setDisabled(true);

		selectedId = -1 ;

	}
	
    public void onClick$more() throws Exception {

           Listitem listitem=new Listitem();
           
           itemSLNOCell =new Listcell();
           
           line_item_sl_num=new Textbox();
           
           line_item_sl_num.setParent(itemSLNOCell);
           
           itemModelCell =new Listcell();
           
           line_item_model=new Textbox();
           
           line_item_model.setParent(itemModelCell);
           
           itemRateCell =new Listcell();
           
           itemRate=new Decimalbox();
           
           itemRate.setConstraint("no negative");
           
           itemRate.setFormat("###########.##");
           
           itemRate.setReadonly(true);
           
           itemRate.setParent(itemRateCell);
           
           itemQuantityCell =new Listcell();
           
           itemQuantity=new Intbox();
           
           itemQuantity.setParent(itemQuantityCell);
           
           itemDiscountCell =new Listcell();
           
           itemDiscount=new Decimalbox();
           
           itemDiscount.setFormat("###########.##");
           
           itemDiscount.setParent(itemRateCell); 
           itemDiscount.addEventListener("onChange", new EventListener() {
			
        	   public void onEvent(Event arg0) throws Exception {
				
   				
					if(itemDiscount.getValue()!=null){
						
						
					
					}else{
					
						
						
					}
				
			}
		});
           
           itemDiscount.setParent(itemDiscountCell);
           
           taxPercentageCell =new Listcell();
           
           taxPercentage=new Decimalbox();
           
           taxPercentage.setFormat("###########.##");
           
           taxPercentage.setConstraint("no negative");
           
           taxPercentage.setReadonly(true);
           
           taxPercentage.setParent(taxPercentageCell);
           
           taxAmountCell =new Listcell();
           
           taxAmount=new Decimalbox();
           
           taxAmount.setConstraint("no negative");
           
           taxAmount.setFormat("###########.##");
           
           taxAmount.setReadonly(true);
           
           taxAmount.setParent(taxAmountCell);
           
           taxAmount.addEventListener("onBlur", new EventListener() {
			
		   public void onEvent(Event arg0) throws Exception {
				
				if((itemQuantity.getValue()!=null||taxPercentage.getValue()!=null||itemRate.getValue()!=null)&&(itemDiscount.getValue()!=null)){
					taxAmount.setValue( (   (    (  (itemRate.getValue().multiply(new BigDecimal(itemQuantity.getValue())))   .subtract(itemDiscount.getValue()))     .multiply(taxPercentage.getValue()))    .divide(new BigDecimal(100))) );
					
				
				}else if( itemDiscount.getValue()==null || itemQuantity.getValue()!=null||taxPercentage.getValue()!=null||itemRate.getValue()!=null){
					taxAmount.setValue( (      (  (itemRate.getValue().multiply(new BigDecimal(itemQuantity.getValue())))       .multiply(taxPercentage.getValue()))    .divide(new BigDecimal(100))) );
					
				}else if( itemDiscount.getValue()!=null || itemQuantity.getValue()!=null||taxPercentage.getValue()==null||itemRate.getValue()!=null){
					taxAmount.setValue(      (  (itemRate.getValue().multiply(new BigDecimal(itemQuantity.getValue())))     .subtract(itemDiscount.getValue()))    );
					
				}else{
			
					alert("All values are not given properly");
					
				}
			}
		
           });
           
           totalAmountCell =new Listcell();
           
           totalAmount=new Decimalbox();
           
           totalAmount.setFormat("###########.##");
                    
           totalAmount.setReadonly(true);
           
           totalAmount.setParent(totalAmountCell);
           
           totalAmount.addEventListener("onBlur", new EventListener() {
			
        	   public void onEvent(Event arg0) throws Exception {
				
   				if(taxAmount.getValue()!=null){
				
					if(itemDiscount.getValue()!=null){
						
						totalAmount.setValue((itemRate.getValue().multiply(new BigDecimal(itemQuantity.getValue())).subtract(itemDiscount.getValue()).add(taxAmount.getValue())));
					   
					}else{
						
						 totalAmount.setValue((itemRate.getValue().multiply(new BigDecimal(itemQuantity.getValue())).add(taxAmount.getValue())));
						
					}
					 int position=itemlistbox.getItemCount();
					
				    for(int index = 0 ; index < position ; index++){
				    	
				    	Listitem li = itemlistbox.getItemAtIndex(index);
				    	List l=li.getChildren();
				    	
				    	
				    	itemDiscountCell=(Listcell)l.get(6);
				    	totalAmountCell=(Listcell)l.get(9);
				    	
				    	 itemDiscount=(Decimalbox)itemDiscountCell.getChildren().get(0);
						 totalAmount=(Decimalbox)totalAmountCell.getChildren().get(0);
				    	
				    	value = value + totalAmount.getValue().doubleValue();
				    	
				    	if(itemDiscount.getValue()!= null){
				    	
				    	     discount = discount + itemDiscount.getValue().doubleValue();
				    	
				    	}
				    }
					
					bill_total_amt.setValue(new BigDecimal(value));
				    item_discount.setValue(new BigDecimal(discount));
					   
					  value = 0 ;
					  discount = 0 ;
					   
					
				}else{
					
					alert("All values are not given");
								
				}
				
   				
   				
			}
		});
           
           itemCategoryCell=new Listcell();
           
           itemCategory=new Combobox();
           
           itemCategory.setCols(12);
           
           Category_masterBean[] categoryMasterBeans=cMasterDAO.findAll();
           
           List cateList=new ArrayList();
           
           for(int i=0;i<categoryMasterBeans.length;i++){
        	   
        	
           cateList.add(categoryMasterBeans[i].getCategory_name());
	        
	 }

	 itemCategory.setModel(new ListModelList(cateList));
           itemCategory.setParent(itemCategoryCell);
           
           itemCategory.addEventListener("onBlur", new EventListener() {
			
		   public void onEvent(Event arg0) throws Exception {
				
				 List itemList=new ArrayList();
			
				 Item_masterBean[] itemMasterBeans=new Item_masterDAO().findBycategory_name(itemCategory.getValue());
				 
				 for(int i=0;i<itemMasterBeans.length;i++){
		        	   
					 itemList.add(itemMasterBeans[i].getItem_name());
		        
				 }
			
				 itemName.setModel(new ListModelList(itemList));
			
		   }
		});
                      
           itemNameCell=new Listcell();
           
           itemName=new Combobox();
           itemName.setCols(12);
           itemName.setParent(itemNameCell);
           
           itemName.addEventListener("onBlur", new EventListener() {
						
		    public void onEvent(Event arg0) throws Exception {
		    
				Item_masterBean itemMasterBean=new Item_masterDAO().findByitem_name(itemName.getValue())[0];
			
				itemRate.setValue(new BigDecimal(itemMasterBean.getItem_rate()));
				
				taxPercentage.setValue(new BigDecimal(itemMasterBean.getItem_tax_percent()));
			
		    }
		});
           
          
           
           
           
           
           
           
           
           
           
           
           
     
          
           itemCategoryCell.setParent(listitem);
          
           itemNameCell.setParent(listitem);
           
           itemSLNOCell.setParent(listitem);
           
           itemModelCell.setParent(listitem);
           
           itemRateCell.setParent(listitem);
           
           itemQuantityCell.setParent(listitem);
           
           itemDiscountCell.setParent(listitem);
           
           taxPercentageCell.setParent(listitem);
           
           taxAmountCell.setParent(listitem);
           
           totalAmountCell.setParent(listitem);
          
           listitem.setParent(itemlistbox);
           	
	}
	
	public void onClick$del() {

	    int position=itemlistbox.getItemCount();

	    if(position<1){
	    	
	    	alert("Nothing to delete");
	    	
	    }else{
	        
	    	
	    	Listitem li = itemlistbox.getItemAtIndex(position-1);
	    	List l=li.getChildren();
	    	
	    	
	    	itemDiscountCell=(Listcell)l.get(6);
	    	totalAmountCell=(Listcell)l.get(9);
	    	
	    	 itemDiscount=(Decimalbox)itemDiscountCell.getChildren().get(0);
			 totalAmount=(Decimalbox)totalAmountCell.getChildren().get(0);
	    	
	    	if (totalAmount.getValue() != null || itemDiscount.getValue() != null) {
				value = bill_total_amt.getValue().doubleValue()	- totalAmount.getValue().doubleValue();
				discount = item_discount.getValue().doubleValue()- itemDiscount.getValue().doubleValue();
				bill_total_amt.setValue(new BigDecimal(value));
				item_discount.setValue(new BigDecimal(discount));
				itemlistbox.removeItemAt(position-1);
			}else if((totalAmount.getValue() == null || itemDiscount.getValue() == null)){
				itemlistbox.removeItemAt(position-1);
			}
			
	    	
	    }
	}
	
	
	public void onChange$bill_payment_mode(){
		
		if(bill_payment_mode.getSelectedItem().getLabel().equals("Cheque")){
			
			Chequerow1.setVisible(true);
			
			Chequerow2.setVisible(true);
			
            Cardrow1.setVisible(false);
			
			Cardrow2.setVisible(false);
			
			
		
		}else if(bill_payment_mode.getSelectedItem().getLabel().equals("Cash")){
			
        Chequerow1.setVisible(false);
			
			Chequerow2.setVisible(false);
			
			
			
			Cardrow1.setVisible(false);
			
			Cardrow2.setVisible(false);
			
		}else{
			
			 Chequerow1.setVisible(false);
				
				Chequerow2.setVisible(false);
					
					Cardrow1.setVisible(true);
					
					Cardrow2.setVisible(true);
				
		}
		}
  

	
	
	
	
public void onChange$amt_received(){
		
	if(amt_received.getValue() == null){
			bill_due_amt.setValue(bill_total_amt.getValue());
			
		
		
		}		
		
		else if(amt_received.getValue() != null){
		
if(bill_due_amt.getValue().doubleValue()==0){
	
	bill_due_amt.setValue(bill_total_amt.getValue().subtract(amt_received.getValue()));
	//alert("bbbbNEXT"+bill_due_amt.getValue());
}else {
	
	bill_due_amt.setValue(bill_due_amt.getValue().subtract(amt_received.getValue()));
	
}
			
			
		}
		}
	
	
	
	
	
	
	
	
	
    public void onChange$customer_type() throws  Exception{
		
		if(customer_type.getSelectedItem().getLabel().equals("New")){
			
			if(UUIDGenerator.patient_regno == null){
				
				patient_reg_no.setValue(UUIDGenerator.getInstance().getFirstTimePatient_regNo());
				
			}else{
				
				patient_reg_no.setValue(UUIDGenerator.getInstance().getNextPatient_regNo());
				
			}
			
			codeDAO.updateRegNo();
			Newrow1.setVisible(true);
			Oldrow1.setVisible(false);
			patient_name.setReadonly(false);
			
			patient_sex.setReadonly(false);
			
			patient_age.setReadonly(false);
			
			patient_telephone.setReadonly(false);
			
			patient_address.setReadonly(false);
			
			patient_pincode.setReadonly(false);
		}else if(customer_type.getSelectedItem().getLabel().equals("Old")){
			
			patient_reg_no_old.setModel(new ListModelList(accPatientBillHeaderDAO.findAll1()));
			
			patient_name.setReadonly(true);
			
			patient_sex.setReadonly(true);
			
			patient_age.setReadonly(true);
			
			patient_telephone.setReadonly(true);
			
			patient_address.setReadonly(true);
			
			patient_pincode.setReadonly(true);
			
			
			Newrow1.setVisible(false);
			Oldrow1.setVisible(true);
				
		}
		}
	
	
	
	
	public void onClick$mSave() throws Exception {

		try {
			//if(validate()){
			
				 Acc_patient_bill_headerBean webbean=new Acc_patient_bill_headerBean();
				  
				webbean.setId(UUIDGenerator.getNextUUID());
				 
				webbean.setBill_no(bill_no.getValue());

				DateFormat	formatter = new SimpleDateFormat("dd-MM-yyyy");
			
				webbean.setBill_date(formatter.format(bill_date.getValue()));
				
				formatter = new SimpleDateFormat("hh:mm a");

				webbean.setBill_time(formatter.format(bill_time.getValue()));
				
				
				
				webbean.setCustomer_type(customer_type.getValue());
				
				if(customer_type.getSelectedItem().getLabel().equals("New")){
					
					webbean.setPatient_reg_no(patient_reg_no.getValue());

					webbean.setPatient_reg_charges(patient_reg_charges.getValue().doubleValue());
					
				}else if(webbean.getCustomer_type().equals("Old")){
					
					webbean.setPatient_reg_no(patient_reg_no_old.getValue());
					webbean.setPatient_reg_charges(0.0);
					
				}else if(customer_type.getSelectedItem().getLabel().equals("Old")){
					
					webbean.setPatient_reg_no(patient_reg_no_old.getSelectedItem().getLabel());
					webbean.setPatient_reg_charges(0.0);
					
				}
				
				
				webbean.setPatient_name(patient_name.getValue());
				
				webbean.setPatient_age(patient_age.getValue());

				webbean.setPatient_sex(patient_sex.getSelectedItem().getLabel());

				webbean.setPatient_address(patient_address.getValue());

				webbean.setPatient_telephone(""+patient_telephone.getValue());

				webbean.setPatient_pincode(""+patient_pincode.getValue());
				
				if(referred_by.getSelectedItem() == null){
					webbean.setReferred_by("");
				}else{
					webbean.setReferred_by(referred_by.getSelectedItem().getLabel());
				}
				
				if(professional_by.getSelectedItem() == null){
					
					webbean.setProfessional_by("");
				}else{
					webbean.setProfessional_by(professional_by.getSelectedItem().getLabel());
				}
				System.out.println("AAAA="+naration.getValue().toString());
				if(naration.getValue() == null){
					
					webbean.setNaration("") ;
					
				}else{
					
				
					webbean.setNaration(naration.getValue().toString());
				}
				
				
				
				webbean.setItem_discount(item_discount.getValue().doubleValue());

				webbean.setBill_total_amt(bill_total_amt.getValue().doubleValue());

				webbean.setAmt_received(amt_received.getValue().doubleValue());
				
				webbean.setBill_due_amt(bill_due_amt.getValue().doubleValue());
				
				
				
				
				webbean.setBill_payment_mode(bill_payment_mode.getSelectedItem().getLabel());
				
				
				if(bill_payment_mode.getSelectedItem().getLabel().equals("Cheque")){
					
					webbean.setBank_br(bank_br.getValue());

					webbean.setCheque_no(cheque_no.getValue());

					webbean.setCheque_date(cheque_date.getValue().toString());

					webbean.setBank_name(bank_name.getValue());
					
					webbean.setCard_no("");

					webbean.setCard_expiry_dt("");

					webbean.setName_of_card("");
					
				}else if(bill_payment_mode.getSelectedItem().getLabel().equals("Cash")){
					
					webbean.setBank_br("");

					webbean.setCheque_no("");

					webbean.setCheque_date("");

					webbean.setBank_name("");
					
					webbean.setCard_no("");

					webbean.setCard_expiry_dt("");

					webbean.setName_of_card("");
					
					
				}else{
					webbean.setBank_br("");

					webbean.setCheque_no("");

					webbean.setCheque_date("");

					webbean.setBank_name("");
					webbean.setCard_no(card_no.getValue());

					webbean.setCard_expiry_dt(card_expiry_dt.getValue().toString());

					webbean.setName_of_card(name_of_card.getValue());
				}
				
				
				
/*if(webbean.getId()==0){
	accPatientBillHeaderDAO.insertAllCols(webbean);
}else if(webbean.getId()!=0){
	System.out.println("AAAAAAAAAAAAAA"+webbean.getBill_due_amt());
	accPatientBillHeaderDAO.updateAllCols(webbean);
}*/


if(selectedId == -1){

	selectedId = UUIDGenerator.getNextUUID() ;

	webbean.setId(selectedId);

	accPatientBillHeaderDAO.insertAllCols(webbean);

}else{

	webbean.setId(selectedId);

	accPatientBillHeaderDAO.updateAllCols(webbean);

}
				  
				  int loopsize=itemlistbox.getItems().size();
				  
				 
				  
					for(int i=0 ; i<loopsize ; i++){
						  
						  Listitem selectedItem=itemlistbox.getItemAtIndex(i);
						  
						  List itemCells=selectedItem.getChildren();
						  
						  itemCategoryCell=(Listcell)itemCells.get(0);
						  
						  itemNameCell=(Listcell)itemCells.get(1);
						  
						  itemSLNOCell=(Listcell)itemCells.get(2);
						  
						  itemModelCell=(Listcell)itemCells.get(3);
						  
						  itemRateCell = (Listcell)itemCells.get(4);
						  
						  itemQuantityCell=(Listcell)itemCells.get(5);
						  
						  itemDiscountCell=(Listcell)itemCells.get(6);
						  
						  taxPercentageCell=(Listcell)itemCells.get(7);
						  
						  taxAmountCell=(Listcell)itemCells.get(8);
						  
						  totalAmountCell=(Listcell)itemCells.get(9);
						  			  
					
						  
						  itemCategory = (Combobox)itemCategoryCell.getChildren().get(0);
						  
						  itemName = (Combobox)itemNameCell.getChildren().get(0);
						  
						 line_item_sl_num =(Textbox)itemSLNOCell.getChildren().get(0);
						 
						 line_item_model =(Textbox)itemModelCell.getChildren().get(0);
						  
						  itemRate=(Decimalbox)itemRateCell.getChildren().get(0);
						  
						  itemQuantity=(Intbox)itemQuantityCell.getChildren().get(0);
						  
						  itemDiscount=(Decimalbox)itemDiscountCell.getChildren().get(0);
						  
						  taxPercentage=(Decimalbox)taxPercentageCell.getChildren().get(0);
						  
						  taxAmount=(Decimalbox)taxAmountCell.getChildren().get(0);
						  
						  totalAmount=(Decimalbox)totalAmountCell.getChildren().get(0);
						  
						 
						
						 Acc_patient_bill_detailsBean billDetailsBean = new Acc_patient_bill_detailsBean() ;
						
						 billDetailsBean.setBill_header_id(webbean.getId());
						 
						 billDetailsBean.setLine_cate_name(itemCategory.getSelectedItem().getValue().toString());
						 
						 billDetailsBean.setLine_item_name(itemName.getSelectedItem().getValue().toString());
						 
						 billDetailsBean.setLine_item_price(itemRate.getValue().doubleValue());
						 
						 billDetailsBean.setLine_item_qty(itemQuantity.getValue());
						 
						 if(itemDiscount.getValue() != null){
						 
							 billDetailsBean.setLine_item_discount(itemDiscount.getValue().doubleValue());
				         
						 }else{
							 
							 billDetailsBean.setLine_item_discount(0.0);
							 
						 }
				         
				         if(line_item_sl_num.getValue().toString().equals("")){
				        	 
				        	 billDetailsBean.setLine_item_sl_num("");
				        	 
				         }else{
				        	 
				        	 billDetailsBean.setLine_item_sl_num(line_item_sl_num.getValue());
				        	 
				         }
				         
                        if(line_item_model.getValue().toString().equals("")){
				        	 
				        	 billDetailsBean.setLine_item_model("");
				        	 
				         }else{
				        	 
				        	 billDetailsBean.setLine_item_model(line_item_model.getValue());
				        	 
				         }
				        
				         billDetailsBean.setLine_item_tax_name(taxPercentage.getValue().toString());
				         
				         billDetailsBean.setLine_item_tax_amt(taxAmount.getValue().doubleValue());
				         
				         billDetailsBean.setLine_gross_amt(totalAmount.getValue().doubleValue());
				         
				         billDetailsBean.setId(UUIDGenerator.getNextUUID());
				         
						 accPatientBillDetailsDAO.insertAllCols(billDetailsBean);
				          
				         
					  }
				
				
					 Superimposing smp=new Superimposing();

					 MoneyReceipt moneyReceipt = new MoneyReceipt();
									
					 String contextPath = ConstantServlet.contextPath ;
						
					 if(webbean.getBill_due_amt() == 0){
						 
						 String bill_SOURCE =contextPath + "reports/bill"+webbean.getBill_no().substring(webbean.getBill_no().lastIndexOf("/")+1,webbean.getBill_no().length())+".pdf";
							
						 smp.createPdf(bill_SOURCE, webbean);
						 
					 }
					 
					 
					 
					 String mr_SOURCE =contextPath + "reports/mr"+webbean.getBill_no().substring(webbean.getBill_no().lastIndexOf("/")+1,webbean.getBill_no().length())+".pdf";
				    
				     moneyReceipt.createPdf(mr_SOURCE,webbean);
				     
				     alert("Data saved successfully");
				
				     reset();
				     
				     mLoad.setDisabled(false);

						mEdit.setDisabled(true);

						mNew.setDisabled(false);

						mSave.setDisabled(true);

						mDelete.setDisabled(true);

						mBack.setDisabled(false);

						report.setVisible(false);

						mPrint.setDisabled(true);
						
						mPrint1.setDisabled(true);
			
						 listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findAll()));

			
		} catch (Exception e) {
			
			e.printStackTrace();
		} 

		
	
		
    }

	public boolean validate(){
		Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();
		
		
		
	if(webbean.getId()!= 0){	
		
		
		
		return true;
		
		
	}else if(webbean.getId()== 0){
		if(itemlistbox.getItemCount()<1){
			
			alert("Atleast insert one Item");
			
			return false;
			
		}if(bill_payment_mode.getSelectedItem() == null){
			
			alert("Mode of payment is not selected");
			
			return false;
			
		}else{
			
			return true;
			
		}
		
	}else{
		
		return true;
		
	}
		
		
		
		
		
	}
	
	public void onClick$mNew() throws  Exception {

		reset();

      Newrow1.setVisible(true);
		
		Oldrow1.setVisible(false);
		mLoad.setDisabled(false);

		mEdit.setDisabled(true);

		mNew.setDisabled(true);

		mSave.setDisabled(false);

		mDelete.setDisabled(true);

		mBack.setDisabled(false);

		report.setVisible(false);

		mPrint.setDisabled(true);

		mode = "add";

		selectedId = -1 ;
		
		if(UUIDGenerator.bill_no== null){
			
			bill_no.setValue(UUIDGenerator.getInstance().getFirstTimeBill_no());
		
		}else{
			
			bill_no.setValue(UUIDGenerator.getInstance().getNextBill_no());
			
		}
		
		codeDAO.updateBillNo();
		
		bill_no.setReadonly(true);
		
		professional_by.setModel(new ListModelList(masterDAO.findAll()));
		
		referred_by.setModel(new ListModelList(referrerMasterDAO.findAll()));
		
		bill_total_amt.setReadonly(true);
		bill_total_amt.setFormat("###########.##");
		bill_due_amt.setReadonly(true);
		bill_due_amt.setFormat("###########.##");
		item_discount.setReadonly(true);
		item_discount.setFormat("###########.##");
		
	}
	
	
	

	
	void reset(){
		
		selectedId = -1;

		mode = "new";
		
		bill_no.setValue("");
 
		customer_type.setValue("");

		patient_reg_no.setValue("");

		patient_reg_charges.setValue(new BigDecimal(0.00));

		patient_name.setValue("");

		patient_age.setValue(0);

		patient_sex.setValue("");

		patient_address.setValue("");

		patient_telephone.setValue("");

		patient_pincode.setValue(0);

		referred_by.setValue("");

		professional_by.setValue("");

		item_discount.setValue(new BigDecimal(0.00));

		bill_total_amt.setValue(new BigDecimal(0.00));

		amt_received.setValue(new BigDecimal(0.00));

		bill_due_amt.setValue(new BigDecimal(0.00));

		bill_payment_mode.setValue("");

		bank_br.setValue("");

		cheque_no.setValue("");

		bank_name.setValue("");

		card_no.setValue("");

		naration.setValue("");

		name_of_card.setValue("");

		listbox.setSelectedItem(null);
		int loopsize=itemlistbox.getItems().size();
		while(loopsize>0){
			
			Listitem listitem = (Listitem) itemlistbox.getLastChild();
			itemlistbox.removeChild(listitem);
			loopsize--;
			
		}
		
	}
	
	public void onClick$mPrint() {
		
		itemdetails.setVisible(false);
		zformgrid.setVisible(false);
		report.setVisible(true);
		mBack.setDisabled(false);
		mPrint.setDisabled(true);
		mLoad.setDisabled(true);
		mSave.setDisabled(true);
		mNew.setDisabled(true);
		mDelete.setDisabled(true);
		
		

		
		FileInputStream fin = null;

		try {
			
			/*  1. Remove the hard-coding filepath
			  2. Create a constant servlet to get the realpath
			  3. Create the PDF report from the code.*/
			 
			Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();
			
			String contextPath = ConstantServlet.contextPath ;
			
			File file = new File(contextPath + "reports/mr"+webbean.getBill_no().substring(webbean.getBill_no().lastIndexOf("/")+1,webbean.getBill_no().length())+".pdf");

			fin = new FileInputStream(file);

			int bufsize = (int) file.length();

			byte[] buf = new byte[bufsize];

			fin.read(buf);

			// prepare the AMedia for iframe
			final InputStream mediais = new ByteArrayInputStream(buf);

			final AMedia amedia = new AMedia("MoneyReceipt.pdf", "pdf",
					"application/pdf", mediais);

			// set iframe content
            if(file!=null){
			report.setContent(amedia);
            }

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (fin != null) {

				try {
					fin.close();
				} catch (Exception e) {
				}
			}
		}

	}
	
	
	
	
	
	
public void onClick$mPrint1() {
		
		itemdetails.setVisible(false);
		zformgrid.setVisible(false);
		report.setVisible(true);
		mBack.setDisabled(false);
		mPrint.setDisabled(true);
		mLoad.setDisabled(true);
		mSave.setDisabled(true);
		mNew.setDisabled(true);
		mDelete.setDisabled(true);
		
		

		
		FileInputStream fin = null;

		try {
			
			/*  1. Remove the hard-coding filepath
			  2. Create a constant servlet to get the realpath
			  3. Create the PDF report from the code.*/
			 
			Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();
			
			String contextPath = ConstantServlet.contextPath ;
			
			File file = new File(contextPath + "reports/bill"+webbean.getBill_no().substring(webbean.getBill_no().lastIndexOf("/")+1,webbean.getBill_no().length())+".pdf");

			fin = new FileInputStream(file);

			int bufsize = (int) file.length();

			byte[] buf = new byte[bufsize];

			fin.read(buf);

			// prepare the AMedia for iframe
			final InputStream mediais = new ByteArrayInputStream(buf);

			final AMedia amedia = new AMedia("Bill.pdf", "pdf",
					"application/pdf", mediais);

			// set iframe content
            if(file!=null){
			report.setContent(amedia);
            }

		} catch (Exception ex) {
			throw new RuntimeException(ex);
		} finally {
			if (fin != null) {

				try {
					fin.close();
				} catch (Exception e) {
				}
			}
		}

	}
	
	public void onClick$mDelete() {

		
		try {

			Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();

			 if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
					
				  selectedId = webbean.getId();

			     accPatientBillHeaderDAO.deleteByid(selectedId);
			      
			    			    	  
			    	  accPatientBillDetailsDAO.deleteBybill_header_id(selectedId);
			  

			      listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findAll()));
			       
			  }else{
			    	
			    	alert("you dont have enough privilege for this action");
			   }

		} catch (Exception e) {

			e.printStackTrace();

		}

	}
	
	public void onChange$patient_reg_no_old(){
		
		try {
			
			Acc_patient_bill_headerBean bean = accPatientBillHeaderDAO.findBypatient_reg_no(patient_reg_no_old.getSelectedItem().getValue().toString())[0];
			
		
			patient_name.setValue(bean.getPatient_name());
			
			patient_sex.setValue(bean.getPatient_sex());
			
			
			
			patient_age.setValue(bean.getPatient_age());
			
			patient_telephone.setValue(bean.getPatient_telephone());
			
			patient_address.setValue(bean.getPatient_address());
			
			patient_pincode.setValue(Integer.parseInt(bean.getPatient_pincode()));
	
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		
	}
	
	public void onClick$mEdit() throws Exception{

		mLoad.setDisabled(false);

		mEdit.setDisabled(true);

		mNew.setDisabled(true);

		mSave.setDisabled(false);

		mDelete.setDisabled(true);

		mBack.setDisabled(false);

professional_by.setModel(new ListModelList(masterDAO.findAll()));
		
		referred_by.setModel(new ListModelList(referrerMasterDAO.findAll()));
		
		Acc_patient_bill_headerBean webbean  = (Acc_patient_bill_headerBean)listbox.getSelectedItem().getValue();

		selectedId = webbean.getId();

		mode = "edit";

		try{

			//id.setValue(""+webbean.getId());

			bill_no.setValue(webbean.getBill_no());
			
		
		
		 DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		 Date date = (Date)formatter.parse(webbean.getBill_date());
		  
		
			bill_date.setValue(date);
			
			
			DateFormat formatter1 =   new SimpleDateFormat("hh:mm a");
			Date date1 = (Date)formatter1.parse(webbean.getBill_time());
			
			bill_time.setValue(date1);

			customer_type.setValue("Old");

			patient_reg_no.setValue(webbean.getPatient_reg_no());
			System.out.println("AAAAAAAA="+patient_reg_no.getValue().toString());
			patient_reg_no_old.setValue(webbean.getPatient_reg_no());
			System.out.println("AAAAAAAABBBBBBBBB="+patient_reg_no_old.getValue());
			DecimalFormat format = new DecimalFormat("0.00");
			
			
			patient_reg_charges.setValue(format.format(new BigDecimal("0.00")));

			patient_name.setValue(webbean.getPatient_name());

			patient_age.setValue(webbean.getPatient_age());

			patient_sex.setValue(webbean.getPatient_sex());

			patient_address.setValue(webbean.getPatient_address());

			patient_telephone.setValue(webbean.getPatient_telephone());

			patient_pincode.setValue(Integer.parseInt(webbean.getPatient_pincode()));

			referred_by.setValue(webbean.getReferred_by());

			professional_by.setValue(webbean.getProfessional_by());

			item_discount.setValue(format.format(new BigDecimal(webbean.getItem_discount())));

			bill_total_amt.setValue(format.format(new BigDecimal(webbean.getBill_total_amt())));
			
			naration.setValue(webbean.getNaration());

			//amt_received.setValue(new BigDecimal(webbean.getAmt_received()));

			bill_due_amt.setValue(format.format(new BigDecimal(webbean.getBill_due_amt())));

		/*	bill_payment_mode.setValue(webbean.getBill_payment_mode());

			bank_br.setValue(webbean.getBank_br());

			cheque_no.setValue(webbean.getCheque_no());

			cheque_date.setValue(webbean.getCheque_date());

			bank_name.setValue(webbean.getBank_name());

			card_no.setValue(webbean.getCard_no());

			card_expiry_dt.setValue(webbean.getCard_expiry_dt());

			name_of_card.setValue(webbean.getName_of_card());*/

			if(selectedId == -1){

				try{

					Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

				}catch(Exception ex){}

			}

		}catch(Exception e){

			e.printStackTrace();

		}

	}
	public void onClick$search() {

		try {
			if(type.getSelectedItem()== null){
				alert("Enter some Search Type");
			}else{
				
				if(type.getSelectedItem().getLabel().equals("Bill No")){
					
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findBybill_no(searchname.getValue())));
					
				}else if(type.getSelectedItem().getLabel().equals("Patient Name")){
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findBypatient_name(searchname.getValue())));

				}else if(type.getSelectedItem().getLabel().equals("Patient Regn No")){
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findBypatient_reg_no(searchname.getValue())));

				}else if(type.getSelectedItem().getLabel().equals("Referral Name")){
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findByreferred_by(searchname.getValue())));

				}else if(type.getSelectedItem().getLabel().equals("Professional Name")){
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findByprofessional_by(searchname.getValue())));

				}else if(type.getSelectedItem().getLabel().equals("Bill Date(DD-MM-YYYY)")){
					listbox.setModel(new ListModelList(accPatientBillHeaderDAO.findBybill_date(searchname.getValue())));

				}
				
				

			}
				
			
			

		} catch (Exception e) {

			e.printStackTrace();

		}

		searchname.setValue("");
		
	}
	
}