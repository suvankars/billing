package org.booleanfx.core.composer;

import java.util.Iterator;
import java.util.List;

import org.booleanfx.core.bean.Employee_masterBean;
import org.booleanfx.core.dao.Employee_masterDAO;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

public class LoginComposer extends GenericForwardComposer{

	Window loginwindow;
	Button login;
	Label msg;
	Textbox username, password;
	
	public void onCreate$loginwindow(){
		
		if(Executions.getCurrent().getParameter("msg")!=null){
			Session session=Sessions.getCurrent();
			session.removeAttribute("logininfo");
			msg.setValue(Executions.getCurrent().getParameter("msg"));
		
		}else{
			
			msg.setValue("WELCOME");
			
		}
		loginwindow.doOverlapped();
		loginwindow.setPosition("center");
		loginwindow.setSizable(true);
		
	}
	
	
	public void onClick$login() throws Exception{
		
		Session session=Sessions.getCurrent();
		String loginstatus="notloggedin";
		Employee_masterDAO empdao=new Employee_masterDAO();
		Employee_masterBean[] empList=empdao.findAll();
		
		for(int i=0;i<empList.length;i++){
			
			if(username.getValue().equals(empList[i].getUsername()) && password.getValue().equals(empList[i].getPassword())){
				loginstatus="loggedin";
				session.setAttribute("logininfo", empList[i]);
				Executions.sendRedirect("pages/index.zul");
			}
			
		}
		
		if(loginstatus.equals("notloggedin")){
			
			msg.setValue("username password doesnt match");
		}
		

	}
}
