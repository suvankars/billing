	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Grid;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Patient_masterComposer extends GenericForwardComposer{

			//Define Common members here

			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Grid zformgrid;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox reg_no ;

			Textbox name ;

			Textbox age ;

			Combobox sex ;

			Textbox telephone ;

			Textbox mobile ;

			Textbox category ;

			Textbox status ;

			Textbox priority ;

			Decimalbox reg_charges ;

			Patient_masterDAO daoHandler = new Patient_masterDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				 

				report.setVisible(false);
				
                sex.appendItem("male");
				
				sex.appendItem("female");

				selectedId = -1 ;

			}

			public void onClick$mNew() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				 

				mode = "add";

				selectedId = -1 ;

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Patient_masterBean webbean = new Patient_masterBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setReg_no(reg_no.getValue());

					webbean.setName(name.getValue());

					webbean.setAge(age.getValue());

					webbean.setSex(sex.getSelectedItem().getLabel());

					webbean.setTelephone(telephone.getValue());

					webbean.setMobile(mobile.getValue());

					webbean.setCategory(category.getValue());

					webbean.setStatus(status.getValue());

					webbean.setPriority(priority.getValue());

					webbean.setReg_charges(reg_charges.getValue().doubleValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);
						
						Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					}else{
                        
					    if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
					    	webbean.setId(selectedId);
    
					        daoHandler.updateAllCols(webbean);
					        
					    	Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);
					        
					    }else{
					    	
					    	alert("you dont have enough privilege for this action");
					    }

					}

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					 

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				zformgrid.setVisible(true);

				report.setVisible(false);

				 

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				Patient_masterBean webbean  = (Patient_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Patient_masterBean webbean  = (Patient_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					reg_no.setValue(webbean.getReg_no());

					name.setValue(webbean.getName());

					age.setValue(webbean.getAge());

					sex.setValue(webbean.getSex());

					telephone.setValue(webbean.getTelephone());

					mobile.setValue(webbean.getMobile());

					category.setValue(webbean.getCategory());

					status.setValue(webbean.getStatus());

					priority.setValue(webbean.getPriority());

					reg_charges.setValue(new BigDecimal(webbean.getReg_charges()));

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Patient_masterBean webbean  = (Patient_masterBean)listbox.getSelectedItem().getValue();

					if(((Employee_masterBean)session.getAttribute("logininfo")).getUsertype().equals("ADMIN")){
						
						  selectedId = webbean.getId();

					      daoHandler.deleteByid(selectedId);

					      listbox.setModel(new ListModelList(daoHandler.findAll()));
					       
					  }else{
					    	
					    	alert("you dont have enough privilege for this action");
					   }

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				reg_no.setValue("");

				name.setValue("");

				age.setValue("");

				sex.setValue("");

				telephone.setValue("");

				mobile.setValue("");

				category.setValue("");

				status.setValue("");

				priority.setValue("");

				reg_charges.setValue(new BigDecimal(0.00));

				listbox.setSelectedItem(null);

			}

	}

