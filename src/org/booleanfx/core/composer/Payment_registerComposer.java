	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zul.Combobox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

import org.booleanfx.core.dao.* ;

	public class Payment_registerComposer extends GenericForwardComposer{

			//Define Common members here

		    Category_masterDAO categoryMasterDAO = new Category_masterDAO();		
			Item_masterDAO itemMasterDAO = new Item_masterDAO();
			Professional_masterDAO professionalMasterDAO=new Professional_masterDAO();
			
			Referrer_masterDAO referrerMasterDAO=new Referrer_masterDAO();
			
			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			Textbox id ;

			Combobox payment_register_type ;

			Combobox payment_register_name ;

			Textbox payment_register_cell_no ;

			Textbox payment_register_address ;

			Combobox category_name_id ;

			Combobox item_name_id ;

			Datebox from_date ;

			Datebox entry_date ;
			
			Datebox to_date ;

			Decimalbox volume_payment_register ;

			Decimalbox commission_amount ;

			Decimalbox due_amount ;

			Decimalbox amount_paid ;

			Payment_registerDAO daoHandler = new Payment_registerDAO();
			
			
			
			int selectedbeanid;

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				  

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew() throws Exception {

				//formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				  

				mode = "add";

				selectedId = -1 ;
				
				category_name_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
				

			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}
			
			public void onChange$volume_payment_register() throws Exception{
				
				double volume = volume_payment_register.getValue().doubleValue() ;
				
				String sub[] = payment_register_name.getSelectedItem().getValue().toString().split("-");
				
				if(payment_register_type.getSelectedItem().getLabel().equals("Professional")){
				
				    Professional_masterBean bean = professionalMasterDAO.findByprofessional_code(sub[0])[0];
				
				    if(volume <= new Double(bean.getTarget_volume1())){
				    	
				    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_volume_commission_percent1())/100));
				    	
				    }else if(volume > new Double(bean.getTarget_volume1()) && volume <= new Double(bean.getTarget_volume2())){
				    	
				    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_volume_commission2())/100));
				    	
				    }else if(volume > new Double(bean.getTarget_volume2()) && volume <= new Double(bean.getTarget_volume3())){
				    	
				    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_volume_commission3())/100));
				    	
				    }else{
				    	
				    	alert("Commission is undeclared");
				    	
				    }
				   
				}else{
					
					Referrer_masterBean bean = referrerMasterDAO.findByreferrer_code(sub[0])[0];
					
					 if(volume <= new Double(bean.getTarget_volume1())){
					    	
					    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_commission1())/100));
					    	
					    }else if(volume > new Double(bean.getTarget_volume1()) && volume <= new Double(bean.getTarget_volume2())){
					    	
					    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_commission2())/100));
					    	
					    }else if(volume > new Double(bean.getTarget_volume2()) && volume <= new Double(bean.getTarget_volume3())){
					    	
					    	commission_amount.setValue(new BigDecimal(volume * new Double(bean.getTarget_commission3())/100));
					    	
					    }else{
					    	
					    	alert("Commission is undeclared");
					    	
					    }
				}
			}

			public void onClick$mSave() {

				Payment_registerBean webbean = new Payment_registerBean();

				try{

					//webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setPayment_register_type(payment_register_type.getValue());

					String sub[] = payment_register_name.getSelectedItem().getValue().toString().split("-");
					
					int id = 0;
					
					if(payment_register_type.getSelectedItem().getLabel().equals("Professional")){
					
					    Professional_masterBean bean = professionalMasterDAO.findByprofessional_code(sub[0])[0];
					    
					    id = bean.getId() ;
					    
					}else{
						
						Referrer_masterBean bean = referrerMasterDAO.findByreferrer_code(sub[0])[0];
						
						id = bean.getId();
						
					}

					webbean.setPayment_register_name(id);
					
					webbean.setPayment_register_cell_no(Integer.parseInt(payment_register_cell_no.getValue()));

					webbean.setPayment_register_address(payment_register_address.getValue());

					webbean.setCategory_name_id(Integer.parseInt(category_name_id.getSelectedItem().getValue().toString()));

					webbean.setItem_name_id(Integer.parseInt(item_name_id.getSelectedItem().getValue().toString()));

					webbean.setFrom_date(new java.sql.Date(from_date.getValue().getTime()));

					webbean.setTo_date(new java.sql.Date(to_date.getValue().getTime()));

  					webbean.setEntry_date(new java.sql.Date(entry_date.getValue().getTime()));
					
					webbean.setVolume_payment_register(volume_payment_register.getValue().toString());

					webbean.setPercent_payment_register(0.0);

					webbean.setCommission_amount(commission_amount.getValue().doubleValue());

					webbean.setDue_amount(due_amount.getValue().doubleValue());

					webbean.setAmount_paid(amount_paid.getValue().doubleValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					  

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				report.setVisible(false);

				  

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);

				

				Payment_registerBean webbean  = (Payment_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit() throws Exception{

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Payment_registerBean webbean  = (Payment_registerBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();
				
				category_name_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
				item_name_id.setModel(new ListModelList(itemMasterDAO.findAll()));

				mode = "edit";

				try{

					//id.setValue(""+webbean.getId());

					payment_register_type.setValue(webbean.getPayment_register_type());
					if(webbean.getPayment_register_type().equals("Professional")){
					    Professional_masterBean[] professionalMasterBeans = professionalMasterDAO.findByid(webbean.getPayment_register_name());
					    payment_register_name.setValue(professionalMasterBeans[0].getProfessional_code()+"-"+professionalMasterBeans[0].getProfessional_name());
					    professionalMasterBeans = professionalMasterDAO.findAll();
					    List proList=new ArrayList();
					    for(int i=0;i<professionalMasterBeans.length;i++){
							
							proList.add(professionalMasterBeans[i].getProfessional_code()+"-"+professionalMasterBeans[i].getProfessional_name());
							
						}
						
						payment_register_name.setModel(new ListModelList(proList));
					
					}else{
						Referrer_masterBean[] referrerMasterBeans = referrerMasterDAO.findByid(webbean.getPayment_register_name());    
						payment_register_name.setValue(referrerMasterBeans[0].getReferrer_code()+"-"+referrerMasterBeans[0].getReferrer_name());
				
						referrerMasterBeans = referrerMasterDAO.findAll();
						 List referrerList=new ArrayList();
						 for(int j=0;j<referrerMasterBeans.length;j++){
	                        	referrerList.add(referrerMasterBeans[j].getReferrer_code()+"-"+referrerMasterBeans[j].getReferrer_name());
	                        	
	                        }
	                        payment_register_name.setModel(new ListModelList(referrerList));
					
					
					
					}
					
					
					
					
					
					payment_register_cell_no.setValue(""+webbean.getPayment_register_cell_no());

					payment_register_address.setValue(""+webbean.getPayment_register_address());

					
					Category_masterBean[] categoryBean = categoryMasterDAO.findByid(webbean.getCategory_name_id());
					
					category_name_id.setValue(categoryBean[0].getCategory_name());
					
					//category_name_id.setValue(""+);
					
					Item_masterBean[] itemMasterBeans = itemMasterDAO.findByid(webbean.getItem_name_id());

					item_name_id.setValue(itemMasterBeans[0].getItem_name());

					from_date.setValue(webbean.getFrom_date());

					to_date.setValue(webbean.getTo_date());

					volume_payment_register.setValue(webbean.getVolume_payment_register());

					//percent_payment_register.setValue(new BigDecimal(webbean.getPercent_payment_register()));

					commission_amount.setValue(new BigDecimal(webbean.getCommission_amount()));

					due_amount.setValue(new BigDecimal(webbean.getDue_amount()));

					amount_paid.setValue(new BigDecimal(webbean.getAmount_paid()));

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Payment_registerBean webbean  = (Payment_registerBean)listbox.getSelectedItem().getValue();

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

					alert("Data deleted successfully");

						listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				payment_register_type.setValue("");

				payment_register_name.setValue("");

				payment_register_cell_no.setValue("");

				payment_register_address.setValue("");

				category_name_id.setValue("");

				item_name_id.setValue("");
        
				volume_payment_register.setValue(new BigDecimal(0.00));

				commission_amount.setValue(new BigDecimal(0.00));

				due_amount.setValue(new BigDecimal(0.00));

				amount_paid.setValue(new BigDecimal(0.00));
				
				from_date.setValue(null);
				
				to_date.setValue(null);

				listbox.setSelectedItem(null);

			}

			public void onChange$payment_register_type() throws Exception{
				
		        
			    String selectedString=payment_register_type.getSelectedItem().getLabel();
				payment_register_name.setValue("");
			    if(selectedString.equals("Professional")){
					
					List proList=new ArrayList();
					Professional_masterBean[] masterBeans=professionalMasterDAO.findAll();
					for(int i=0;i<masterBeans.length;i++){
						
						proList.add(masterBeans[i].getProfessional_code()+"-"+masterBeans[i].getProfessional_name());
						
					}
					
					payment_register_name.setModel(new ListModelList(proList));
					
					
				}else{
                        List referrerList=new ArrayList();
                        Referrer_masterBean[] masterbeans1=referrerMasterDAO.findAll();
                        for(int j=0;j<masterbeans1.length;j++){
                        	referrerList.add(masterbeans1[j].getReferrer_code()+"-"+masterbeans1[j].getReferrer_name());
                        	
                        }
                        payment_register_name.setModel(new ListModelList(referrerList));
				   }
					
				}
			
			
			public void onChange$payment_register_name() throws Exception{
			   
				String selectedString= payment_register_type.getSelectedItem().getLabel();
				
				if(selectedString.equals("Professional")){
					
					 String code=((String) payment_register_name.getSelectedItem().getValue()).split("-")[0];
					Professional_masterBean professionalMasterBean = professionalMasterDAO.findByprofessional_code(code)[0];
				
					payment_register_address.setValue(professionalMasterBean.getProfessional_address());
					payment_register_cell_no.setValue((professionalMasterBean.getProfessional_cell1()));
                    
					
				}else{
					
					     String code=((String) payment_register_name.getSelectedItem().getValue()).split("-")[0];
						Referrer_masterBean referrerMasterBean = referrerMasterDAO.findByreferrer_code(code)[0];
						payment_register_address.setValue(referrerMasterBean.getReferrer_address());
						payment_register_cell_no.setValue(referrerMasterBean.getReferrer_mobile1());
	                    
					
				}
				
				
			}
			
			public void onChange$to_date() throws Exception{
				
			    DateFormat sqlDateFormatter = new SimpleDateFormat("yyyy-MM-dd");
				
				String name = payment_register_name.getSelectedItem().getValue().toString(); 
				
				int index = name.indexOf("-");
				
				name = name.substring(index+1,name.length());
				
				Date from = java.sql.Date.valueOf(sqlDateFormatter.format(from_date.getValue()));
				
				Date to = java.sql.Date.valueOf(sqlDateFormatter.format(to_date.getValue()));
				
				double volume = 0;
				
				if(payment_register_type.getSelectedItem() == null){
					
					alert("Payment Register Type is not selected");
					
				}else{
				
					if(payment_register_type.getSelectedItem().getLabel().equals("Professional")){
						
						volume = daoHandler.findVolume_prof(name, item_name_id.getSelectedItem().getValue().toString(),from, to);		
						System.out.println(volume);
						volume_payment_register.setValue(new BigDecimal(volume));
	
						
					}else {
						
						volume = daoHandler.findVolume_referred(name, item_name_id.getSelectedItem().getValue().toString(),from,to);
						
						volume_payment_register.setValue(new BigDecimal(volume));
					}
				
				}				
			}
			
			
		public void onChange$category_name_id()throws Exception {
			
			try {
				item_name_id.setModel(new ListModelList(itemMasterDAO.findBycategory_name(category_name_id.getSelectedItem().getLabel())));
			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
		}
		
		public void onChange$amount_paid() throws  Exception{
			
			if(amount_paid.getValue().doubleValue() > commission_amount.getValue().doubleValue()){
				
				alert("Amount can not pay more than commission amount");
				
			}
			
			due_amount.setValue(new BigDecimal(commission_amount.getValue().doubleValue() - amount_paid.getValue().doubleValue()));
			
		}
			
  }
	 

