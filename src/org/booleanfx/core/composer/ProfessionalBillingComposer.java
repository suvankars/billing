package org.booleanfx.core.composer;


import java.io.ByteArrayInputStream;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

import org.booleanfx.core.dao.Category_masterDAO;
import org.booleanfx.core.dao.Item_masterDAO;
import org.booleanfx.core.dao.Professional_masterDAO;
import org.python.antlr.PythonParser.file_input_return;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.util.GenericForwardComposer;
import org.zkoss.zul.Button;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Datebox;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.ListModelList;
import org.zkoss.zul.Textbox;

import com.domains.servlets.ConstantServlet;


public class ProfessionalBillingComposer extends GenericForwardComposer {
	
	Button show;
	
	Datebox fromdate,todate;
	Combobox category,item,professional;
	Category_masterDAO daoHandler = new Category_masterDAO();
	Item_masterDAO daoHandler1 = new Item_masterDAO();
	String fdate,tdate,cname,iname,pname;
	Professional_masterDAO daoHandler2 = new Professional_masterDAO();

	Iframe report;
	
	public void onCreate$win()throws Exception{
		
		category.setModel(new ListModelList(daoHandler.findAll()));
		
		item.setModel(new ListModelList(daoHandler1.findAll()));
		professional.setModel(new ListModelList(daoHandler2.findAll()));
	}
	
	
	public void onClick$show(){
		
		
		  //for bill generation
		
		JasperReport jasperReport;
		
		JasperPrint jasperPrint;
		
		String contextPath = ConstantServlet.contextPath ;
		
			try {
				
				DateFormat s = new SimpleDateFormat("dd-MM-yyyy");
				fdate = s.format(fromdate.getValue());
				tdate = s.format(todate.getValue());
		
				if(category.getSelectedItem() != null){
					
					cname = category.getSelectedItem().getValue().toString();
					
				}else{
					
					cname = "" ;
					
				}
				if(item.getSelectedItem() != null){
					
					iname = item.getSelectedItem().getValue().toString() ;					
					
				}else{
					
				    iname = "" ;	
					
				}if(professional.getSelectedItem() != null){
					
					pname = professional.getSelectedItem().getValue().toString() ;					
					
				}else{
					
				    pname = "" ;	
				}

										
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				
				Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/mammothshq","root","password");
											
				Map parameters = new HashMap();
				
				parameters.put("Title", "PDF");
				
				parameters.put("to date", tdate);
				
				parameters.put("from date",fdate);
               
				parameters.put("Category",cname);
				
				parameters.put("Item",iname);
				parameters.put("ProfessionalName",pname);
										  
				jasperReport = JasperCompileManager.compileReport(contextPath+"reports/ProfessionalBilling.jrxml");
				
				jasperPrint = JasperFillManager.fillReport(jasperReport, parameters,conn);
				
				JasperExportManager.exportReportToPdfFile(jasperPrint, contextPath+"reports/ProfessionalBilling.pdf");
				
				JasperExportManager.exportReportToPdf(jasperPrint);
				
				
				FileInputStream fin = null;

				try {
					
					/*  1. Remove the hard-coding filepath
					  2. Create a constant servlet to get the realpath
					  3. Create the PDF report from the code.*/
				
					
					File file = new File(contextPath + "reports/ProfessionalBilling.pdf");

					fin = new FileInputStream(file);

					int bufsize = (int) file.length();

					byte[] buf = new byte[bufsize];

					fin.read(buf);

					// prepare the AMedia for iframe
					final InputStream mediais = new ByteArrayInputStream(buf);

					final AMedia amedia = new AMedia("ProfessionalBilling.pdf", "pdf",
							"application/pdf", mediais);

					// set iframe content
	                if(file!=null){
					report.setContent(amedia);
	                }

				} catch (Exception ex) {
					throw new RuntimeException(ex);
				} finally {
					if (fin != null) {

						try {
							fin.close();
						} catch (Exception e) {
						}
					}
				}

			} catch (Exception e) {
				
				e.printStackTrace();
			}
			
			
			
	}
	
	

}
