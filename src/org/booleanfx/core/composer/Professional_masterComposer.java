	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;
import org.zkoss.zul.ListModel;
import org.zkoss.zul.Longbox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;
import org.zkoss.zul.event.ListDataListener;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Professional_masterComposer extends GenericForwardComposer{

			//Define Common members here

             Category_masterDAO categoryMasterDAO = new Category_masterDAO();		
		Item_masterDAO itemMasterDAO = new Item_masterDAO();
		
			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox professional_code ;

			Textbox professional_name ;

			Textbox professional_cell1 ;

			Textbox professional_cell2 ;

			Textbox professional_address ;

			Textbox professional_address_telephone ;

			Combobox category_name_id ;

			Combobox item_name_id ;

			Textbox target_volume1 ;

			Textbox target_volume_commission_percent1 ;

			Textbox target_volume2 ;

			Textbox target_volume_commission2 ;

			Textbox target_volume3 ;

			Textbox target_volume_commission3 ;

			Professional_masterDAO daoHandler = new Professional_masterDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				  

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew() throws Exception {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				  

				mode = "add";

				selectedId = -1 ;
			
				String pro_code = Codegenerator.getInstance().getNewEmploytNumber();
				
				professional_code.setValue(pro_code);
				category_name_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
				item_name_id.setModel(new ListModelList(itemMasterDAO.findAll()));
			}

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Professional_masterBean webbean = new Professional_masterBean();

				try{

				//	webbean.setId(Integer.parseInt(id.getValue()));


					
					

					
					webbean.setProfessional_code(professional_code.getValue());
					webbean.setProfessional_name(professional_name.getValue());

					webbean.setProfessional_cell1(professional_cell1.getValue());

					webbean.setProfessional_cell2(professional_cell2.getValue());

					webbean.setProfessional_address(professional_address.getValue());

					webbean.setProfessional_address_telephone(professional_address_telephone.getValue());

					webbean.setCategory_name_id(Integer.parseInt(category_name_id.getSelectedItem().getValue().toString()));

					webbean.setItem_name_id(Integer.parseInt(item_name_id.getSelectedItem().getValue().toString()));

					webbean.setTarget_volume1(target_volume1.getValue());

					webbean.setTarget_volume_commission_percent1(target_volume_commission_percent1.getValue());

					webbean.setTarget_volume2(target_volume2.getValue());

					webbean.setTarget_volume_commission2(target_volume_commission2.getValue());

					webbean.setTarget_volume3(target_volume3.getValue());

					webbean.setTarget_volume_commission3(target_volume_commission3.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					  

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				report.setVisible(false);

				  

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);


				Professional_masterBean webbean  = (Professional_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Professional_masterBean webbean  = (Professional_masterBean)listbox.getSelectedItem().getValue();
				
				//Category_masterBean categoryMasterBean = (Category_masterBean)listbox.getSelectedItem().getValue();
				selectedId = webbean.getId();

				mode = "edit";

				try{

				//	id.setValue(""+webbean.getId());

					professional_code.setValue(webbean.getProfessional_code());

					professional_name.setValue(webbean.getProfessional_name());

					professional_cell1.setValue((webbean.getProfessional_cell1()));

					professional_cell2.setValue((webbean.getProfessional_cell2()));

					professional_address.setValue(webbean.getProfessional_address());

					professional_address_telephone.setValue((webbean.getProfessional_address_telephone()));

					
					
					
					category_name_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
					
					Category_masterBean[] categoryBean = categoryMasterDAO.findByid(webbean.getCategory_name_id());
										
					category_name_id.setValue(categoryBean[0].getCategory_name());
					
					
					item_name_id.setModel(new ListModelList(itemMasterDAO.findAll()));
                     
					Item_masterBean[] itemMasterBeans = itemMasterDAO.findByid(webbean.getItem_name_id());
					
					item_name_id.setValue(itemMasterBeans[0].getItem_name());

					
					
					
					
					target_volume1.setValue(webbean.getTarget_volume1());
					
					target_volume_commission_percent1.setValue(webbean.getTarget_volume_commission_percent1());

					target_volume2.setValue(webbean.getTarget_volume2());

					target_volume_commission2.setValue(webbean.getTarget_volume_commission2());

					target_volume3.setValue(webbean.getTarget_volume3());

					target_volume_commission3.setValue(webbean.getTarget_volume_commission3());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Professional_masterBean webbean  = (Professional_masterBean)listbox.getSelectedItem().getValue();

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

					alert("Data deleted successfully");

						listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				professional_code.setValue("");

				professional_name.setValue("");

				professional_cell1.setValue("");

				professional_cell2.setValue("");

				professional_address.setValue("");

				professional_address_telephone.setValue("");

				category_name_id.setValue("");

				item_name_id.setValue("");

				target_volume1.setValue("");

				target_volume_commission_percent1.setValue("");

				target_volume2.setValue("");

				target_volume_commission2.setValue("");

				target_volume3.setValue("");

				target_volume_commission3.setValue("");

				listbox.setSelectedItem(null);

			}
			
			
			public void onChange$category_name_id()throws Exception {
				
				try {
					item_name_id.setModel(new ListModelList(itemMasterDAO.findBycategory_name(category_name_id.getSelectedItem().getLabel())));
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				
			}

	}

