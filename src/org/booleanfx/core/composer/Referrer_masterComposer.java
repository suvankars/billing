	package org.booleanfx.core.composer ;

	import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zk.ui.util.GenericForwardComposer;

	import org.zkoss.zkmax.zul.Tablelayout;

	import org.zkoss.zul.Combobox;
import org.zkoss.zul.Intbox;
import org.zkoss.zul.Longbox;

	import org.zkoss.zul.Datebox;

	import org.zkoss.zul.Decimalbox;

	import org.zkoss.zul.Div;

	import org.zkoss.zul.Iframe;

	import org.zkoss.zul.Label;

	import org.zkoss.zul.ListModelList;

	import org.zkoss.zul.Listbox;

	import org.zkoss.zul.Menuitem;

	import org.zkoss.zul.Messagebox;

	import org.zkoss.zul.Textbox;

	import java.io.*;

	import java.math.BigDecimal;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.utils.* ;

	import org.booleanfx.core.servlet.* ;

import org.booleanfx.core.dao.* ;

	public class Referrer_masterComposer extends GenericForwardComposer{

			//Define Common members here
	     Category_masterDAO categoryMasterDAO = new Category_masterDAO();		
			Item_masterDAO itemMasterDAO = new Item_masterDAO();
			Div zmenu, zgrid, zform, main;

			Menuitem mLoad, mEdit, mNew, mSave, mDelete, mBack;

			Listbox listbox;

			Label editId;

			Iframe report;

			String mode = "";

			int selectedId = -1 ;
			
			int value ;

			//Define ZUL Specific Fields here

			//Textbox id ;

			Textbox referrer_code ;

			Textbox referrer_name ;

			Textbox referrer_mobile1 ;

			Textbox referrer_address ;

			Textbox referrer_telephone ;

			Combobox category_id ;

			Combobox item_id ;

			Textbox target_volume1 ;

			Textbox target_commission1 ;

			Textbox referrer_mobile2 ;

			Textbox target_volume2 ;

			Textbox target_commission2 ;

			Textbox target_volume3 ;

			Textbox target_commission3 ;
			CodeDAO codeDAO = new CodeDAO();
			Referrer_masterDAO daoHandler = new Referrer_masterDAO();

			public void onCreate$main() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				  

				report.setVisible(false);

				selectedId = -1 ;

			}

			public void onClick$mNew()throws Exception {

				//formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				report.setVisible(false);

				  

				mode = "add";

				selectedId = -1 ;
	/*if(UUIDGenerator.referrer_code == null){
					
					value = Integer.parseInt(UUIDGenerator.getInstance().getFirstTimecategory_code());
					
				}else{
					
					value = Integer.parseInt(UUIDGenerator.getInstance().getNextcategory_code());
				}


				codeDAO.updateCategorycode();*/
				
				String ref_code = Codegenerator.getInstance().getNewEmploytNumber();
				
				referrer_code.setValue(ref_code);
				category_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
				item_id.setModel(new ListModelList(itemMasterDAO.findAll()));
			

				

			}

			

			public void onClick$mLoad() {

				try {

					listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void onClick$mSave() {

				Referrer_masterBean webbean = new Referrer_masterBean();

				try{

				//	webbean.setId(Integer.parseInt(id.getValue()));

					webbean.setReferrer_code(referrer_code.getValue());

					webbean.setReferrer_name(referrer_name.getValue());

					webbean.setReferrer_mobile1(referrer_mobile1.getValue());

					webbean.setReferrer_address(referrer_address.getValue());

					webbean.setReferrer_telephone(referrer_telephone.getValue());

					webbean.setCategory_id(Integer.parseInt(category_id.getSelectedItem().getValue().toString()));

					webbean.setItem_id(Integer.parseInt(item_id.getSelectedItem().getValue().toString()));

					webbean.setTarget_volume1(target_volume1.getValue());

					webbean.setTarget_commission1(target_commission1.getValue());

					webbean.setReferrer_mobile2(referrer_mobile2.getValue());

					webbean.setTarget_volume2(target_volume2.getValue());

					webbean.setTarget_commission2(target_commission2.getValue());

					webbean.setTarget_volume3(target_volume3.getValue());

					webbean.setTarget_commission3(target_commission3.getValue());

					if(selectedId == -1){

						selectedId = UUIDGenerator.getNextUUID() ;

						webbean.setId(selectedId);

						daoHandler.insertAllCols(webbean);

					}else{

						webbean.setId(selectedId);

						daoHandler.updateAllCols(webbean);

					}

					mLoad.setDisabled(false);

					mEdit.setDisabled(true);

					mNew.setDisabled(false);

					mSave.setDisabled(true);

					mDelete.setDisabled(true);

					mBack.setDisabled(false);

					report.setVisible(false);

					  

					listbox.setModel(new ListModelList(daoHandler.findAll()));

					Messagebox.show("Data saved successfully", "Information", Messagebox.OK, Messagebox.INFORMATION);

					formReset();

				}catch(Exception e){

					try{

					Messagebox.show("Data saving failed", "Error", Messagebox.OK, Messagebox.ERROR);

					}catch(Exception ex){}

					e.printStackTrace();

				}

			}

			public void onClick$mBack() {

				//formReset();

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(false);

				mSave.setDisabled(true);

				mDelete.setDisabled(true);

				mBack.setDisabled(true);

				report.setVisible(false);

				  

				selectedId = -1 ;

			}

			public void onSelect$listbox() {

				mLoad.setDisabled(false);

				mEdit.setDisabled(false);

				mNew.setDisabled(true);

				mSave.setDisabled(true);

				mDelete.setDisabled(false);

				mBack.setDisabled(false);

				

				Referrer_masterBean webbean  = (Referrer_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

			}

			public void onClick$mEdit(){

				mLoad.setDisabled(false);

				mEdit.setDisabled(true);

				mNew.setDisabled(true);

				mSave.setDisabled(false);

				mDelete.setDisabled(true);

				mBack.setDisabled(false);

				Referrer_masterBean webbean  = (Referrer_masterBean)listbox.getSelectedItem().getValue();

				selectedId = webbean.getId();

				mode = "edit";

				try{

				//	id.setValue(""+webbean.getId());

					referrer_code.setValue(webbean.getReferrer_code());

					referrer_name.setValue(webbean.getReferrer_name());

					referrer_mobile1.setValue(webbean.getReferrer_mobile1());

					referrer_address.setValue(webbean.getReferrer_address());

					referrer_telephone.setValue(webbean.getReferrer_telephone());

					
category_id.setModel(new ListModelList(categoryMasterDAO.findAll()));
					
					Category_masterBean[] categoryBean = categoryMasterDAO.findByid(webbean.getCategory_id());
										
					category_id.setValue(categoryBean[0].getCategory_name());
					
					
					item_id.setModel(new ListModelList(itemMasterDAO.findAll()));
                     
					Item_masterBean[] itemMasterBeans = itemMasterDAO.findByid(webbean.getItem_id());
					
					item_id.setValue(itemMasterBeans[0].getItem_name());

					
					
				

					target_volume1.setValue(webbean.getTarget_volume1());

					target_commission1.setValue(webbean.getTarget_commission1());

					referrer_mobile2.setValue(webbean.getReferrer_mobile2());

					target_volume2.setValue(webbean.getTarget_volume2());

					target_commission2.setValue(webbean.getTarget_commission2());

					target_volume3.setValue(webbean.getTarget_volume3());

					target_commission3.setValue(webbean.getTarget_commission3());

					if(selectedId == -1){

						try{

							Messagebox.show("Invalid Edit Key", "Error", Messagebox.OK, Messagebox.ERROR);

						}catch(Exception ex){}

					}

				}catch(Exception e){

					e.printStackTrace();

				}

			}

			public void onClick$mDelete() {

				try {

					Referrer_masterBean webbean  = (Referrer_masterBean)listbox.getSelectedItem().getValue();

						selectedId = webbean.getId();

						daoHandler.deleteByid(selectedId);

					alert("Data deleted successfully");

						listbox.setModel(new ListModelList(daoHandler.findAll()));

				} catch (Exception e) {

					e.printStackTrace();

				}

			}

			public void formReset(){

				selectedId = -1;

				mode = "new";

				//id.setValue("");

				//referrer_code.setValue("");

				referrer_name.setValue("");

				referrer_mobile1.setValue("");

				referrer_address.setValue("");

				referrer_telephone.setValue("");

				category_id.setValue("");

				item_id.setValue("");

				target_volume1.setValue("");

				target_commission1.setValue("");

				referrer_mobile2.setValue("");

				target_volume2.setValue("");

				target_commission2.setValue("");

				target_volume3.setValue("");

				target_commission3.setValue("");

				listbox.setSelectedItem(null);

			}
			
			
			
			public void onChange$category_id()throws Exception {
				
				try {
					item_id.setModel(new ListModelList(itemMasterDAO.findBycategory_name(category_id.getSelectedItem().getLabel())));
				} catch (Exception e) {
					
					e.printStackTrace();
				}
				
			}

	}

