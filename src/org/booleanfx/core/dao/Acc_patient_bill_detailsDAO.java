	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Acc_patient_bill_detailsDAO {

		public Acc_patient_bill_detailsBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findBybill_header_id(int bill_header_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_header_id = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE bill_header_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_header_id );

				preparedStatement.setInt(1,bill_header_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_cate_name(String line_cate_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_cate_name = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_cate_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_cate_name );

				preparedStatement.setString(1,line_cate_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_name(String line_item_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_name = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_name );

				preparedStatement.setString(1,line_item_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_qty(double line_item_qty )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_qty = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_qty = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_qty );

				preparedStatement.setDouble(1,line_item_qty); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_sl_num(String line_item_sl_num )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_sl_num = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_sl_num = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_sl_num );

				preparedStatement.setString(1,line_item_sl_num); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_model(String line_item_model )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_model = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_model = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_model );

				preparedStatement.setString(1,line_item_model); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_price(double line_item_price )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_price = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_price = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_price );

				preparedStatement.setDouble(1,line_item_price); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_discount(double line_item_discount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_discount = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_discount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_discount );

				preparedStatement.setDouble(1,line_item_discount); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_tax_name(String line_item_tax_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_tax_name = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_tax_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_tax_name );

				preparedStatement.setString(1,line_item_tax_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_item_tax_amt(double line_item_tax_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_item_tax_amt = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_item_tax_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_item_tax_amt );

				preparedStatement.setDouble(1,line_item_tax_amt); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_detailsBean[] findByline_gross_amt(double line_gross_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_detailsBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLine_gross_amt = "SELECT  id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt FROM acc_patient_bill_details WHERE line_gross_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLine_gross_amt );

				preparedStatement.setDouble(1,line_gross_amt); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_detailsBean bean = new Acc_patient_bill_detailsBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_header_id( resultSet.getInt("bill_header_id"));

					bean.setLine_cate_name( resultSet.getString("line_cate_name"));

					bean.setLine_item_name( resultSet.getString("line_item_name"));

					bean.setLine_item_qty( resultSet.getDouble("line_item_qty"));

					bean.setLine_item_sl_num( resultSet.getString("line_item_sl_num"));

					bean.setLine_item_model( resultSet.getString("line_item_model"));

					bean.setLine_item_price( resultSet.getDouble("line_item_price"));

					bean.setLine_item_discount( resultSet.getDouble("line_item_discount"));

					bean.setLine_item_tax_name( resultSet.getString("line_item_tax_name"));

					bean.setLine_item_tax_amt( resultSet.getDouble("line_item_tax_amt"));

					bean.setLine_gross_amt( resultSet.getDouble("line_gross_amt"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_detailsBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_detailsBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Acc_patient_bill_detailsBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO acc_patient_bill_details( id, bill_header_id, line_cate_name, line_item_name, line_item_qty, line_item_sl_num, line_item_model, line_item_price, line_item_discount, line_item_tax_name, line_item_tax_amt, line_gross_amt ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setInt(2, bean.getBill_header_id());

				preparedStatement.setString(3, bean.getLine_cate_name());

				preparedStatement.setString(4, bean.getLine_item_name());

				preparedStatement.setDouble(5, bean.getLine_item_qty());

				preparedStatement.setString(6, bean.getLine_item_sl_num());

				preparedStatement.setString(7, bean.getLine_item_model());

				preparedStatement.setDouble(8, bean.getLine_item_price());

				preparedStatement.setDouble(9, bean.getLine_item_discount());

				preparedStatement.setString(10, bean.getLine_item_tax_name());

				preparedStatement.setDouble(11, bean.getLine_item_tax_amt());

				preparedStatement.setDouble(12, bean.getLine_gross_amt());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Acc_patient_bill_detailsBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE acc_patient_bill_details SET  id = ?, bill_header_id = ?, line_cate_name = ?, line_item_name = ?, line_item_qty = ?, line_item_sl_num = ?, line_item_model = ?, line_item_price = ?, line_item_discount = ?, line_item_tax_name = ?, line_item_tax_amt = ?, line_gross_amt = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setInt(2, bean.getBill_header_id());

				preparedStatement.setString(3, bean.getLine_cate_name());

				preparedStatement.setString(4, bean.getLine_item_name());

				preparedStatement.setDouble(5, bean.getLine_item_qty());

				preparedStatement.setString(6, bean.getLine_item_sl_num());

				preparedStatement.setString(7, bean.getLine_item_model());

				preparedStatement.setDouble(8, bean.getLine_item_price());

				preparedStatement.setDouble(9, bean.getLine_item_discount());

				preparedStatement.setString(10, bean.getLine_item_tax_name());

				preparedStatement.setDouble(11, bean.getLine_item_tax_amt());

				preparedStatement.setDouble(12, bean.getLine_gross_amt());

				preparedStatement.setInt(13, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM acc_patient_bill_details " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM acc_patient_bill_details WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_header_id(int bill_header_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_header_id = "DELETE FROM acc_patient_bill_details WHERE bill_header_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_header_id );

				preparedStatement.setInt(1,bill_header_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_cate_name(String line_cate_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_cate_name = "DELETE FROM acc_patient_bill_details WHERE line_cate_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_cate_name );

				preparedStatement.setString(1,line_cate_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_name(String line_item_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_name = "DELETE FROM acc_patient_bill_details WHERE line_item_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_name );

				preparedStatement.setString(1,line_item_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_qty(double line_item_qty )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_qty = "DELETE FROM acc_patient_bill_details WHERE line_item_qty = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_qty );

				preparedStatement.setDouble(1,line_item_qty); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_sl_num(String line_item_sl_num )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_sl_num = "DELETE FROM acc_patient_bill_details WHERE line_item_sl_num = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_sl_num );

				preparedStatement.setString(1,line_item_sl_num); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_model(String line_item_model )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_model = "DELETE FROM acc_patient_bill_details WHERE line_item_model = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_model );

				preparedStatement.setString(1,line_item_model); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_price(double line_item_price )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_price = "DELETE FROM acc_patient_bill_details WHERE line_item_price = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_price );

				preparedStatement.setDouble(1,line_item_price); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_discount(double line_item_discount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_discount = "DELETE FROM acc_patient_bill_details WHERE line_item_discount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_discount );

				preparedStatement.setDouble(1,line_item_discount); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_tax_name(String line_item_tax_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_tax_name = "DELETE FROM acc_patient_bill_details WHERE line_item_tax_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_tax_name );

				preparedStatement.setString(1,line_item_tax_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_item_tax_amt(double line_item_tax_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_item_tax_amt = "DELETE FROM acc_patient_bill_details WHERE line_item_tax_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_item_tax_amt );

				preparedStatement.setDouble(1,line_item_tax_amt); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByline_gross_amt(double line_gross_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLine_gross_amt = "DELETE FROM acc_patient_bill_details WHERE line_gross_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLine_gross_amt );

				preparedStatement.setDouble(1,line_gross_amt); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

