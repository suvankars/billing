	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Acc_patient_bill_headerDAO {

		public Acc_patient_bill_headerBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header order by bill_no desc" ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));
					
					bean.setNaration( resultSet.getString("naration"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));
					
					bean.setName_of_card( resultSet.getString("naration"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_no(String bill_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_no = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE bill_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_no );

				preparedStatement.setString(1,bill_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					bean.setName_of_card( resultSet.getString("naration"));
					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_date(String bill_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_date = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE bill_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_date );

				preparedStatement.setString(1,bill_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));
				
					bean.setName_of_card( resultSet.getString("naration"));
					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_time(String bill_time )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_time = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bill_time = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_time );

				preparedStatement.setString(1,bill_time); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBycustomer_type(String customer_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCustomer_type = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE customer_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCustomer_type );

				preparedStatement.setString(1,customer_type); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_reg_no(String patient_reg_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_reg_no = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE patient_reg_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_reg_no );

				preparedStatement.setString(1,patient_reg_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					bean.setName_of_card( resultSet.getString("naration"));
					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_reg_charges(double patient_reg_charges )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_reg_charges = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_reg_charges = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_reg_charges );

				preparedStatement.setDouble(1,patient_reg_charges); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_name = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_name );

				preparedStatement.setString(1,patient_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					bean.setName_of_card( resultSet.getString("naration"));

					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_age = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_sex = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_address(String patient_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_address = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_address );

				preparedStatement.setString(1,patient_address); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_telephone(String patient_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_telephone = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_telephone );

				preparedStatement.setString(1,patient_telephone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBypatient_pincode(String patient_pincode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_pincode = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE patient_pincode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_pincode );

				preparedStatement.setString(1,patient_pincode); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferred_by = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferred_by );

				preparedStatement.setString(1,referred_by); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));
					
					bean.setName_of_card( resultSet.getString("naration"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByprofessional_by(String professional_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_by = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card,naration FROM acc_patient_bill_header WHERE professional_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_by );

				preparedStatement.setString(1,professional_by); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					bean.setName_of_card( resultSet.getString("naration"));
					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByitem_discount(double item_discount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_discount = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE item_discount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_discount );

				preparedStatement.setDouble(1,item_discount); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_total_amt(double bill_total_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_total_amt = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bill_total_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_total_amt );

				preparedStatement.setDouble(1,bill_total_amt); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByamt_received(double amt_received )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAmt_received = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE amt_received = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAmt_received );

				preparedStatement.setDouble(1,amt_received); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_due_amt(double bill_due_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_due_amt = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bill_due_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_due_amt );

				preparedStatement.setDouble(1,bill_due_amt); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybill_payment_mode(String bill_payment_mode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBill_payment_mode = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bill_payment_mode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBill_payment_mode );

				preparedStatement.setString(1,bill_payment_mode); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybank_br(String bank_br )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBank_br = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bank_br = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBank_br );

				preparedStatement.setString(1,bank_br); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBycheque_no(String cheque_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCheque_no = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE cheque_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCheque_no );

				preparedStatement.setString(1,cheque_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBycheque_date(String cheque_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCheque_date = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE cheque_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCheque_date );

				preparedStatement.setString(1,cheque_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBybank_name(String bank_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByBank_name = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE bank_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByBank_name );

				preparedStatement.setString(1,bank_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBycard_no(String card_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCard_no = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE card_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCard_no );

				preparedStatement.setString(1,card_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findBycard_expiry_dt(String card_expiry_dt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCard_expiry_dt = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE card_expiry_dt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCard_expiry_dt );

				preparedStatement.setString(1,card_expiry_dt); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Acc_patient_bill_headerBean[] findByname_of_card(String name_of_card )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByName_of_card = "SELECT  id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card FROM acc_patient_bill_header WHERE name_of_card = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByName_of_card );

				preparedStatement.setString(1,name_of_card); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setBill_date( resultSet.getString("bill_date"));

					bean.setBill_time( resultSet.getString("bill_time"));

					bean.setCustomer_type( resultSet.getString("customer_type"));

					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					bean.setPatient_reg_charges( resultSet.getDouble("patient_reg_charges"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_address( resultSet.getString("patient_address"));

					bean.setPatient_telephone( resultSet.getString("patient_telephone"));

					bean.setPatient_pincode( resultSet.getString("patient_pincode"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setProfessional_by( resultSet.getString("professional_by"));

					bean.setItem_discount( resultSet.getDouble("item_discount"));

					bean.setBill_total_amt( resultSet.getDouble("bill_total_amt"));

					bean.setAmt_received( resultSet.getDouble("amt_received"));

					bean.setBill_due_amt( resultSet.getDouble("bill_due_amt"));

					bean.setBill_payment_mode( resultSet.getString("bill_payment_mode"));

					bean.setBank_br( resultSet.getString("bank_br"));

					bean.setCheque_no( resultSet.getString("cheque_no"));

					bean.setCheque_date( resultSet.getString("cheque_date"));

					bean.setBank_name( resultSet.getString("bank_name"));

					bean.setCard_no( resultSet.getString("card_no"));

					bean.setCard_expiry_dt( resultSet.getString("card_expiry_dt"));

					bean.setName_of_card( resultSet.getString("name_of_card"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Acc_patient_bill_headerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO acc_patient_bill_header( id, bill_no, bill_date, bill_time, customer_type, patient_reg_no, patient_reg_charges, patient_name, patient_age, patient_sex, patient_address, patient_telephone, patient_pincode, referred_by, professional_by, item_discount, bill_total_amt, amt_received, bill_due_amt, bill_payment_mode, bank_br, cheque_no, cheque_date, bank_name, card_no, card_expiry_dt, name_of_card ,naration) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? ,? )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getBill_no());

				preparedStatement.setString(3, bean.getBill_date());

				preparedStatement.setString(4, bean.getBill_time());

				preparedStatement.setString(5, bean.getCustomer_type());

				preparedStatement.setString(6, bean.getPatient_reg_no());

				preparedStatement.setDouble(7, bean.getPatient_reg_charges());

				preparedStatement.setString(8, bean.getPatient_name());

				preparedStatement.setInt(9, bean.getPatient_age());

				preparedStatement.setString(10, bean.getPatient_sex());

				preparedStatement.setString(11, bean.getPatient_address());

				preparedStatement.setString(12, bean.getPatient_telephone());

				preparedStatement.setString(13, bean.getPatient_pincode());

				preparedStatement.setString(14, bean.getReferred_by());

				preparedStatement.setString(15, bean.getProfessional_by());

				preparedStatement.setDouble(16, bean.getItem_discount());

				preparedStatement.setDouble(17, bean.getBill_total_amt());

				preparedStatement.setDouble(18, bean.getAmt_received());

				preparedStatement.setDouble(19, bean.getBill_due_amt());

				preparedStatement.setString(20, bean.getBill_payment_mode());

				preparedStatement.setString(21, bean.getBank_br());

				preparedStatement.setString(22, bean.getCheque_no());

				preparedStatement.setString(23, bean.getCheque_date());

				preparedStatement.setString(24, bean.getBank_name());

				preparedStatement.setString(25, bean.getCard_no());

				preparedStatement.setString(26, bean.getCard_expiry_dt());

				preparedStatement.setString(27, bean.getName_of_card());
				
				preparedStatement.setString(28, bean.getNaration());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Acc_patient_bill_headerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE acc_patient_bill_header SET  id = ?, bill_no = ?, bill_date = ?, bill_time = ?, customer_type = ?, patient_reg_no = ?, patient_reg_charges = ?, patient_name = ?, patient_age = ?, patient_sex = ?, patient_address = ?, patient_telephone = ?, patient_pincode = ?, referred_by = ?, professional_by = ?, item_discount = ?, bill_total_amt = ?, amt_received = ?, bill_due_amt = ?, bill_payment_mode = ?, bank_br = ?, cheque_no = ?, cheque_date = ?, bank_name = ?, card_no = ?, card_expiry_dt = ?, name_of_card = ?,naration = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getBill_no());

				preparedStatement.setString(3, bean.getBill_date());

				preparedStatement.setString(4, bean.getBill_time());

				preparedStatement.setString(5, bean.getCustomer_type());

				preparedStatement.setString(6, bean.getPatient_reg_no());

				preparedStatement.setDouble(7, bean.getPatient_reg_charges());

				preparedStatement.setString(8, bean.getPatient_name());

				preparedStatement.setInt(9, bean.getPatient_age());

				preparedStatement.setString(10, bean.getPatient_sex());

				preparedStatement.setString(11, bean.getPatient_address());

				preparedStatement.setString(12, bean.getPatient_telephone());

				preparedStatement.setString(13, bean.getPatient_pincode());

				preparedStatement.setString(14, bean.getReferred_by());

				preparedStatement.setString(15, bean.getProfessional_by());

				preparedStatement.setDouble(16, bean.getItem_discount());

				preparedStatement.setDouble(17, bean.getBill_total_amt());

				preparedStatement.setDouble(18, bean.getAmt_received());

				preparedStatement.setDouble(19, bean.getBill_due_amt());

				preparedStatement.setString(20, bean.getBill_payment_mode());

				preparedStatement.setString(21, bean.getBank_br());

				preparedStatement.setString(22, bean.getCheque_no());

				preparedStatement.setString(23, bean.getCheque_date());

				preparedStatement.setString(24, bean.getBank_name());

				preparedStatement.setString(25, bean.getCard_no());

				preparedStatement.setString(26, bean.getCard_expiry_dt());

				preparedStatement.setString(27, bean.getName_of_card());

				preparedStatement.setString(28, bean.getNaration());
				
				preparedStatement.setInt(29, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM acc_patient_bill_header " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM acc_patient_bill_header WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_no(String bill_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_no = "DELETE FROM acc_patient_bill_header WHERE bill_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_no );

				preparedStatement.setString(1,bill_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_date(String bill_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_date = "DELETE FROM acc_patient_bill_header WHERE bill_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_date );

				preparedStatement.setString(1,bill_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_time(String bill_time )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_time = "DELETE FROM acc_patient_bill_header WHERE bill_time = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_time );

				preparedStatement.setString(1,bill_time); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycustomer_type(String customer_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCustomer_type = "DELETE FROM acc_patient_bill_header WHERE customer_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCustomer_type );

				preparedStatement.setString(1,customer_type); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_reg_no(String patient_reg_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_reg_no = "DELETE FROM acc_patient_bill_header WHERE patient_reg_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_reg_no );

				preparedStatement.setString(1,patient_reg_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_reg_charges(double patient_reg_charges )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_reg_charges = "DELETE FROM acc_patient_bill_header WHERE patient_reg_charges = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_reg_charges );

				preparedStatement.setDouble(1,patient_reg_charges); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_name = "DELETE FROM acc_patient_bill_header WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_name );

				preparedStatement.setString(1,patient_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_age = "DELETE FROM acc_patient_bill_header WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_sex = "DELETE FROM acc_patient_bill_header WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_address(String patient_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_address = "DELETE FROM acc_patient_bill_header WHERE patient_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_address );

				preparedStatement.setString(1,patient_address); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_telephone(String patient_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_telephone = "DELETE FROM acc_patient_bill_header WHERE patient_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_telephone );

				preparedStatement.setString(1,patient_telephone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_pincode(String patient_pincode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_pincode = "DELETE FROM acc_patient_bill_header WHERE patient_pincode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_pincode );

				preparedStatement.setString(1,patient_pincode); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferred_by = "DELETE FROM acc_patient_bill_header WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferred_by );

				preparedStatement.setString(1,referred_by); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_by(String professional_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_by = "DELETE FROM acc_patient_bill_header WHERE professional_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_by );

				preparedStatement.setString(1,professional_by); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_discount(double item_discount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_discount = "DELETE FROM acc_patient_bill_header WHERE item_discount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_discount );

				preparedStatement.setDouble(1,item_discount); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_total_amt(double bill_total_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_total_amt = "DELETE FROM acc_patient_bill_header WHERE bill_total_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_total_amt );

				preparedStatement.setDouble(1,bill_total_amt); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByamt_received(double amt_received )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAmt_received = "DELETE FROM acc_patient_bill_header WHERE amt_received = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAmt_received );

				preparedStatement.setDouble(1,amt_received); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_due_amt(double bill_due_amt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_due_amt = "DELETE FROM acc_patient_bill_header WHERE bill_due_amt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_due_amt );

				preparedStatement.setDouble(1,bill_due_amt); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybill_payment_mode(String bill_payment_mode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBill_payment_mode = "DELETE FROM acc_patient_bill_header WHERE bill_payment_mode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBill_payment_mode );

				preparedStatement.setString(1,bill_payment_mode); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybank_br(String bank_br )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBank_br = "DELETE FROM acc_patient_bill_header WHERE bank_br = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBank_br );

				preparedStatement.setString(1,bank_br); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycheque_no(String cheque_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCheque_no = "DELETE FROM acc_patient_bill_header WHERE cheque_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCheque_no );

				preparedStatement.setString(1,cheque_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycheque_date(String cheque_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCheque_date = "DELETE FROM acc_patient_bill_header WHERE cheque_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCheque_date );

				preparedStatement.setString(1,cheque_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBybank_name(String bank_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByBank_name = "DELETE FROM acc_patient_bill_header WHERE bank_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByBank_name );

				preparedStatement.setString(1,bank_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycard_no(String card_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCard_no = "DELETE FROM acc_patient_bill_header WHERE card_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCard_no );

				preparedStatement.setString(1,card_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycard_expiry_dt(String card_expiry_dt )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCard_expiry_dt = "DELETE FROM acc_patient_bill_header WHERE card_expiry_dt = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCard_expiry_dt );

				preparedStatement.setString(1,card_expiry_dt); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByname_of_card(String name_of_card )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByName_of_card = "DELETE FROM acc_patient_bill_header WHERE name_of_card = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByName_of_card );

				preparedStatement.setString(1,name_of_card); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}
		
		public Acc_patient_bill_headerBean[] findAll1( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Acc_patient_bill_headerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT DISTINCT patient_reg_no FROM acc_patient_bill_header " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Acc_patient_bill_headerBean bean = new Acc_patient_bill_headerBean();

					
					bean.setPatient_reg_no( resultSet.getString("patient_reg_no"));

					
					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Acc_patient_bill_headerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Acc_patient_bill_headerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}


	}

