	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Access_matrixDAO {

		public Access_matrixBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByresourcecategory(String resourcecategory )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByResourcecategory = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE resourcecategory = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByResourcecategory );

				preparedStatement.setString(1,resourcecategory); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByresourcename(String resourcename )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByResourcename = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE resourcename = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByResourcename );

				preparedStatement.setString(1,resourcename); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByview(String view )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByView = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE view = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByView );

				preparedStatement.setString(1,view); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findBysave(String save )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectBySave = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE save = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectBySave );

				preparedStatement.setString(1,save); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByedit(String edit )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEdit = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE edit = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEdit );

				preparedStatement.setString(1,edit); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findBydelete(String delete )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByDelete = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE delete = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByDelete );

				preparedStatement.setString(1,delete); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findBylist(String list )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByList = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE list = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByList );

				preparedStatement.setString(1,list); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByprint(String print )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPrint = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE print = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPrint );

				preparedStatement.setString(1,print); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Access_matrixBean[] findByemployeeid(int employeeid )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Access_matrixBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEmployeeid = "SELECT  id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid FROM access_matrix WHERE employeeid = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEmployeeid );

				preparedStatement.setInt(1,employeeid); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Access_matrixBean bean = new Access_matrixBean();

					bean.setId( resultSet.getInt("id"));

					bean.setResourcecategory( resultSet.getString("resourcecategory"));

					bean.setResourcename( resultSet.getString("resourcename"));

					bean.setView( resultSet.getString("view"));

					bean.setSave( resultSet.getString("save"));

					bean.setEdit( resultSet.getString("edit"));

					bean.setDelete( resultSet.getString("delete"));

					bean.setList( resultSet.getString("list"));

					bean.setPrint( resultSet.getString("print"));

					bean.setEmployeeid( resultSet.getInt("employeeid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Access_matrixBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Access_matrixBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Access_matrixBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO access_matrix( id, resourcecategory, resourcename, view, save, edit, delete, list, print, employeeid ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getResourcecategory());

				preparedStatement.setString(3, bean.getResourcename());

				preparedStatement.setString(4, bean.getView());

				preparedStatement.setString(5, bean.getSave());

				preparedStatement.setString(6, bean.getEdit());

				preparedStatement.setString(7, bean.getDelete());

				preparedStatement.setString(8, bean.getList());

				preparedStatement.setString(9, bean.getPrint());

				preparedStatement.setInt(10, bean.getEmployeeid());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Access_matrixBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE access_matrix SET  id = ?, resourcecategory = ?, resourcename = ?, view = ?, save = ?, edit = ?, delete = ?, list = ?, print = ?, employeeid = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getResourcecategory());

				preparedStatement.setString(3, bean.getResourcename());

				preparedStatement.setString(4, bean.getView());

				preparedStatement.setString(5, bean.getSave());

				preparedStatement.setString(6, bean.getEdit());

				preparedStatement.setString(7, bean.getDelete());

				preparedStatement.setString(8, bean.getList());

				preparedStatement.setString(9, bean.getPrint());

				preparedStatement.setInt(10, bean.getEmployeeid());

				preparedStatement.setInt(11, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM access_matrix " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM access_matrix WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByresourcecategory(String resourcecategory )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByResourcecategory = "DELETE FROM access_matrix WHERE resourcecategory = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByResourcecategory );

				preparedStatement.setString(1,resourcecategory); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByresourcename(String resourcename )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByResourcename = "DELETE FROM access_matrix WHERE resourcename = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByResourcename );

				preparedStatement.setString(1,resourcename); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByview(String view )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByView = "DELETE FROM access_matrix WHERE view = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByView );

				preparedStatement.setString(1,view); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBysave(String save )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteBySave = "DELETE FROM access_matrix WHERE save = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteBySave );

				preparedStatement.setString(1,save); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByedit(String edit )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEdit = "DELETE FROM access_matrix WHERE edit = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEdit );

				preparedStatement.setString(1,edit); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBydelete(String delete )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByDelete = "DELETE FROM access_matrix WHERE delete = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByDelete );

				preparedStatement.setString(1,delete); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBylist(String list )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByList = "DELETE FROM access_matrix WHERE list = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByList );

				preparedStatement.setString(1,list); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprint(String print )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPrint = "DELETE FROM access_matrix WHERE print = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPrint );

				preparedStatement.setString(1,print); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByemployeeid(int employeeid )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEmployeeid = "DELETE FROM access_matrix WHERE employeeid = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEmployeeid );

				preparedStatement.setInt(1,employeeid); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

