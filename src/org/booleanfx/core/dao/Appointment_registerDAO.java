	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

import org.booleanfx.core.database.* ;

	public class Appointment_registerDAO {

		public Appointment_registerBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register order by entry_no" ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByentry_no(String entry_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEntry_no = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE entry_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEntry_no );

				preparedStatement.setString(1,entry_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByentry_date(Date entry_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEntry_date = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE entry_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEntry_date );

				preparedStatement.setDate(1,entry_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByappointment_date(String appointment_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAppointment_date = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE appointment_date  like ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAppointment_date );

				preparedStatement.setString(1,appointment_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBytherapist_name(String therapist_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTherapist_name = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE therapist_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTherapist_name );

				preparedStatement.setString(1,therapist_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBytherapist_phone(String therapist_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTherapist_phone = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE therapist_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTherapist_phone );

				preparedStatement.setString(1,therapist_phone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBypatient_regno(String patient_regno )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_regno = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE patient_regno = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_regno );

				preparedStatement.setString(1,patient_regno); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_name = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_name );

				preparedStatement.setString(1,patient_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_age = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_sex = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBycaller_phone(String caller_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCaller_phone = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE caller_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCaller_phone );

				preparedStatement.setString(1,caller_phone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferred_by = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferred_by );

				preparedStatement.setString(1,referred_by); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findByemployee_name(String employee_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEmployee_name = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE employee_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEmployee_name );

				preparedStatement.setString(1,employee_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Appointment_registerBean[] findBydescription(String description )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Appointment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByDescription = "SELECT  id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description FROM appointment_register WHERE description = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByDescription );

				preparedStatement.setString(1,description); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Appointment_registerBean bean = new Appointment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEntry_no( resultSet.getString("entry_no"));

					bean.setEntry_date( resultSet.getDate("entry_date"));

					bean.setAppointment_date( resultSet.getTimestamp("appointment_date"));

					bean.setTherapist_name( resultSet.getString("therapist_name"));

					bean.setTherapist_phone( resultSet.getString("therapist_phone"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setEmployee_name( resultSet.getString("employee_name"));

					bean.setDescription( resultSet.getString("description"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Appointment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Appointment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Appointment_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO appointment_register( id, entry_no, entry_date, appointment_date, therapist_name, therapist_phone, patient_regno, patient_name, patient_age, patient_sex, caller_phone, referred_by, employee_name, description ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getEntry_no());

				preparedStatement.setDate(3, bean.getEntry_date());

				preparedStatement.setTimestamp(4, bean.getAppointment_date());

				preparedStatement.setString(5, bean.getTherapist_name());

				preparedStatement.setString(6, bean.getTherapist_phone());

				preparedStatement.setString(7, bean.getPatient_regno());

				preparedStatement.setString(8, bean.getPatient_name());

				preparedStatement.setInt(9, bean.getPatient_age());

				preparedStatement.setString(10, bean.getPatient_sex());

				preparedStatement.setString(11, bean.getCaller_phone());

				preparedStatement.setString(12, bean.getReferred_by());

				preparedStatement.setString(13, bean.getEmployee_name());

				preparedStatement.setString(14, bean.getDescription());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Appointment_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE appointment_register SET  id = ?, entry_no = ?, entry_date = ?, appointment_date = ?, therapist_name = ?, therapist_phone = ?, patient_regno = ?, patient_name = ?, patient_age = ?, patient_sex = ?, caller_phone = ?, referred_by = ?, employee_name = ?, description = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getEntry_no());

				preparedStatement.setDate(3, bean.getEntry_date());

				preparedStatement.setTimestamp(4, bean.getAppointment_date());

				preparedStatement.setString(5, bean.getTherapist_name());

				preparedStatement.setString(6, bean.getTherapist_phone());

				preparedStatement.setString(7, bean.getPatient_regno());

				preparedStatement.setString(8, bean.getPatient_name());

				preparedStatement.setInt(9, bean.getPatient_age());

				preparedStatement.setString(10, bean.getPatient_sex());

				preparedStatement.setString(11, bean.getCaller_phone());

				preparedStatement.setString(12, bean.getReferred_by());

				preparedStatement.setString(13, bean.getEmployee_name());

				preparedStatement.setString(14, bean.getDescription());

				preparedStatement.setInt(15, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM appointment_register " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM appointment_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByentry_no(String entry_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEntry_no = "DELETE FROM appointment_register WHERE entry_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEntry_no );

				preparedStatement.setString(1,entry_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByentry_date(Date entry_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEntry_date = "DELETE FROM appointment_register WHERE entry_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEntry_date );

				preparedStatement.setDate(1,entry_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByappointment_date(Date appointment_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAppointment_date = "DELETE FROM appointment_register WHERE appointment_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAppointment_date );

				preparedStatement.setDate(1,appointment_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytherapist_name(String therapist_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTherapist_name = "DELETE FROM appointment_register WHERE therapist_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTherapist_name );

				preparedStatement.setString(1,therapist_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytherapist_phone(String therapist_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTherapist_phone = "DELETE FROM appointment_register WHERE therapist_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTherapist_phone );

				preparedStatement.setString(1,therapist_phone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_regno(String patient_regno )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_regno = "DELETE FROM appointment_register WHERE patient_regno = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_regno );

				preparedStatement.setString(1,patient_regno); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_name = "DELETE FROM appointment_register WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_name );

				preparedStatement.setString(1,patient_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_age = "DELETE FROM appointment_register WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_sex = "DELETE FROM appointment_register WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycaller_phone(String caller_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCaller_phone = "DELETE FROM appointment_register WHERE caller_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCaller_phone );

				preparedStatement.setString(1,caller_phone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferred_by = "DELETE FROM appointment_register WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferred_by );

				preparedStatement.setString(1,referred_by); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByemployee_name(String employee_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEmployee_name = "DELETE FROM appointment_register WHERE employee_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEmployee_name );

				preparedStatement.setString(1,employee_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBydescription(String description )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByDescription = "DELETE FROM appointment_register WHERE description = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByDescription );

				preparedStatement.setString(1,description); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}
		
	}

