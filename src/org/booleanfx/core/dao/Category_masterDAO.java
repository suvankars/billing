	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Category_masterDAO {

		public Category_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Category_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, category_name, category_code, category_prefix FROM category_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Category_masterBean bean = new Category_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setCategory_code( resultSet.getString("category_code"));

					bean.setCategory_prefix( resultSet.getString("category_prefix"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Category_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Category_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Category_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Category_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, category_name, category_code, category_prefix FROM category_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Category_masterBean bean = new Category_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setCategory_code( resultSet.getString("category_code"));

					bean.setCategory_prefix( resultSet.getString("category_prefix"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Category_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Category_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Category_masterBean[] findBycategory_name(String category_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Category_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_name = "SELECT  id, category_name, category_code, category_prefix FROM category_master WHERE category_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_name );

				preparedStatement.setString(1,category_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Category_masterBean bean = new Category_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setCategory_code( resultSet.getString("category_code"));

					bean.setCategory_prefix( resultSet.getString("category_prefix"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Category_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Category_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Category_masterBean[] findBycategory_code(String category_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Category_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_code = "SELECT  id, category_name, category_code, category_prefix FROM category_master WHERE category_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_code );

				preparedStatement.setString(1,category_code); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Category_masterBean bean = new Category_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setCategory_code( resultSet.getString("category_code"));

					bean.setCategory_prefix( resultSet.getString("category_prefix"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Category_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Category_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Category_masterBean[] findBycategory_prefix(String category_prefix )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Category_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_prefix = "SELECT  id, category_name, category_code, category_prefix FROM category_master WHERE category_prefix = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_prefix );

				preparedStatement.setString(1,category_prefix); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Category_masterBean bean = new Category_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setCategory_code( resultSet.getString("category_code"));

					bean.setCategory_prefix( resultSet.getString("category_prefix"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Category_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Category_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Category_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO category_master( id, category_name, category_code, category_prefix ) VALUES ( ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCategory_name());

				preparedStatement.setString(3, bean.getCategory_code());

				preparedStatement.setString(4, bean.getCategory_prefix());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Category_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE category_master SET  id = ?, category_name = ?, category_code = ?, category_prefix = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCategory_name());

				preparedStatement.setString(3, bean.getCategory_code());

				preparedStatement.setString(4, bean.getCategory_prefix());

				preparedStatement.setInt(5, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM category_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM category_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_name(String category_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_name = "DELETE FROM category_master WHERE category_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_name );

				preparedStatement.setString(1,category_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_code(String category_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_code = "DELETE FROM category_master WHERE category_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_code );

				preparedStatement.setString(1,category_code); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_prefix(String category_prefix )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_prefix = "DELETE FROM category_master WHERE category_prefix = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_prefix );

				preparedStatement.setString(1,category_prefix); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

