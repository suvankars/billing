 package org.booleanfx.core.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.booleanfx.core.bean.CodeBean;
import org.booleanfx.core.database.DatabaseHandler;
import org.booleanfx.core.utils.UUIDGenerator;

 public class CodeDAO {

	 public CodeBean findRSH()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;
			
			CodeBean bean = new CodeBean();

			String sqlSelectByEntry_no = "SELECT  branch,bill_no,patient_regno,app_entryno,category_code,item_code,referral_code,professional_code from code WHERE branch = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEntry_no );

				preparedStatement.setString(1,"RSH"); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					bean.setBranch(resultSet.getString("branch"));

					bean.setBill_no( resultSet.getString("bill_no"));

					bean.setPatient_regno(resultSet.getString("patient_regno"));
					
					bean.setApp_entryno(resultSet.getString("app_entryno"));

					bean.setCategory_code(resultSet.getString("category_code"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setReferral_code(resultSet.getString("referral_code"));
					
					bean.setProfessional_code(resultSet.getString("professional_code"));
				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return bean ;

		}
	 
		public void updateBillNoandRegNo()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  bill_no = ?, patient_regno = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setString(1,UUIDGenerator.bill_no);
				
				preparedStatement.setString(2,UUIDGenerator.patient_regno);
				
				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}
	 
		
		public void updateEntry_No()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  app_entryno = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setString(1,UUIDGenerator.app_entryno);

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}
		
		
		
		
		public void updateCategorycode()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  category_code = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				

				preparedStatement.setString(1,UUIDGenerator.category_code);
				
			

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}
		public void updateItemcode()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  item_code = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				

				preparedStatement.setString(1,UUIDGenerator.item_code);
				
			

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}
	 
		public void updateReferrercode()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  referrer_code = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				

				preparedStatement.setString(1,UUIDGenerator.referrer_code);
				
			

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}
	 
		public void updateBillNo()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  bill_no = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				

				preparedStatement.setString(1,UUIDGenerator.bill_no);
				
			

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		
		public void updateRegNo()throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE code SET  patient_regno = ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				

				preparedStatement.setString(1,UUIDGenerator.patient_regno);
				
			

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}


	 
 }
