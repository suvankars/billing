	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Employee_masterDAO {

		public Employee_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findByemp_no(String emp_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByEmp_no = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE emp_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByEmp_no );

				preparedStatement.setString(1,emp_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByUsername = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByUsername );

				preparedStatement.setString(1,username); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBypassword(String password )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPassword = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE password = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPassword );

				preparedStatement.setString(1,password); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findByfirstname(String firstname )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByFirstname = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE firstname = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByFirstname );

				preparedStatement.setString(1,firstname); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findByusertype(String usertype )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByUsertype = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE usertype = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByUsertype );

				preparedStatement.setString(1,usertype); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBymiddlename(String middlename )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByMiddlename = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE middlename = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByMiddlename );

				preparedStatement.setString(1,middlename); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBylastname(String lastname )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByLastname = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE lastname = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByLastname );

				preparedStatement.setString(1,lastname); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBymobile(String mobile )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByMobile = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE mobile = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByMobile );

				preparedStatement.setString(1,mobile); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByStatus = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByStatus );

				preparedStatement.setString(1,status); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Employee_masterBean[] findBypanicmode(String panicmode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Employee_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPanicmode = "SELECT  id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode FROM employee_master WHERE panicmode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPanicmode );

				preparedStatement.setString(1,panicmode); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Employee_masterBean bean = new Employee_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setEmp_no( resultSet.getString("emp_no"));

					bean.setUsername( resultSet.getString("username"));

					bean.setPassword( resultSet.getString("password"));

					bean.setFirstname( resultSet.getString("firstname"));

					bean.setUsertype( resultSet.getString("usertype"));

					bean.setMiddlename( resultSet.getString("middlename"));

					bean.setLastname( resultSet.getString("lastname"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPanicmode( resultSet.getString("panicmode"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Employee_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Employee_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Employee_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO employee_master( id, emp_no, username, password, firstname, usertype, middlename, lastname, mobile, status, panicmode ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getEmp_no());

				preparedStatement.setString(3, bean.getUsername());

				preparedStatement.setString(4, bean.getPassword());

				preparedStatement.setString(5, bean.getFirstname());

				preparedStatement.setString(6, bean.getUsertype());

				preparedStatement.setString(7, bean.getMiddlename());

				preparedStatement.setString(8, bean.getLastname());

				preparedStatement.setString(9, bean.getMobile());

				preparedStatement.setString(10, bean.getStatus());

				preparedStatement.setString(11, bean.getPanicmode());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Employee_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE employee_master SET  id = ?, emp_no = ?, username = ?, password = ?, firstname = ?, usertype = ?, middlename = ?, lastname = ?, mobile = ?, status = ?, panicmode = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getEmp_no());

				preparedStatement.setString(3, bean.getUsername());

				preparedStatement.setString(4, bean.getPassword());

				preparedStatement.setString(5, bean.getFirstname());

				preparedStatement.setString(6, bean.getUsertype());

				preparedStatement.setString(7, bean.getMiddlename());

				preparedStatement.setString(8, bean.getLastname());

				preparedStatement.setString(9, bean.getMobile());

				preparedStatement.setString(10, bean.getStatus());

				preparedStatement.setString(11, bean.getPanicmode());

				preparedStatement.setInt(12, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM employee_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM employee_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByemp_no(String emp_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByEmp_no = "DELETE FROM employee_master WHERE emp_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByEmp_no );

				preparedStatement.setString(1,emp_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByUsername = "DELETE FROM employee_master WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByUsername );

				preparedStatement.setString(1,username); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypassword(String password )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPassword = "DELETE FROM employee_master WHERE password = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPassword );

				preparedStatement.setString(1,password); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByfirstname(String firstname )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByFirstname = "DELETE FROM employee_master WHERE firstname = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByFirstname );

				preparedStatement.setString(1,firstname); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByusertype(String usertype )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByUsertype = "DELETE FROM employee_master WHERE usertype = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByUsertype );

				preparedStatement.setString(1,usertype); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBymiddlename(String middlename )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByMiddlename = "DELETE FROM employee_master WHERE middlename = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByMiddlename );

				preparedStatement.setString(1,middlename); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBylastname(String lastname )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByLastname = "DELETE FROM employee_master WHERE lastname = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByLastname );

				preparedStatement.setString(1,lastname); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBymobile(String mobile )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByMobile = "DELETE FROM employee_master WHERE mobile = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByMobile );

				preparedStatement.setString(1,mobile); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByStatus = "DELETE FROM employee_master WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByStatus );

				preparedStatement.setString(1,status); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypanicmode(String panicmode )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPanicmode = "DELETE FROM employee_master WHERE panicmode = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPanicmode );

				preparedStatement.setString(1,panicmode); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

