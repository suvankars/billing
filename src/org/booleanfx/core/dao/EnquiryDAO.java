	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class EnquiryDAO {

		public EnquiryBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBycaller_name(String caller_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCaller_name = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE caller_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCaller_name );

				preparedStatement.setString(1,caller_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBycalling_date(Date calling_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCalling_date = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE calling_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCalling_date );

				preparedStatement.setDate(1,calling_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBycaller_phone(String caller_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCaller_phone = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE caller_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCaller_phone );

				preparedStatement.setString(1,caller_phone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBycall_priority(String call_priority )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCall_priority = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE call_priority = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCall_priority );

				preparedStatement.setString(1,call_priority); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_name = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_name );

				preparedStatement.setString(1,patient_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_age = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_sex = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBypatient_phone(String patient_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_phone = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE patient_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_phone );

				preparedStatement.setString(1,patient_phone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByappointment_with(String appointment_with )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAppointment_with = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE appointment_with = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAppointment_with );

				preparedStatement.setString(1,appointment_with); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByappointment_date(Date appointment_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAppointment_date = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE appointment_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAppointment_date );

				preparedStatement.setDate(1,appointment_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByappointment_time(Date appointment_time )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAppointment_time = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE appointment_time = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAppointment_time );

				preparedStatement.setDate(1,appointment_time); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferred_by = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferred_by );

				preparedStatement.setString(1,referred_by); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByStatus = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByStatus );

				preparedStatement.setString(1,status); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBydescription(String description )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByDescription = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE description = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByDescription );

				preparedStatement.setString(1,description); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findBypatient_regno(String patient_regno )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPatient_regno = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE patient_regno = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPatient_regno );

				preparedStatement.setString(1,patient_regno); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public EnquiryBean[] findByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			EnquiryBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByUsername = "SELECT  id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username FROM enquiry WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByUsername );

				preparedStatement.setString(1,username); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					EnquiryBean bean = new EnquiryBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCaller_name( resultSet.getString("caller_name"));

					bean.setCalling_date( resultSet.getDate("calling_date"));

					bean.setCaller_phone( resultSet.getString("caller_phone"));

					bean.setCall_priority( resultSet.getString("call_priority"));

					bean.setPatient_name( resultSet.getString("patient_name"));

					bean.setPatient_age( resultSet.getInt("patient_age"));

					bean.setPatient_sex( resultSet.getString("patient_sex"));

					bean.setPatient_phone( resultSet.getString("patient_phone"));

					bean.setAppointment_with( resultSet.getString("appointment_with"));

					bean.setAppointment_date( resultSet.getDate("appointment_date"));

					bean.setAppointment_time( resultSet.getDate("appointment_time"));

					bean.setReferred_by( resultSet.getString("referred_by"));

					bean.setStatus( resultSet.getString("status"));

					bean.setDescription( resultSet.getString("description"));

					bean.setPatient_regno( resultSet.getString("patient_regno"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new EnquiryBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (EnquiryBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( EnquiryBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO enquiry( id, caller_name, calling_date, caller_phone, call_priority, patient_name, patient_age, patient_sex, patient_phone, appointment_with, appointment_date, appointment_time, referred_by, status, description, patient_regno, username ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCaller_name());

				preparedStatement.setDate(3, bean.getCalling_date());

				preparedStatement.setString(4, bean.getCaller_phone());

				preparedStatement.setString(5, bean.getCall_priority());

				preparedStatement.setString(6, bean.getPatient_name());

				preparedStatement.setInt(7, bean.getPatient_age());

				preparedStatement.setString(8, bean.getPatient_sex());

				preparedStatement.setString(9, bean.getPatient_phone());

				preparedStatement.setString(10, bean.getAppointment_with());

				preparedStatement.setDate(11, bean.getAppointment_date());

				preparedStatement.setDate(12, bean.getAppointment_time());

				preparedStatement.setString(13, bean.getReferred_by());

				preparedStatement.setString(14, bean.getStatus());

				preparedStatement.setString(15, bean.getDescription());

				preparedStatement.setString(16, bean.getPatient_regno());

				preparedStatement.setString(17, bean.getUsername());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( EnquiryBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE enquiry SET  id = ?, caller_name = ?, calling_date = ?, caller_phone = ?, call_priority = ?, patient_name = ?, patient_age = ?, patient_sex = ?, patient_phone = ?, appointment_with = ?, appointment_date = ?, appointment_time = ?, referred_by = ?, status = ?, description = ?, patient_regno = ?, username = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCaller_name());

				preparedStatement.setDate(3, bean.getCalling_date());

				preparedStatement.setString(4, bean.getCaller_phone());

				preparedStatement.setString(5, bean.getCall_priority());

				preparedStatement.setString(6, bean.getPatient_name());

				preparedStatement.setInt(7, bean.getPatient_age());

				preparedStatement.setString(8, bean.getPatient_sex());

				preparedStatement.setString(9, bean.getPatient_phone());

				preparedStatement.setString(10, bean.getAppointment_with());

				preparedStatement.setDate(11, bean.getAppointment_date());

				preparedStatement.setDate(12, bean.getAppointment_time());

				preparedStatement.setString(13, bean.getReferred_by());

				preparedStatement.setString(14, bean.getStatus());

				preparedStatement.setString(15, bean.getDescription());

				preparedStatement.setString(16, bean.getPatient_regno());

				preparedStatement.setString(17, bean.getUsername());

				preparedStatement.setInt(18, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM enquiry " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM enquiry WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycaller_name(String caller_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCaller_name = "DELETE FROM enquiry WHERE caller_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCaller_name );

				preparedStatement.setString(1,caller_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycalling_date(Date calling_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCalling_date = "DELETE FROM enquiry WHERE calling_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCalling_date );

				preparedStatement.setDate(1,calling_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycaller_phone(String caller_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCaller_phone = "DELETE FROM enquiry WHERE caller_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCaller_phone );

				preparedStatement.setString(1,caller_phone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycall_priority(String call_priority )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCall_priority = "DELETE FROM enquiry WHERE call_priority = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCall_priority );

				preparedStatement.setString(1,call_priority); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_name(String patient_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_name = "DELETE FROM enquiry WHERE patient_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_name );

				preparedStatement.setString(1,patient_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_age(int patient_age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_age = "DELETE FROM enquiry WHERE patient_age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_age );

				preparedStatement.setInt(1,patient_age); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_sex(String patient_sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_sex = "DELETE FROM enquiry WHERE patient_sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_sex );

				preparedStatement.setString(1,patient_sex); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_phone(String patient_phone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_phone = "DELETE FROM enquiry WHERE patient_phone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_phone );

				preparedStatement.setString(1,patient_phone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByappointment_with(String appointment_with )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAppointment_with = "DELETE FROM enquiry WHERE appointment_with = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAppointment_with );

				preparedStatement.setString(1,appointment_with); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByappointment_date(Date appointment_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAppointment_date = "DELETE FROM enquiry WHERE appointment_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAppointment_date );

				preparedStatement.setDate(1,appointment_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByappointment_time(Date appointment_time )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAppointment_time = "DELETE FROM enquiry WHERE appointment_time = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAppointment_time );

				preparedStatement.setDate(1,appointment_time); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferred_by(String referred_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferred_by = "DELETE FROM enquiry WHERE referred_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferred_by );

				preparedStatement.setString(1,referred_by); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByStatus = "DELETE FROM enquiry WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByStatus );

				preparedStatement.setString(1,status); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBydescription(String description )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByDescription = "DELETE FROM enquiry WHERE description = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByDescription );

				preparedStatement.setString(1,description); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypatient_regno(String patient_regno )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPatient_regno = "DELETE FROM enquiry WHERE patient_regno = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPatient_regno );

				preparedStatement.setString(1,patient_regno); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByUsername = "DELETE FROM enquiry WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByUsername );

				preparedStatement.setString(1,username); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

