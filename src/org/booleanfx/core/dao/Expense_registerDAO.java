	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Expense_registerDAO {

		public Expense_registerBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByvoucher_date(Date voucher_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByVoucher_date = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE voucher_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByVoucher_date );

				preparedStatement.setDate(1,voucher_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByvoucher_no(String voucher_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByVoucher_no = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE voucher_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByVoucher_no );

				preparedStatement.setString(1,voucher_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findBynarration(String narration )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByNarration = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE narration = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByNarration );

				preparedStatement.setString(1,narration); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByamount(double amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAmount = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAmount );

				preparedStatement.setDouble(1,amount); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByapproved_by(String approved_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByApproved_by = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE approved_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByApproved_by );

				preparedStatement.setString(1,approved_by); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByremarks(String remarks )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByRemarks = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE remarks = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByRemarks );

				preparedStatement.setString(1,remarks); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Expense_registerBean[] findByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Expense_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByUsername = "SELECT  id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username FROM expense_register WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByUsername );

				preparedStatement.setString(1,username); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Expense_registerBean bean = new Expense_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setVoucher_date( resultSet.getDate("voucher_date"));

					bean.setVoucher_no( resultSet.getString("voucher_no"));

					bean.setNarration( resultSet.getString("narration"));

					bean.setAmount( resultSet.getDouble("amount"));

					bean.setApproved_by( resultSet.getString("approved_by"));

					bean.setRemarks( resultSet.getString("remarks"));

					bean.setUsername( resultSet.getString("username"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Expense_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Expense_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Expense_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO expense_register( id, voucher_date, voucher_no, narration, amount, approved_by, remarks, username ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setDate(2, bean.getVoucher_date());

				preparedStatement.setString(3, bean.getVoucher_no());

				preparedStatement.setString(4, bean.getNarration());

				preparedStatement.setDouble(5, bean.getAmount());

				preparedStatement.setString(6, bean.getApproved_by());

				preparedStatement.setString(7, bean.getRemarks());

				preparedStatement.setString(8, bean.getUsername());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Expense_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE expense_register SET  id = ?, voucher_date = ?, voucher_no = ?, narration = ?, amount = ?, approved_by = ?, remarks = ?, username = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setDate(2, bean.getVoucher_date());

				preparedStatement.setString(3, bean.getVoucher_no());

				preparedStatement.setString(4, bean.getNarration());

				preparedStatement.setDouble(5, bean.getAmount());

				preparedStatement.setString(6, bean.getApproved_by());

				preparedStatement.setString(7, bean.getRemarks());

				preparedStatement.setString(8, bean.getUsername());

				preparedStatement.setInt(9, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM expense_register " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM expense_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByvoucher_date(Date voucher_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByVoucher_date = "DELETE FROM expense_register WHERE voucher_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByVoucher_date );

				preparedStatement.setDate(1,voucher_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByvoucher_no(String voucher_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByVoucher_no = "DELETE FROM expense_register WHERE voucher_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByVoucher_no );

				preparedStatement.setString(1,voucher_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBynarration(String narration )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByNarration = "DELETE FROM expense_register WHERE narration = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByNarration );

				preparedStatement.setString(1,narration); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByamount(double amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAmount = "DELETE FROM expense_register WHERE amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAmount );

				preparedStatement.setDouble(1,amount); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByapproved_by(String approved_by )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByApproved_by = "DELETE FROM expense_register WHERE approved_by = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByApproved_by );

				preparedStatement.setString(1,approved_by); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByremarks(String remarks )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByRemarks = "DELETE FROM expense_register WHERE remarks = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByRemarks );

				preparedStatement.setString(1,remarks); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByusername(String username )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByUsername = "DELETE FROM expense_register WHERE username = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByUsername );

				preparedStatement.setString(1,username); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

