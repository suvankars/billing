	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Item_masterDAO {

		public Item_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findBycategory_name(String category_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_name = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE category_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_name );

				preparedStatement.setString(1,category_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_code_prefix(String item_code_prefix )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_code_prefix = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_code_prefix = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_code_prefix );

				preparedStatement.setString(1,item_code_prefix); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_code(String item_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_code = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_code );

				preparedStatement.setString(1,item_code); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_name(String item_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_name = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_name );

				preparedStatement.setString(1,item_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_rate(double item_rate )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_rate = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_rate = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_rate );

				preparedStatement.setDouble(1,item_rate); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_tax_type(String item_tax_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_tax_type = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_tax_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_tax_type );

				preparedStatement.setString(1,item_tax_type); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Item_masterBean[] findByitem_tax_percent(double item_tax_percent )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Item_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_tax_percent = "SELECT  id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent FROM item_master WHERE item_tax_percent = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_tax_percent );

				preparedStatement.setDouble(1,item_tax_percent); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Item_masterBean bean = new Item_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setCategory_name( resultSet.getString("category_name"));

					bean.setItem_code_prefix( resultSet.getString("item_code_prefix"));

					bean.setItem_code( resultSet.getString("item_code"));

					bean.setItem_name( resultSet.getString("item_name"));

					bean.setItem_rate( resultSet.getDouble("item_rate"));

					bean.setItem_tax_type( resultSet.getString("item_tax_type"));

					bean.setItem_tax_percent( resultSet.getDouble("item_tax_percent"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Item_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Item_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Item_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO item_master( id, category_name, item_code_prefix, item_code, item_name, item_rate, item_tax_type, item_tax_percent ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCategory_name());

				preparedStatement.setString(3, bean.getItem_code_prefix());

				preparedStatement.setString(4, bean.getItem_code());

				preparedStatement.setString(5, bean.getItem_name());

				preparedStatement.setDouble(6, bean.getItem_rate());

				preparedStatement.setString(7, bean.getItem_tax_type());

				preparedStatement.setDouble(8, bean.getItem_tax_percent());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Item_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE item_master SET  id = ?, category_name = ?, item_code_prefix = ?, item_code = ?, item_name = ?, item_rate = ?, item_tax_type = ?, item_tax_percent = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getCategory_name());

				preparedStatement.setString(3, bean.getItem_code_prefix());

				preparedStatement.setString(4, bean.getItem_code());

				preparedStatement.setString(5, bean.getItem_name());

				preparedStatement.setDouble(6, bean.getItem_rate());

				preparedStatement.setString(7, bean.getItem_tax_type());

				preparedStatement.setDouble(8, bean.getItem_tax_percent());

				preparedStatement.setInt(9, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM item_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM item_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_name(String category_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_name = "DELETE FROM item_master WHERE category_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_name );

				preparedStatement.setString(1,category_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_code_prefix(String item_code_prefix )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_code_prefix = "DELETE FROM item_master WHERE item_code_prefix = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_code_prefix );

				preparedStatement.setString(1,item_code_prefix); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_code(String item_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_code = "DELETE FROM item_master WHERE item_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_code );

				preparedStatement.setString(1,item_code); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_name(String item_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_name = "DELETE FROM item_master WHERE item_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_name );

				preparedStatement.setString(1,item_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_rate(double item_rate )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_rate = "DELETE FROM item_master WHERE item_rate = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_rate );

				preparedStatement.setDouble(1,item_rate); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_tax_type(String item_tax_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_tax_type = "DELETE FROM item_master WHERE item_tax_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_tax_type );

				preparedStatement.setString(1,item_tax_type); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_tax_percent(double item_tax_percent )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_tax_percent = "DELETE FROM item_master WHERE item_tax_percent = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_tax_percent );

				preparedStatement.setDouble(1,item_tax_percent); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

