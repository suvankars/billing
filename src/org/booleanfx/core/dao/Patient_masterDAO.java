	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Patient_masterDAO {

		public Patient_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findByreg_no(String reg_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReg_no = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE reg_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReg_no );

				preparedStatement.setString(1,reg_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findByname(String name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByName = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByName );

				preparedStatement.setString(1,name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findByage(String age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAge = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAge );

				preparedStatement.setString(1,age); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBysex(String sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectBySex = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectBySex );

				preparedStatement.setString(1,sex); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBytelephone(String telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTelephone = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTelephone );

				preparedStatement.setString(1,telephone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBymobile(String mobile )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByMobile = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE mobile = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByMobile );

				preparedStatement.setString(1,mobile); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBycategory(String category )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE category = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory );

				preparedStatement.setString(1,category); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByStatus = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByStatus );

				preparedStatement.setString(1,status); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findBypriority(String priority )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPriority = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE priority = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPriority );

				preparedStatement.setString(1,priority); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Patient_masterBean[] findByreg_charges(double reg_charges )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Patient_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReg_charges = "SELECT  id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges FROM patient_master WHERE reg_charges = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReg_charges );

				preparedStatement.setDouble(1,reg_charges); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Patient_masterBean bean = new Patient_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReg_no( resultSet.getString("reg_no"));

					bean.setName( resultSet.getString("name"));

					bean.setAge( resultSet.getString("age"));

					bean.setSex( resultSet.getString("sex"));

					bean.setTelephone( resultSet.getString("telephone"));

					bean.setMobile( resultSet.getString("mobile"));

					bean.setCategory( resultSet.getString("category"));

					bean.setStatus( resultSet.getString("status"));

					bean.setPriority( resultSet.getString("priority"));

					bean.setReg_charges( resultSet.getDouble("reg_charges"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Patient_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Patient_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Patient_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO patient_master( id, reg_no, name, age, sex, telephone, mobile, category, status, priority, reg_charges ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getReg_no());

				preparedStatement.setString(3, bean.getName());

				preparedStatement.setString(4, bean.getAge());

				preparedStatement.setString(5, bean.getSex());

				preparedStatement.setString(6, bean.getTelephone());

				preparedStatement.setString(7, bean.getMobile());

				preparedStatement.setString(8, bean.getCategory());

				preparedStatement.setString(9, bean.getStatus());

				preparedStatement.setString(10, bean.getPriority());

				preparedStatement.setDouble(11, bean.getReg_charges());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Patient_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE patient_master SET  id = ?, reg_no = ?, name = ?, age = ?, sex = ?, telephone = ?, mobile = ?, category = ?, status = ?, priority = ?, reg_charges = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getReg_no());

				preparedStatement.setString(3, bean.getName());

				preparedStatement.setString(4, bean.getAge());

				preparedStatement.setString(5, bean.getSex());

				preparedStatement.setString(6, bean.getTelephone());

				preparedStatement.setString(7, bean.getMobile());

				preparedStatement.setString(8, bean.getCategory());

				preparedStatement.setString(9, bean.getStatus());

				preparedStatement.setString(10, bean.getPriority());

				preparedStatement.setDouble(11, bean.getReg_charges());

				preparedStatement.setInt(12, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM patient_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM patient_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreg_no(String reg_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReg_no = "DELETE FROM patient_master WHERE reg_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReg_no );

				preparedStatement.setString(1,reg_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByname(String name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByName = "DELETE FROM patient_master WHERE name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByName );

				preparedStatement.setString(1,name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByage(String age )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAge = "DELETE FROM patient_master WHERE age = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAge );

				preparedStatement.setString(1,age); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBysex(String sex )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteBySex = "DELETE FROM patient_master WHERE sex = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteBySex );

				preparedStatement.setString(1,sex); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytelephone(String telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTelephone = "DELETE FROM patient_master WHERE telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTelephone );

				preparedStatement.setString(1,telephone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBymobile(String mobile )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByMobile = "DELETE FROM patient_master WHERE mobile = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByMobile );

				preparedStatement.setString(1,mobile); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory(String category )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory = "DELETE FROM patient_master WHERE category = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory );

				preparedStatement.setString(1,category); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBystatus(String status )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByStatus = "DELETE FROM patient_master WHERE status = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByStatus );

				preparedStatement.setString(1,status); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypriority(String priority )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPriority = "DELETE FROM patient_master WHERE priority = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPriority );

				preparedStatement.setString(1,priority); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreg_charges(double reg_charges )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReg_charges = "DELETE FROM patient_master WHERE reg_charges = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReg_charges );

				preparedStatement.setDouble(1,reg_charges); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

