	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import org.booleanfx.core.bean.* ;
	
	import java.sql.Date;

	import org.booleanfx.core.database.* ;

	public class Payment_registerDAO {

		public Payment_registerBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBypayment_register_type(String payment_register_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPayment_register_type = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE payment_register_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPayment_register_type );

				preparedStatement.setString(1,payment_register_type); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBypayment_register_name(int payment_register_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPayment_register_name = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE payment_register_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPayment_register_name );

				preparedStatement.setInt(1,payment_register_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBypayment_register_cell_no(int payment_register_cell_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPayment_register_cell_no = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE payment_register_cell_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPayment_register_cell_no );

				preparedStatement.setInt(1,payment_register_cell_no); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBypayment_register_address(int payment_register_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPayment_register_address = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE payment_register_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPayment_register_address );

				preparedStatement.setInt(1,payment_register_address); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBycategory_name_id(int category_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_name_id = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE category_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_name_id );

				preparedStatement.setInt(1,category_name_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByitem_name_id(int item_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_name_id = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE item_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_name_id );

				preparedStatement.setInt(1,item_name_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByfrom_date(Date from_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByFrom_date = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE from_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByFrom_date );

				preparedStatement.setDate(1,from_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByto_date(Date to_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTo_date = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE to_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTo_date );

				preparedStatement.setDate(1,to_date); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByvolume_payment_register(String volume_payment_register )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByVolume_payment_register = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE volume_payment_register = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByVolume_payment_register );

				preparedStatement.setString(1,volume_payment_register); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBypercent_payment_register(double percent_payment_register )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByPercent_payment_register = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE percent_payment_register = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByPercent_payment_register );

				preparedStatement.setDouble(1,percent_payment_register); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBycommission_amount(double commission_amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCommission_amount = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE commission_amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCommission_amount );

				preparedStatement.setDouble(1,commission_amount); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findBydue_amount(double due_amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByDue_amount = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE due_amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByDue_amount );

				preparedStatement.setDouble(1,due_amount); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Payment_registerBean[] findByamount_paid(double amount_paid )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Payment_registerBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAmount_paid = "SELECT  id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid FROM payment_register WHERE amount_paid = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAmount_paid );

				preparedStatement.setDouble(1,amount_paid); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Payment_registerBean bean = new Payment_registerBean();

					bean.setId( resultSet.getInt("id"));

					bean.setPayment_register_type( resultSet.getString("payment_register_type"));

					bean.setPayment_register_name( resultSet.getInt("payment_register_name"));

					bean.setPayment_register_cell_no( resultSet.getInt("payment_register_cell_no"));

					bean.setPayment_register_address( resultSet.getString("payment_register_address"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setFrom_date( resultSet.getDate("from_date"));

					bean.setTo_date( resultSet.getDate("to_date"));

					bean.setVolume_payment_register( resultSet.getString("volume_payment_register"));

					bean.setPercent_payment_register( resultSet.getDouble("percent_payment_register"));

					bean.setCommission_amount( resultSet.getDouble("commission_amount"));

					bean.setDue_amount( resultSet.getDouble("due_amount"));

					bean.setAmount_paid( resultSet.getDouble("amount_paid"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Payment_registerBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Payment_registerBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Payment_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO payment_register( id, payment_register_type, payment_register_name, payment_register_cell_no, payment_register_address, category_name_id, item_name_id, from_date, to_date, volume_payment_register, percent_payment_register, commission_amount, due_amount, amount_paid ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getPayment_register_type());

				preparedStatement.setInt(3, bean.getPayment_register_name());

				preparedStatement.setInt(4, bean.getPayment_register_cell_no());

				preparedStatement.setString(5, bean.getPayment_register_address());

				preparedStatement.setInt(6, bean.getCategory_name_id());

				preparedStatement.setInt(7, bean.getItem_name_id());

				preparedStatement.setDate(8, bean.getFrom_date());

				preparedStatement.setDate(9, bean.getTo_date());

				preparedStatement.setString(10, bean.getVolume_payment_register());

				preparedStatement.setDouble(11, bean.getPercent_payment_register());

				preparedStatement.setDouble(12, bean.getCommission_amount());

				preparedStatement.setDouble(13, bean.getDue_amount());

				preparedStatement.setDouble(14, bean.getAmount_paid());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Payment_registerBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE payment_register SET  id = ?, payment_register_type = ?, payment_register_name = ?, payment_register_cell_no = ?, payment_register_address = ?, category_name_id = ?, item_name_id = ?, from_date = ?, to_date = ?, volume_payment_register = ?, percent_payment_register = ?, commission_amount = ?, due_amount = ?, amount_paid = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getPayment_register_type());

				preparedStatement.setInt(3, bean.getPayment_register_name());

				preparedStatement.setInt(4, bean.getPayment_register_cell_no());

				preparedStatement.setString(5, bean.getPayment_register_address());

				preparedStatement.setInt(6, bean.getCategory_name_id());

				preparedStatement.setInt(7, bean.getItem_name_id());

				preparedStatement.setDate(8, bean.getFrom_date());

				preparedStatement.setDate(9, bean.getTo_date());

				preparedStatement.setString(10, bean.getVolume_payment_register());

				preparedStatement.setDouble(11, bean.getPercent_payment_register());

				preparedStatement.setDouble(12, bean.getCommission_amount());

				preparedStatement.setDouble(13, bean.getDue_amount());

				preparedStatement.setDouble(14, bean.getAmount_paid());

				preparedStatement.setInt(15, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM payment_register " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM payment_register WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypayment_register_type(String payment_register_type )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPayment_register_type = "DELETE FROM payment_register WHERE payment_register_type = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPayment_register_type );

				preparedStatement.setString(1,payment_register_type); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypayment_register_name(int payment_register_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPayment_register_name = "DELETE FROM payment_register WHERE payment_register_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPayment_register_name );

				preparedStatement.setInt(1,payment_register_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypayment_register_cell_no(int payment_register_cell_no )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPayment_register_cell_no = "DELETE FROM payment_register WHERE payment_register_cell_no = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPayment_register_cell_no );

				preparedStatement.setInt(1,payment_register_cell_no); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypayment_register_address(int payment_register_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPayment_register_address = "DELETE FROM payment_register WHERE payment_register_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPayment_register_address );

				preparedStatement.setInt(1,payment_register_address); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_name_id(int category_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_name_id = "DELETE FROM payment_register WHERE category_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_name_id );

				preparedStatement.setInt(1,category_name_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_name_id(int item_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_name_id = "DELETE FROM payment_register WHERE item_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_name_id );

				preparedStatement.setInt(1,item_name_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByfrom_date(Date from_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByFrom_date = "DELETE FROM payment_register WHERE from_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByFrom_date );

				preparedStatement.setDate(1,from_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByto_date(Date to_date )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTo_date = "DELETE FROM payment_register WHERE to_date = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTo_date );

				preparedStatement.setDate(1,to_date); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByvolume_payment_register(String volume_payment_register )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByVolume_payment_register = "DELETE FROM payment_register WHERE volume_payment_register = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByVolume_payment_register );

				preparedStatement.setString(1,volume_payment_register); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBypercent_payment_register(double percent_payment_register )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByPercent_payment_register = "DELETE FROM payment_register WHERE percent_payment_register = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByPercent_payment_register );

				preparedStatement.setDouble(1,percent_payment_register); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycommission_amount(double commission_amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCommission_amount = "DELETE FROM payment_register WHERE commission_amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCommission_amount );

				preparedStatement.setDouble(1,commission_amount); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBydue_amount(double due_amount )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByDue_amount = "DELETE FROM payment_register WHERE due_amount = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByDue_amount );

				preparedStatement.setDouble(1,due_amount); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByamount_paid(double amount_paid )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByAmount_paid = "DELETE FROM payment_register WHERE amount_paid = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByAmount_paid );

				preparedStatement.setDouble(1,amount_paid); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
				
                      preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}
		
		public double findVolume_referred(String referred_by,String item_name,Date from,Date to )throws Exception{

			double netAmount = 0 ;
			
			double gross = 0 ;
			
			double tax = 0 ;
			
			
			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			String sqlSelectByAll = "SELECT ad.line_gross_amt,ad.line_item_tax_amt FROM acc_patient_bill_details ad,acc_patient_bill_header a where a.id = ad.bill_header_id and a.referred_by= ? and ad.line_item_name= ? and a.bill_date between ? and ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );
				
				preparedStatement.setString(1,referred_by);
				
				preparedStatement.setString(2,item_name);
				
				preparedStatement.setDate(3,from);
				
				preparedStatement.setDate(4,to);
				
				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					gross = gross +  resultSet.getDouble(1);
					
					tax = tax + resultSet.getDouble(2);

					
				}


			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}
           
			netAmount = gross - tax ;
			
			
			return netAmount ;

		}
		
		public double findVolume_prof(String professional_name,String item_name,Date from,Date to )throws Exception{

           double netAmount = 0 ;
			
			double gross = 0 ;
			
			double tax = 0 ;
			
			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			String sqlSelectByAll = "SELECT ad.line_gross_amt,ad.line_item_tax_amt FROM acc_patient_bill_details as ad,acc_patient_bill_header as a where a.id = ad.bill_header_id and a.professional_by= ? and ad.line_item_name= ? and a.bill_date between ? and ?" ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				preparedStatement.setString(1,professional_name);
				
				preparedStatement.setString(2,item_name);
				
                preparedStatement.setDate(3,from);
				
				preparedStatement.setDate(4,to);
				
				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

                	gross = gross +  resultSet.getDouble(1);
					
					tax = tax + resultSet.getDouble(2);

                    
					
				}


			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

            netAmount = gross - tax ;
			System.out.println(netAmount + "   c");
			
			return netAmount ;

		}

	}

