	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Professional_masterDAO {

		public Professional_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_code(String professional_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_code = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_code );

				preparedStatement.setString(1,professional_code); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_name(String professional_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_name = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_name );

				preparedStatement.setString(1,professional_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_cell1(int professional_cell1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_cell1 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_cell1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_cell1 );

				preparedStatement.setInt(1,professional_cell1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_cell2(int professional_cell2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_cell2 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_cell2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_cell2 );

				preparedStatement.setInt(1,professional_cell2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_address(String professional_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_address = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_address );

				preparedStatement.setString(1,professional_address); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByprofessional_address_telephone(int professional_address_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByProfessional_address_telephone = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE professional_address_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByProfessional_address_telephone );

				preparedStatement.setInt(1,professional_address_telephone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBycategory_name_id(int category_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_name_id = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE category_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_name_id );

				preparedStatement.setInt(1,category_name_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findByitem_name_id(int item_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_name_id = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE item_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_name_id );

				preparedStatement.setInt(1,item_name_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume1(String target_volume1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume1 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume1 );

				preparedStatement.setString(1,target_volume1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume_commission_percent1(String target_volume_commission_percent1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume_commission_percent1 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume_commission_percent1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume_commission_percent1 );

				preparedStatement.setString(1,target_volume_commission_percent1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume2(String target_volume2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume2 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume2 );

				preparedStatement.setString(1,target_volume2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume_commission2(String target_volume_commission2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume_commission2 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume_commission2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume_commission2 );

				preparedStatement.setString(1,target_volume_commission2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume3(String target_volume3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume3 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume3 );

				preparedStatement.setString(1,target_volume3); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Professional_masterBean[] findBytarget_volume_commission3(String target_volume_commission3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Professional_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume_commission3 = "SELECT  id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 FROM professional_master WHERE target_volume_commission3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume_commission3 );

				preparedStatement.setString(1,target_volume_commission3); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Professional_masterBean bean = new Professional_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setProfessional_code( resultSet.getString("professional_code"));

					bean.setProfessional_name( resultSet.getString("professional_name"));

					bean.setProfessional_cell1( resultSet.getString("professional_cell1"));

					bean.setProfessional_cell2( resultSet.getString("professional_cell2"));

					bean.setProfessional_address( resultSet.getString("professional_address"));

					bean.setProfessional_address_telephone( resultSet.getString("professional_address_telephone"));

					bean.setCategory_name_id( resultSet.getInt("category_name_id"));

					bean.setItem_name_id( resultSet.getInt("item_name_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_volume_commission_percent1( resultSet.getString("target_volume_commission_percent1"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_volume_commission2( resultSet.getString("target_volume_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_volume_commission3( resultSet.getString("target_volume_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Professional_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Professional_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Professional_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO professional_master( id, professional_code, professional_name, professional_cell1, professional_cell2, professional_address, professional_address_telephone, category_name_id, item_name_id, target_volume1, target_volume_commission_percent1, target_volume2, target_volume_commission2, target_volume3, target_volume_commission3 ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getProfessional_code());

				preparedStatement.setString(3, bean.getProfessional_name());

				preparedStatement.setString(4, bean.getProfessional_cell1());

				preparedStatement.setString(5, bean.getProfessional_cell2());

				preparedStatement.setString(6, bean.getProfessional_address());

				preparedStatement.setString(7, bean.getProfessional_address_telephone());

				preparedStatement.setInt(8, bean.getCategory_name_id());

				preparedStatement.setInt(9, bean.getItem_name_id());

				preparedStatement.setString(10, bean.getTarget_volume1());

				preparedStatement.setString(11, bean.getTarget_volume_commission_percent1());

				preparedStatement.setString(12, bean.getTarget_volume2());

				preparedStatement.setString(13, bean.getTarget_volume_commission2());

				preparedStatement.setString(14, bean.getTarget_volume3());

				preparedStatement.setString(15, bean.getTarget_volume_commission3());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Professional_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE professional_master SET  id = ?, professional_code = ?, professional_name = ?, professional_cell1 = ?, professional_cell2 = ?, professional_address = ?, professional_address_telephone = ?, category_name_id = ?, item_name_id = ?, target_volume1 = ?, target_volume_commission_percent1 = ?, target_volume2 = ?, target_volume_commission2 = ?, target_volume3 = ?, target_volume_commission3 = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getProfessional_code());

				preparedStatement.setString(3, bean.getProfessional_name());

				preparedStatement.setString(4, bean.getProfessional_cell1());

				preparedStatement.setString(5, bean.getProfessional_cell2());

				preparedStatement.setString(6, bean.getProfessional_address());

				preparedStatement.setString(7, bean.getProfessional_address_telephone());

				preparedStatement.setInt(8, bean.getCategory_name_id());

				preparedStatement.setInt(9, bean.getItem_name_id());

				preparedStatement.setString(10, bean.getTarget_volume1());

				preparedStatement.setString(11, bean.getTarget_volume_commission_percent1());

				preparedStatement.setString(12, bean.getTarget_volume2());

				preparedStatement.setString(13, bean.getTarget_volume_commission2());

				preparedStatement.setString(14, bean.getTarget_volume3());

				preparedStatement.setString(15, bean.getTarget_volume_commission3());

				preparedStatement.setInt(16, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM professional_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM professional_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_code(String professional_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_code = "DELETE FROM professional_master WHERE professional_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_code );

				preparedStatement.setString(1,professional_code); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_name(String professional_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_name = "DELETE FROM professional_master WHERE professional_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_name );

				preparedStatement.setString(1,professional_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_cell1(int professional_cell1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_cell1 = "DELETE FROM professional_master WHERE professional_cell1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_cell1 );

				preparedStatement.setInt(1,professional_cell1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_cell2(int professional_cell2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_cell2 = "DELETE FROM professional_master WHERE professional_cell2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_cell2 );

				preparedStatement.setInt(1,professional_cell2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_address(String professional_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_address = "DELETE FROM professional_master WHERE professional_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_address );

				preparedStatement.setString(1,professional_address); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByprofessional_address_telephone(int professional_address_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByProfessional_address_telephone = "DELETE FROM professional_master WHERE professional_address_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByProfessional_address_telephone );

				preparedStatement.setInt(1,professional_address_telephone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_name_id(int category_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_name_id = "DELETE FROM professional_master WHERE category_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_name_id );

				preparedStatement.setInt(1,category_name_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_name_id(int item_name_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_name_id = "DELETE FROM professional_master WHERE item_name_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_name_id );

				preparedStatement.setInt(1,item_name_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume1(String target_volume1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume1 = "DELETE FROM professional_master WHERE target_volume1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume1 );

				preparedStatement.setString(1,target_volume1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume_commission_percent1(String target_volume_commission_percent1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume_commission_percent1 = "DELETE FROM professional_master WHERE target_volume_commission_percent1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume_commission_percent1 );

				preparedStatement.setString(1,target_volume_commission_percent1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume2(String target_volume2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume2 = "DELETE FROM professional_master WHERE target_volume2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume2 );

				preparedStatement.setString(1,target_volume2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume_commission2(String target_volume_commission2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume_commission2 = "DELETE FROM professional_master WHERE target_volume_commission2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume_commission2 );

				preparedStatement.setString(1,target_volume_commission2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume3(String target_volume3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume3 = "DELETE FROM professional_master WHERE target_volume3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume3 );

				preparedStatement.setString(1,target_volume3); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume_commission3(String target_volume_commission3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume_commission3 = "DELETE FROM professional_master WHERE target_volume_commission3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume_commission3 );

				preparedStatement.setString(1,target_volume_commission3); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

