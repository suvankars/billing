	package org.booleanfx.core.dao ;

	import java.util.ArrayList ;

	import java.sql.Date ;

	import java.sql.Timestamp ;

	import java.sql.Connection ;

	import java.sql.PreparedStatement ;

	import java.sql.ResultSet ;

	import java.sql.SQLException ;

	import java.io.* ;

	import org.booleanfx.core.bean.* ;

	import org.booleanfx.core.database.* ;

	public class Referrer_masterDAO {

		public Referrer_masterBean[] findAll( )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByAll = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByAll );

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectById = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectById );

				preparedStatement.setInt(1,id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_code(String referrer_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_code = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_code );

				preparedStatement.setString(1,referrer_code); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_name(String referrer_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_name = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_name );

				preparedStatement.setString(1,referrer_name); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_mobile1(int referrer_mobile1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_mobile1 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_mobile1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_mobile1 );

				preparedStatement.setInt(1,referrer_mobile1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_address(String referrer_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_address = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_address );

				preparedStatement.setString(1,referrer_address); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_telephone(int referrer_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_telephone = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_telephone );

				preparedStatement.setInt(1,referrer_telephone); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBycategory_id(int category_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByCategory_id = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE category_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByCategory_id );

				preparedStatement.setInt(1,category_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByitem_id(int item_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByItem_id = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE item_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByItem_id );

				preparedStatement.setInt(1,item_id); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_volume1(String target_volume1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume1 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_volume1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume1 );

				preparedStatement.setString(1,target_volume1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_commission1(String target_commission1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_commission1 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_commission1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_commission1 );

				preparedStatement.setString(1,target_commission1); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findByreferrer_mobile2(int referrer_mobile2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByReferrer_mobile2 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE referrer_mobile2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByReferrer_mobile2 );

				preparedStatement.setInt(1,referrer_mobile2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_volume2(String target_volume2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume2 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_volume2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume2 );

				preparedStatement.setString(1,target_volume2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_commission2(String target_commission2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_commission2 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_commission2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_commission2 );

				preparedStatement.setString(1,target_commission2); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_volume3(String target_volume3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_volume3 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_volume3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_volume3 );

				preparedStatement.setString(1,target_volume3); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public Referrer_masterBean[] findBytarget_commission3(String target_commission3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			ResultSet resultSet = null ;

			Referrer_masterBean[] beans = null ;

			ArrayList beansList = new ArrayList();

			String sqlSelectByTarget_commission3 = "SELECT  id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 FROM referrer_master WHERE target_commission3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlSelectByTarget_commission3 );

				preparedStatement.setString(1,target_commission3); 

				resultSet = preparedStatement.executeQuery();

				while(resultSet.next()){

					Referrer_masterBean bean = new Referrer_masterBean();

					bean.setId( resultSet.getInt("id"));

					bean.setReferrer_code( resultSet.getString("referrer_code"));

					bean.setReferrer_name( resultSet.getString("referrer_name"));

					bean.setReferrer_mobile1( resultSet.getString("referrer_mobile1"));

					bean.setReferrer_address( resultSet.getString("referrer_address"));

					bean.setReferrer_telephone( resultSet.getString("referrer_telephone"));

					bean.setCategory_id( resultSet.getInt("category_id"));

					bean.setItem_id( resultSet.getInt("item_id"));

					bean.setTarget_volume1( resultSet.getString("target_volume1"));

					bean.setTarget_commission1( resultSet.getString("target_commission1"));

					bean.setReferrer_mobile2( resultSet.getString("referrer_mobile2"));

					bean.setTarget_volume2( resultSet.getString("target_volume2"));

					bean.setTarget_commission2( resultSet.getString("target_commission2"));

					bean.setTarget_volume3( resultSet.getString("target_volume3"));

					bean.setTarget_commission3( resultSet.getString("target_commission3"));

					beansList.add(bean);

				}

				Object[] objectArray = beansList.toArray();

				beans = new Referrer_masterBean[objectArray.length];

				for(int jindex = 0 ; jindex < objectArray.length ; jindex++){

					beans[jindex] = (Referrer_masterBean)objectArray[jindex];

				}

			}catch(Exception e){

				e.printStackTrace();

			}finally{

				try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

			return beans ;

		}

		public void insertAllCols( Referrer_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlInsertAllStmt = "INSERT INTO referrer_master( id, referrer_code, referrer_name, referrer_mobile1, referrer_address, referrer_telephone, category_id, item_id, target_volume1, target_commission1, referrer_mobile2, target_volume2, target_commission2, target_volume3, target_commission3 ) VALUES ( ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ? , ?  )" ;

			try{

				preparedStatement = conn.prepareStatement( sqlInsertAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getReferrer_code());

				preparedStatement.setString(3, bean.getReferrer_name());

				preparedStatement.setString(4, bean.getReferrer_mobile1());

				preparedStatement.setString(5, bean.getReferrer_address());

				preparedStatement.setString(6, bean.getReferrer_telephone());

				preparedStatement.setInt(7, bean.getCategory_id());

				preparedStatement.setInt(8, bean.getItem_id());

				preparedStatement.setString(9, bean.getTarget_volume1());

				preparedStatement.setString(10, bean.getTarget_commission1());

				preparedStatement.setString(11, bean.getReferrer_mobile2());

				preparedStatement.setString(12, bean.getTarget_volume2());

				preparedStatement.setString(13, bean.getTarget_commission2());

				preparedStatement.setString(14, bean.getTarget_volume3());

				preparedStatement.setString(15, bean.getTarget_commission3());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void updateAllCols( Referrer_masterBean bean )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlUpdateAllStmt = "UPDATE referrer_master SET  id = ?, referrer_code = ?, referrer_name = ?, referrer_mobile1 = ?, referrer_address = ?, referrer_telephone = ?, category_id = ?, item_id = ?, target_volume1 = ?, target_commission1 = ?, referrer_mobile2 = ?, target_volume2 = ?, target_commission2 = ?, target_volume3 = ?, target_commission3 = ? WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlUpdateAllStmt );

				preparedStatement.setInt(1, bean.getId());

				preparedStatement.setString(2, bean.getReferrer_code());

				preparedStatement.setString(3, bean.getReferrer_name());

				preparedStatement.setString(4, bean.getReferrer_mobile1());

				preparedStatement.setString(5, bean.getReferrer_address());

				preparedStatement.setString(6, bean.getReferrer_telephone());

				preparedStatement.setInt(7, bean.getCategory_id());

				preparedStatement.setInt(8, bean.getItem_id());

				preparedStatement.setString(9, bean.getTarget_volume1());

				preparedStatement.setString(10, bean.getTarget_commission1());

				preparedStatement.setString(11, bean.getReferrer_mobile2());

				preparedStatement.setString(12, bean.getTarget_volume2());

				preparedStatement.setString(13, bean.getTarget_commission2());

				preparedStatement.setString(14, bean.getTarget_volume3());

				preparedStatement.setString(15, bean.getTarget_commission3());

				preparedStatement.setInt(16, bean.getId());

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				conn.rollback();

				e.printStackTrace();

				throw new Exception(e.getMessage());

			}finally{

			try{ dbDatabaseHandler.closeConnection();preparedStatement.close();}catch(SQLException ioe){}

			}

		}

		public void deleteAll( )throws Exception{

			String sqlDeleteAll = "DELETE FROM referrer_master " ;

			//delete code here...

		}

		public void deleteByid(int id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteById = "DELETE FROM referrer_master WHERE id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteById );

				preparedStatement.setInt(1,id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_code(String referrer_code )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_code = "DELETE FROM referrer_master WHERE referrer_code = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_code );

				preparedStatement.setString(1,referrer_code); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_name(String referrer_name )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_name = "DELETE FROM referrer_master WHERE referrer_name = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_name );

				preparedStatement.setString(1,referrer_name); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_mobile1(int referrer_mobile1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_mobile1 = "DELETE FROM referrer_master WHERE referrer_mobile1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_mobile1 );

				preparedStatement.setInt(1,referrer_mobile1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_address(String referrer_address )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_address = "DELETE FROM referrer_master WHERE referrer_address = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_address );

				preparedStatement.setString(1,referrer_address); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_telephone(int referrer_telephone )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_telephone = "DELETE FROM referrer_master WHERE referrer_telephone = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_telephone );

				preparedStatement.setInt(1,referrer_telephone); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBycategory_id(int category_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByCategory_id = "DELETE FROM referrer_master WHERE category_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByCategory_id );

				preparedStatement.setInt(1,category_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByitem_id(int item_id )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByItem_id = "DELETE FROM referrer_master WHERE item_id = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByItem_id );

				preparedStatement.setInt(1,item_id); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume1(String target_volume1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume1 = "DELETE FROM referrer_master WHERE target_volume1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume1 );

				preparedStatement.setString(1,target_volume1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_commission1(String target_commission1 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_commission1 = "DELETE FROM referrer_master WHERE target_commission1 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_commission1 );

				preparedStatement.setString(1,target_commission1); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteByreferrer_mobile2(int referrer_mobile2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByReferrer_mobile2 = "DELETE FROM referrer_master WHERE referrer_mobile2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByReferrer_mobile2 );

				preparedStatement.setInt(1,referrer_mobile2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume2(String target_volume2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume2 = "DELETE FROM referrer_master WHERE target_volume2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume2 );

				preparedStatement.setString(1,target_volume2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_commission2(String target_commission2 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_commission2 = "DELETE FROM referrer_master WHERE target_commission2 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_commission2 );

				preparedStatement.setString(1,target_commission2); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_volume3(String target_volume3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_volume3 = "DELETE FROM referrer_master WHERE target_volume3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_volume3 );

				preparedStatement.setString(1,target_volume3); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

		public void deleteBytarget_commission3(String target_commission3 )throws Exception{

			DatabaseHandler dbDatabaseHandler = DatabaseHandler.getNewInstance();

			Connection conn = dbDatabaseHandler.getConnection();

			PreparedStatement preparedStatement = null ;

			String sqlDeleteByTarget_commission3 = "DELETE FROM referrer_master WHERE target_commission3 = ? " ;

			try{

				preparedStatement = conn.prepareStatement( sqlDeleteByTarget_commission3 );

				preparedStatement.setString(1,target_commission3); 

				preparedStatement.executeUpdate();

				conn.commit();

			}catch(Exception e){

				e.printStackTrace();

				conn.rollback();

				throw new Exception(e.getMessage());

			}finally{

				try{ dbDatabaseHandler.closeConnection();
preparedStatement.close();}catch(SQLException ioe){}

			}

			//no return value here

		}

	}

