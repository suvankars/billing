package org.booleanfx.core.utils;


public class AutomaticNoGenerator {

	public String getFirstBillNO(){
		
		return "RSB/11-12/1" ;
		
	}
	
	public String getFirstEntryNO(){
		
		return "RSB/1" ;
		
	}
	
	public String getFirstPatientID(){
		
		return "RSB/1" ;
		
	}
	

	public String getnextBillNO(String input){
		System.out.println(input);
		String output="";
		int index = input.lastIndexOf("/");
		int value = Integer.parseInt(input.substring(index+1,input.length()));
		value++;
		output = input.substring(0,index+1)+value;
		System.out.println(output);
		return output ;
		
	}
	
	public String getnextEntryNO(String input){
		
		String output="";
		int index = input.lastIndexOf("/");
		int value = Integer.parseInt(input.substring(index+1,input.length()));
		value++;
		output = input.substring(0,index+1)+value;
		return output ;
		
	}	
	
	public String getnextPatientID(String input){
		
		String output="";
		int index = input.lastIndexOf("/");
		int value = Integer.parseInt(input.substring(index+1,input.length()));
		value++;
		output = input.substring(0,index+1)+value;
		return output ;
		
	}	

	public static void main(String[] args) {
		
		String x = "RSB/0";
		AutomaticNoGenerator getNO = new AutomaticNoGenerator();
		
		for(int i = 0 ; i < 10 ; i ++){
			x = getNO.getnextBillNO(x);
			System.out.println(x);
		}
		
	
		
	}
}


