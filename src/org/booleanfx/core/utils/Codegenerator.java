/**
 * Purpose of the class :
 * 
 * @author 
 * 
 */
package org.booleanfx.core.utils;


public class Codegenerator {
	
	public static final int MAXSIZE = 6;

	private static Codegenerator codegenerator;
	
	private Codegenerator(){
		
	}
	
	public static Codegenerator getInstance(){
		
		if(codegenerator == null){
			
			codegenerator = new Codegenerator();
			
		}
		
		return codegenerator ;
	}
	
	public String getNewEmploytNumber(){
		
		
		  StringBuffer sb = new StringBuffer();  
		    
		    for (int x = 0; x < MAXSIZE; x++)  
		    {  
		    	int i = ((int)((Math.random()*26)+48));

		    	if(i == 58){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i1 = ((int)((Math.random()*26)+65));
		            
		    		sb.append((char)i1); 
		            }
		    	}
		    	        	
		    	if(i == 59){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i2 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i2); 
		            }
		    		       		
		    	}
		    	
		    	
		    	if(i == 60){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i3 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i3); 
		            }
		    	}
		    	
		    	
		    	if(i == 61){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i4 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i4); 
		            }
		    	}
		    	
		    	
		    	if(i == 62){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i5 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i5); 
		            }
		    	}
		    	
		    	
		    	if(i == 63){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i6 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i6); 
		            }
		    	}
		    	
		    	
		    	if(i == 64){
		    		for (int x1 = 0; x1 < 1; x1++)  
		            { 
		    		int i7 = ((int)((Math.random()*26)+65));
		    		sb.append((char)i7); 
		            }
		    	}else if((i != 58) && (i != 59) && (i != 60) && (i != 61) && (i != 62) && (i != 63) && (i != 64)){
		    		sb.append((char)i); 
		    	}
		    	
		    
		    }  
		  
		
		return sb.toString() ;
		
	}
	
	
	
	  public static void main(String[] args)
      {
    	  new Codegenerator();
    	  } 
	
}
