package org.booleanfx.core.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;

import org.booleanfx.core.bean.Acc_patient_bill_headerBean;
 
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;
 
public class MoneyReceipt {
 
    /** The resulting PDF. */
    public static final String SOURCE
        = "FirstPdf.pdf";
    /** The resulting PDF. */
    public static final String RESULT
        = "Second.pdf";
    /** The movie poster. */
   
 
  /*  public static void main(String[] args)
        throws IOException, DocumentException {
        new MoneyReceipt().createPdf(SOURCE);
       
        PdfReader reader = new PdfReader(SOURCE);
       
        
        
        Document document = new Document(new Rectangle(864,576));
      
        PdfWriter writer= PdfWriter.getInstance(document, new FileOutputStream(RESULT));
      
        document.open();
        
        PdfContentByte canvas = writer.getDirectContent();
        PdfImportedPage page;
        for (int i = 1; i <= reader.getNumberOfPages(); i++) {
            page = writer.getImportedPage(reader, i);
            canvas.addTemplate(page,0, 0);
        }
       
        document.close();
    }
 */
    
    public void createPdf(String filename,Acc_patient_bill_headerBean bean)
        throws IOException, DocumentException {
      
        Document document = new Document(new Rectangle(864,576));
        
        NumToWord numToWord = new NumToWord() ;
       
        PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(filename));
        
        document.open();
      
        DecimalFormat dformat = new DecimalFormat("0.00");

      /* document.newPage();
       
        Image img = Image.getInstance(RESOURCE);
        img.scaleAbsolute(800,500);
        img.setAlignment( Image.TEXTWRAP | Image.ALIGN_LEFT);
     
        document.add(img);*/
        document.newPage();
       
        
        PdfContentByte over = writer.getDirectContent();
        over.saveState();
      
        BaseFont bf = BaseFont.createFont();
        over.beginText();
        over.setTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
        over.setFontAndSize(bf,12);
        over.setTextMatrix(150,400);
        over.showText("No.:  "+bean.getBill_no());
        String date_content = bean.getBill_date();
		String format[] = date_content.split("-");
		String date = format[2]+"/"+format[1]+"/"+format[0];
		over.setTextMatrix(380,400);
        over.showText("Date:  "+date);
        over.setTextMatrix(550,400);
        over.showText("Pat. Regn No.:  "+bean.getPatient_reg_no());
       
     
        if(bean.getPatient_reg_charges() == 0.0){
        
        over.setTextMatrix(150,340);
        over.showText("Regn. Charge Rs.  "+dformat.format(bean.getPatient_reg_charges()));
       
        }else{
        	 over.setTextMatrix(150,340);
             over.showText("Regn. Charge Rs.  "+dformat.format(bean.getPatient_reg_charges()));
            
            
        }
        
        over.setTextMatrix(150,320);
        Double a = bean.getAmt_received() + bean.getPatient_reg_charges();
        over.showText("Received with thanks the sum of Rs.  "+dformat.format(a));
        
        String word = numToWord.numberToWords(bean.getAmt_received());
        
       
        if(word.length() > 100){
        	
        	 over.setTextMatrix(430,320);
             over.showText("(Rupees.  "+word.substring(0,100)+")");
             over.setTextMatrix(430,300);
             over.showText(word.substring(100));
                	
        }else{
        	
        	over.setTextMatrix(430,320);
            over.showText("(Rupees.  "+word);
            
        }
        
        over.setTextMatrix(150,300);
        over.showText("from<Mr/Mrs/Miss>  "+bean.getPatient_name());
        
        over.setTextMatrix(150,280);
        over.showText("of  "+bean.getPatient_address()+" "+bean.getPatient_pincode());
       
        over.setTextMatrix(425,260);
        over.showText("Contact No.  "+bean.getPatient_telephone());
       
        if(!bean.getProfessional_by().equals("")&&(bean.getReferred_by().equals(""))){
        	
        	 over.setTextMatrix(150,240);
             over.showText("Professional Name = "+bean.getProfessional_by());
        	
        }else if(!bean.getReferred_by().equals("")&&(bean.getProfessional_by().equals(""))){
        	
        	 over.setTextMatrix(150,240);
             over.showText("Referred By Name =  "+bean.getReferred_by());
        	
        }else if((!bean.getProfessional_by().equals(""))&&(!bean.getReferred_by().equals(""))){
        	
        	 over.setTextMatrix(150,240);
        	 over.showText("Professional Name =  " +bean.getProfessional_by());
        	 over.setTextMatrix(150,225);
         	over.showText("Referred Name = " +bean.getReferred_by());
        	
        }
        
      
        over.setTextMatrix(150,210);
        over.showText("By  "+bean.getBill_payment_mode());
        
        if(bean.getBill_payment_mode().equalsIgnoreCase("Cheque")){
        	 over.setTextMatrix(150,190);
             over.showText("By Cheque No.  "+bean.getCheque_no());
             
             over.setTextMatrix(360,190);
             over.showText("Dated  "+bean.getCheque_date());
             
             over.setTextMatrix(500,190);
             over.showText("of  "+bean.getBank_name() + "  "+bean.getBank_br() );
        }else if(bean.getBill_payment_mode().equalsIgnoreCase("Card")){
        	
        	over.setTextMatrix(150,190);
            over.showText("By Card Name.  "+bean.getName_of_card());
            
            over.setTextMatrix(360,190);
            over.showText("Card No.  "+bean.getCard_no());
            
            over.setTextMatrix(500,190);
            over.showText("Expiry Date  "+bean.getCard_expiry_dt());
        	
        }
        
        over.endText();
        over.restoreState();
        
        document.close();
    }
}
