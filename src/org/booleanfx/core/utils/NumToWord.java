package org.booleanfx.core.utils;
public class NumToWord {
	
	public String numberToWords(double num){
		String str = Double.toString(num);
		String[] str1 = str.split("\\.");
		String word = null;
		if(num == 0){
			word = "zero";
		}else{
			word = convertNum(Double.parseDouble(str1[0]));
			if(Double.valueOf(str1[0]) != 0){
				word = word + " and ";
			}
			if(Double.valueOf(str1[1]) != 0){
				word = word + convertNum(Double.valueOf(str1[1])) + " Paise";
			}else{
				word = word + " zero " + " Paise";
			}
		}		
		return word+" only";
	}
	
	
	public String numword(double N){
	
	int num;

	boolean skip=false;
	
	

	int [] numword=new int[20];

	String subtext="";

	String numinWords="";

	num=(int)N;
	System.out.println("after cast :"+ num);

	int i=-1;

	while (num>0){
		i++;
		numword[i]=num%10;
		num=num/10;
		System.out.println(numword[i]);
	}
	for( ;i>=0;i--){
		
		if(i%2==0 && i!=2 && i!=0 || i==1){
			if(i==1 && numword.length>2) numinWords+=" ";
			switch(numword[i]){
			case 2: subtext="Twenty ";break;
			case 3: subtext="Thirty ";break;
			case 4: subtext="Fourty ";break;
			case 5: subtext="Fifty ";break;
			case 6: subtext="Sixty ";break;
			case 7: subtext="Seventy ";break;
			case 8: subtext="Eighty ";break;
			case 9: subtext="Ninty ";break;
			case 0: subtext="";break;
			}

			if(numword[i]==1){
				switch(numword[--i]){
				case 1: subtext="Eleven ";break;
				case 2: subtext="Twelve ";break;
				case 3: subtext="Thirteen ";break;
				case 4: subtext="Fourteen ";break;
				case 5: subtext="Fifteen ";break;
				case 6: subtext="Sixteen ";break;
				case 7: subtext="Seventeen ";break;
				case 8: subtext="Eighteen ";break;
				case 9: subtext="Ninteen ";break;
				case 0: subtext="Ten ";break;
				}
				skip=true;
			}
			numinWords = numinWords + subtext;
		}
		if(((i%2)!=0||i==2 || i==0) && i!=1 && !skip){
			switch(numword[i]){
			case 1: subtext="One ";break;
			case 2: subtext="Two ";break;
			case 3: subtext="Three ";break;
			case 4: subtext="Four ";;break;
			case 5: subtext="Five ";break;
			case 6: subtext="Six ";break;
			case 7: subtext="Seven ";break;
			case 8: subtext="Eight ";break;
			case 9: subtext="Nine ";break;
			case 0: subtext="";break;
			}
			numinWords = numinWords + subtext;
		}
		
		if(i==7){numinWords = numinWords + "Crore ";}
		else if(i==5){numinWords = numinWords + "lakh ";}
		else if(i==3){numinWords = numinWords + "Thousand ";}
		else if(i==2){numinWords = numinWords +"Hundred" ;}
	}
	return numinWords;
	}
	
	
	//test num
	public String convertNum(double n){
		long num =(long)n;
		int i = -1;
		int [] numword = new int[20];
		String numinWords="", subtext=""; 
		boolean flag = true;
		boolean skip = true;
		
		while (num>0){
			i++;
			numword[i]=(int) (num%10);
			num=num/10;			
		}
		
		for( ;i>=0;i--){
			
			if((i==1) ||(i>3 && i%2==0)){
				switch(numword[i]){
				case 0: subtext="";break;
				case 2: subtext="Twenty ";break;
				case 3: subtext="Thirty ";break;
				case 4: subtext="Fourty ";break;
				case 5: subtext="Fifty ";break;
				case 6: subtext="Sixty ";break;
				case 7: subtext="Seventy ";break;
				case 8: subtext="Eighty ";break;
				case 9: subtext="Ninty ";break;				
				}
				if(numword[i] == 1){
					switch(numword[i-1]){
					case 1: subtext="Eleven ";break;
					case 2: subtext="Twelve ";break;
					case 3: subtext="Thirt3een ";break;
					case 4: subtext="Fourteen ";break;
					case 5: subtext="Fifteen ";break;
					case 6: subtext="Sixteen ";break;
					case 7: subtext="Seventeen ";break;
					case 8: subtext="Eighteen ";break;
					case 9: subtext="Ninteen ";break;
					case 0: subtext="Ten ";break;
					}
					flag = false;
				}				
				numinWords = numinWords + subtext;				
			}
			
			
			if(((i<4 && i!=1)||(i>4 && i%2==1)) && (flag)){
				switch(numword[i]){
				case 0: subtext=""; skip=false;break;
				case 1: subtext="One ";skip = true;break;
				case 2: subtext="Two ";skip = true;break;
				case 3: subtext="Three ";skip = true;break;
				case 4: subtext="Four ";skip = true;break;
				case 5: subtext="Five ";skip = true;break;
				case 6: subtext="Six ";skip = true;break;
				case 7: subtext="Seven ";skip = true;break;
				case 8: subtext="Eight ";skip = true;break;
				case 9: subtext="Nine ";skip = true;break;
				}				
				numinWords = numinWords + subtext;
			}
			
			if(i==2 && skip && flag){
				numinWords = numinWords +"Hundred " ;				
			}			
			
			if(i==3){ 
				numinWords = numinWords + "Thousand ";
			}
			
			if(i==7){
				numinWords = numinWords + "Crore ";
			}
			if(i==5){
				numinWords = numinWords + "lakh ";
			}
		}	
		
		
		return numinWords;
	}
	
	public static void main(String[] args){
		NumToWord numToWord = new NumToWord();
		System.out.println(numToWord.numberToWords(125000.0));
		//numToWord.numword(40000);
	}
	
}
