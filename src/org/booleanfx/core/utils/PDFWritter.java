package org.booleanfx.core.utils;


import java.io.FileOutputStream;

import org.booleanfx.core.bean.Acc_patient_bill_headerBean;

import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfWriter;


public class PDFWritter {

	private static String FILE = "c:/GENERATED_PDF/FirstPdf.pdf";

	public PDFWritter(Acc_patient_bill_headerBean webbean) {
	
	}
	
	
	public void execute() {
		try {
			
			Rectangle pageSize = new Rectangle(0,0,870,576);
			
			Document document = new Document(pageSize);
			PdfWriter.getInstance(document, new FileOutputStream(FILE));
			document.open();
			Paragraph paragraph = new Paragraph();
			Rectangle rectangle = new Rectangle(110,375,760,460);
			
			rectangle.setBorderWidthBottom(.5f);
			rectangle.setBorderWidthLeft(.5f);
			rectangle.setBorderWidthTop(.5f);
			rectangle.setBorderWidthRight(.5f);
			
		
			
			document.add(rectangle);
			
			 rectangle = new Rectangle(110,110,760,365);
			
			rectangle.setBorderWidthBottom(.5f);
			rectangle.setBorderWidthLeft(.5f);
			rectangle.setBorderWidthTop(.5f);
			rectangle.setBorderWidthRight(.5f);
			document.add(rectangle);
			
			
			document.close();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static void addMetaData(Document document) {
		document.addTitle("MAMMOTHSHQ BILL ");
		document.addSubject("Using iText");
		document.addKeywords("Java, PDF, iText");
		document.addAuthor("");
		document.addCreator("");
	}

	
	private static void addEmptyLine(Paragraph paragraph, int number) {
		for (int i = 0; i < number; i++) {
			paragraph.add(new Paragraph(" "));
		}
	}
	
}
