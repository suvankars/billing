package org.booleanfx.core.utils;

import java.io.FileOutputStream;
import java.text.DecimalFormat;

import org.booleanfx.core.bean.Acc_patient_bill_detailsBean;
import org.booleanfx.core.bean.Acc_patient_bill_headerBean;
import org.booleanfx.core.dao.Acc_patient_bill_detailsDAO;

import com.itextpdf.text.Document;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfWriter;

public class Superimposing {

	/** The resulting PDF. */
	public static final String SOURCE = "FirstPdf.pdf";
	/** The resulting PDF. */
	public static final String RESULT = "Second.pdf";
	/** The movie poster. */
	

	/*public static void main(String[] args) throws Exception {
		int i = 1;
		new Superimposing().createPdf(SOURCE);

		PdfReader reader = new PdfReader(SOURCE);

		Document document = new Document(new Rectangle(864, 576));

		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(RESULT));

		document.open();

		PdfContentByte canvas = writer.getDirectContent();
		PdfImportedPage page;
		for (i = 1; i <= 2; i++) {
			page = writer.getImportedPage(reader, i);
			canvas.addTemplate(page, 0, 0);
		}
		document.newPage();
		for (i = 3; i <= 4; i++) {
			page = writer.getImportedPage(reader, i);
			canvas.addTemplate(page, 0, 0);
		}

		document.close();
	}*/

	
	public void createPdf(String filename, Acc_patient_bill_headerBean webbean) throws Exception {

		Acc_patient_bill_detailsDAO billDetailsDAO = new Acc_patient_bill_detailsDAO();
		
		Document document = new Document(new Rectangle(864, 576));

		PdfWriter writer = PdfWriter.getInstance(document,
				new FileOutputStream(filename));

		document.open();


		DecimalFormat format = new DecimalFormat("0.00");
		/*document.newPage();

		Image img = Image.getInstance(RESOURCE);
		img.scaleAbsolute(800, 500);
		img.setAlignment(Image.TEXTWRAP | Image.ALIGN_LEFT);

		document.add(img);
*/	
		document.newPage();	

		PdfContentByte over = writer.getDirectContent();
		over.saveState();

		BaseFont bf = BaseFont.createFont();
		over.beginText();
		over.setTextRenderingMode(PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
		over.setFontAndSize(bf, 12);

		over.setTextMatrix(135, 415);
		over.showText("No.:  "+webbean.getBill_no());
		over.setTextMatrix(380, 415);
		over.showText("Date:  "+webbean.getBill_date());

		over.setTextMatrix(135, 400);
		over.showText("Patient Name:  "+webbean.getPatient_name());
		over.setTextMatrix(380, 400);
		over.showText("Patient Age:  "+webbean.getPatient_age());

		over.setTextMatrix(135, 385);
		over.showText("Patient Address:  "+webbean.getPatient_address());

		over.setTextMatrix(135, 370);
		over.showText("Patient Reg No:  "+webbean.getPatient_reg_no());
				
		  if(!webbean.getProfessional_by().equals("")&&(webbean.getReferred_by().equals(""))){
	        	
	        	 over.setTextMatrix(380,370);
	             over.showText("Professional Name = "+webbean.getProfessional_by());
	        	
	        }else if(!webbean.getReferred_by().equals("")&&(webbean.getProfessional_by().equals(""))){
	        	
	        	 over.setTextMatrix(380,370);
	             over.showText("Referred By Name =  "+webbean.getReferred_by());
	        	
	        }else if((!webbean.getProfessional_by().equals(""))&&(!webbean.getReferred_by().equals(""))){
	        	
	        	 over.setTextMatrix(380,385);
	        	 over.showText("Professional Name =  " +webbean.getProfessional_by());
	        	 over.setTextMatrix(380,370);
	         	over.showText("Referred Name = " +webbean.getReferred_by());
	        	
	        }
		 
		 
		
		int  header_id = webbean.getId();
		
		int y_pixel = 350 ;
		
		double total = 0 ;
		
		Acc_patient_bill_detailsBean[] bean = billDetailsDAO.findBybill_header_id(header_id);
					
			over.setTextMatrix(135, y_pixel);
			over.showText("SLNo.");
			
			over.setTextMatrix(250, y_pixel);
			over.showText("Particulars");

			over.setTextMatrix(390, y_pixel);
			over.showText("Rate" );

			over.setTextMatrix(450, y_pixel);
			over.showText("Quantity");

			over.setTextMatrix(520, y_pixel);
			over.showText("Discount");

			over.setTextMatrix(585, y_pixel);
			over.showText("TaxAmount");

			over.setTextMatrix(665, y_pixel);
			over.showText("NetAmount");


			for(int index = 0 ; index < bean.length ; index++){
				
				y_pixel = y_pixel - 15 ;
				
				total = total + bean[index].getLine_gross_amt();
				
				over.setTextMatrix(138, y_pixel);
				over.showText(Integer.toString(index+1));

				if(!bean[index].getLine_item_sl_num().equals("")){
					
					over.setTextMatrix(190, y_pixel);
					over.showText(""+bean[index].getLine_item_sl_num()+",");
					
				}
				
				if(!bean[index].getLine_item_name().equals("")){
					
					over.setTextMatrix(250, y_pixel);
					over.showText(""+bean[index].getLine_item_name());
					
				}
				
				if(!bean[index].getLine_item_model().equals("")){
					
					over.setTextMatrix(320, y_pixel);
					over.showText(","+bean[index].getLine_item_model());
								
				}
				
				over.setTextMatrix(390, y_pixel);
				over.showText(""+format.format(bean[index].getLine_item_price()) );

				over.setTextMatrix(450, y_pixel);
				over.showText(""+(int)bean[index].getLine_item_qty());
				
				over.setTextMatrix(520, y_pixel);
				over.showText(""+format.format(bean[index].getLine_item_discount()));

				over.setTextMatrix(585, y_pixel);
				over.showText(""+format.format(bean[index].getLine_item_tax_amt()));

				over.setTextMatrix(665, y_pixel);
				over.showText(""+format.format(bean[index].getLine_gross_amt()));


				
			
			}
			
	   NumToWord numToWord = new NumToWord() ;
		
		

		over.setTextMatrix(138, 210);
		over.showText("---------------------------------------------------------------------------------------------------------------------------------------------------");
		
		over.setTextMatrix(585, 200);
		over.showText("Total :  "+format.format(total));	
		
		over.setTextMatrix(665, 140);
		over.showText("E.& O.E.");
		
		over.setTextMatrix(660, 160);
		over.showText("Signature:");
		
		over.setTextMatrix(135, 180);
		over.showText("In words :  "+numToWord.numberToWords(total));
		
		over.setTextMatrix(135, 160);
		over.showText("Recieved with Thanks");
		
		//check for payment type
		
		if(webbean.getBill_payment_mode().equals("Cheque")){
		
			over.setTextMatrix(135, 140);
			over.showText("(Subject to realisation)");
			
		}
		

		over.endText();
		over.restoreState();
		itemSection(document,over);
		/**
		 * Open the comment portion for the 2nd page bill
		 * 
		 * 
		 */

		/*
		 * document.newPage();
		 * 
		 * img = Image.getInstance(RESOURCE); img.scaleAbsolute(800,500);
		 * img.setAlignment( Image.TEXTWRAP | Image.ALIGN_LEFT);
		 * 
		 * document.add(img);
		 * 
		 * document.newPage();
		 * 
		 * 
		 * over = writer.getDirectContent(); over.saveState();
		 * 
		 * bf = BaseFont.createFont(); over.beginText();
		 * over.setTextRenderingMode
		 * (PdfContentByte.TEXT_RENDER_MODE_FILL_STROKE);
		 * over.setFontAndSize(bf,12);
		 * 
		 * over.setTextMatrix(150,240); over.showText("zzzzz");
		 * 
		 * over.endText(); over.restoreState();
		 */

		document.close();
	}

	public void itemSection(Document document, PdfContentByte over) throws Exception {

	      
		
	}

	
}
