package org.booleanfx.core.utils;

import org.booleanfx.core.bean.CodeBean;
import org.booleanfx.core.dao.CodeDAO;

public class UUIDGenerator {
	
	public static int getNextUUID(){
		
		String randomString = "" + ((int)(Math.random()*100)) + ((int)(Math.random()*100)) ;
		
		int randomNumber = Integer.parseInt(randomString) ;
		
		return randomNumber ;
	}
	
	private static UUIDGenerator uuidGenerator;
	
	public CodeDAO codeDAO = new CodeDAO() ; 
	
	public static String  bill_no = null ;
	
	public static String  patient_regno = null ;
	
	public static String  app_entryno = null ;
	
public static String  category_code = null ;
	
	public static String  item_code = null ;
	
	public static String  referrer_code = null ;

	public static String  professional_code = null ;

	

	private UUIDGenerator() {

	}

	public static UUIDGenerator getInstance() {
		
		if (uuidGenerator == null) {

			uuidGenerator = new UUIDGenerator();

		}

		return uuidGenerator;
	}

	protected Object clone() throws CloneNotSupportedException {

		throw new CloneNotSupportedException("Singleton class cannot be cloned");

	}

	public String getFirstTimeBill_no() throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			
			bill_no = bean.getBill_no() ;
			
			bill_no = getNextBill_no() ;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("Bill no Exception");
			
		}
		
		return bill_no ;
		
	}
	
	public String getNextBill_no(){
		
		String output="";
		
		int index = bill_no.lastIndexOf("/");
		
		int value = Integer.parseInt(bill_no.substring(index+1,bill_no.length()));
		
		value++;
		
		output = bill_no.substring(0,index+1)+value;
		
		bill_no = output ;
		
		return output ;
		
	}
	
	public String getFirstTimePatient_regNo() throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			
			patient_regno = bean.getPatient_regno() ;
			
			patient_regno = getNextPatient_regNo() ;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("Bill no Exception");
			
		}
		
		return patient_regno ;
		
	}
	
	public String getNextPatient_regNo(){
		
		String output="";
		
		int index = patient_regno.lastIndexOf("/");
		
		int value = Integer.parseInt(patient_regno.substring(index+1,patient_regno.length()));
		
		value++;
		
		output = patient_regno.substring(0,index+1)+value;
		
		patient_regno = output ;
		
		return output ;
		
	}
	
	public String getFirstTimeAppointment_entryNo()throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			
			app_entryno = bean.getApp_entryno() ;
			
			app_entryno = getNextAppointment_entryNo() ;
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("Bill no Exception");
			
		}
		
		return app_entryno ;
		
	}
	
	public String getNextAppointment_entryNo(){
		
        String output="";
		
		int index = app_entryno.lastIndexOf("/");
		
		int value = Integer.parseInt(app_entryno.substring(index+1,app_entryno.length()));
		
		value++;
		
		output = app_entryno.substring(0,index+1)+value;
		
		app_entryno = output ;
		
		return output ;
		
		
	}
	
public String getFirstTimecategory_code() throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			category_code = bean.getCategory_code();
			
			category_code = getNextcategory_code();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("category code Exception");
			
		}
		
		return category_code ;
		
	}
	
	public String getNextcategory_code(){
		
		int value = Integer.parseInt(category_code);
		
		value++;
		
		category_code = ""+value ;
		
		return category_code ;
		
	}

	public String getFirstTimeitem_code() throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			
			item_code = bean.getItem_code();
			
			item_code = getNextitem_code();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("item code Exception");
			
		}
		
		return item_code ;
		
	}
	
	public String getNextitem_code(){
		
		int value = Integer.parseInt(item_code);
		
		value++;
		
		item_code = ""+value ;
		
		return item_code ;
		
	}

public String getFirstTimereferrer_code() throws Exception{
		
		try {
			
			CodeBean  bean = new CodeBean() ;
			
			bean = codeDAO.findRSH() ;
			referrer_code = bean.getCategory_code();
			
			referrer_code = getNextcategory_code();
			
		} catch (Exception e) {
			
			e.printStackTrace();
			
			throw new Exception("referrer code Exception");
			
		}
		
		return referrer_code ;
		
	}
	
	public String getNextreferrer_code(){
		
		int value = Integer.parseInt(referrer_code);
		
		value++;
		
		referrer_code = ""+value ;
		
		return referrer_code ;
		
	}

	



}
